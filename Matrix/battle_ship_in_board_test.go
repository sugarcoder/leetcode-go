package Matrix

import (
	"fmt"
	"testing"
)

func TestBattleShip(t *testing.T) {
	board := [][]byte{{'X','.','.','X'},{'.','.','.','X'},{'.','.','.','X'}}
	fmt.Println(countBattleships(board))
}

func countBattleships(board [][]byte) int {
	dirs := [][]int{{0, -1}, {-1, 0}}
	count := 0
	for i := range board {
		for j := range board[0] {
			if board[i][j] == '.' {
				continue
			}
			first := true
			for _, dir := range dirs {
				ni, nj := i + dir[0], j + dir[1]
				if ni >= 0 && ni < len(board) && nj >= 0 && nj < len(board[0]) {
					if board[ni][nj] == 'X' {
						first = false
						break
					}
				}
			}
			if first {
				count += 1
			}
		}
	}
	return count
}