package Matrix

import (
	"fmt"
	"testing"
	// "gitee.com/sugarcoder/leetcode-go/util"
)

// 417
func TestPacificAtlantic(t *testing.T) {
	// util.ConvertBrackets2Braces("[[1,2,3],[8,9,4],[7,6,5]]")
	heights := [][]int{{1,2,3},{8,9,4},{7,6,5}}
	// heights := [][]int{{1, 2, 2, 3, 5}, {3, 2, 3, 4, 4}, {2, 4, 5, 3, 1}, {6, 7, 1, 4, 5}, {5, 1, 1, 2, 4}}
	fmt.Println(pacificAtlantic(heights))
}

func pacificAtlantic(heights [][]int) [][]int {
	visited1 := bfs(heights, true)
	visited2 := bfs(heights, false)
	res := make([][]int, 0)
	for i, row := range visited1 {
		for j := range row {
			if visited1[i][j] && visited2[i][j]{
				res = append(res, []int{i, j})
			}
		}
	}
	return res
}

func bfs(heights [][]int, isPacific bool) [][]bool {
	dirs := [][]int{{1, 0}, {0, 1}, {-1, 0}, {0, -1}}
	row, col := 0, 0
	marked := make([][]bool, len(heights)) // marked[i][j] == 1, can flow to pacific; marked[i][j] == 2, can flow to both
	for i := range marked {
		marked[i] = make([]bool, len(heights[0]))
	}
	if isPacific == false {
		row = len(heights) - 1
		col = len(heights[0]) - 1
	}
	queue := make([][]int, 0)
	for c := 0; c < len(heights[0]); c++ {
		marked[row][c] = true
		queue = append(queue, []int{row, c})
	}
	for r := 0; r < len(heights); r++ {
		if r == row {
			continue
		}
		marked[r][col] = true
		queue = append(queue, []int{r, col})
	}
	for len(queue) > 0 {
		size := len(queue)
		for i := 0; i < size; i++ {
			point := queue[i]
			for _, dir := range dirs {
				r, c := point[0] + dir[0], point[1] + dir[1]
				if r < 0 || r >= len(heights) || c < 0 || c >= len(heights[0]) {
					continue
				}
				if marked[r][c] == false && heights[r][c] >= heights[point[0]][point[1]] {
					marked[r][c] = true
					queue = append(queue, []int{r, c})
				}
			}
		}
		queue = queue[size:]
	}
	return marked
}
