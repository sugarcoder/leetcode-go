package Matrix

import (
	"fmt"
	"math"
	"testing"

	util "gitee.com/sugarcoder/leetcode-go/util"
)


func TestLeetCode363(t *testing.T) {
    util.ConvertBrackets2Braces("[[1,0,1],[0,-2,3]]")
	matrix := [][]int{{1,0,1},{0,-2,3}}
	k := 2
	fmt.Println(maxSumSubmatrix(matrix, k))
}

func maxSumSubmatrix(matrix [][]int, k int) int {
    // 将问题降维
    // 枚举上下边界
    // 看成数组，Si代表数组arr[:i+1]的前缀和
    // 矩形的sum = Sr - Sl, 数组右端点 - 数组左端点
    // 找到最大的矩形sum 使得 Sr - Sl <= k
    // 枚举右边界，固定Sr，即找 Sl >= Sr - k 能使上式成立的最小Sl
	// 统计矩形最大值
	ans := math.MinInt32
	for i := range matrix {
		sum := make([]int, len(matrix[0]))
		for _, row := range matrix[i:] {
			for i, v := range row {
				sum[i] += v
			}
			t := &util.Treap{}
			t.Put(0)
			sr := 0
			for _, v := range sum {
				sr += v
				if lb := t.Lowerbound(sr - k); lb != nil {
					ans = util.Max(ans, sr - lb.GetVal())
				}
				t.Put(sr)
			}
		}
	}
	return ans
}
