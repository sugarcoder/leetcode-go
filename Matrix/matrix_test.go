package Matrix

import (
	"fmt"
	"testing"

	"gitee.com/sugarcoder/leetcode-go/util"
)

func TestArray2D(t *testing.T) {
	mat := [][]int{}
	row1 := []int{1, 2, 3}
    row2 := []int{4, 5, 6}
    mat = append(mat, row1)
    mat = append(mat, row2)
	fmt.Printf("row num %d\n", len(mat))
	for i, v := range mat {
		fmt.Printf("row %d:", i)
		fmt.Printf("%T:%v\n", v, v)
	}
}

func TestMatCapacity(t *testing.T) {
	mat := [][]int{{1,2},{3,4},{5, 6}}
	newmat := matrixReshape(mat, 2, 3)
	fmt.Println(newmat)
}

func matrixReshape(mat [][]int, r int, c int) [][]int {
    if len(mat) > 0 && r * c != len(mat) * len(mat[0]) {
        return mat
    }
    newmat := [][]int{}
    newrow := make([]int, 0, c)
    for _, row := range mat {
        for _, v := range row {
			// fmt.Println("val:", v)
			newrow = append(newrow, v)
            if len(newrow) == cap(newrow) {
                newmat = append(newmat, newrow)
				newrow = make([]int, 0, c)
				// fmt.Println("newrow", newrow)
            }
        }
    }
    return newmat
}


// 240
func TestSearchMatrix(tt *testing.T) {
	matrix := [][]int{
		{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25},
	}
	for _, row := range matrix {
		fmt.Println(row)
	}
	fmt.Println(searchCenter(matrix, 0, 0, 15))
}

func searchCenter(matrix [][]int, i, j, target int) bool {
    r, c := len(matrix), len(matrix[0])
	fmt.Println("searching", i, j, matrix[i][j])
    if target == matrix[i][j] {
        return true
    } else if target > matrix[i][j] {
        if i == r-1 && j == c-1 {
            return false
        }
        return searchCenter(matrix, min(r-1, i+1), min(c-1, j+1), target)
    } else {
        if searchLeft(matrix, i, j, target) {
            return true
        } else if searchUp(matrix, i, j, target) {
            return true
        } else {
            return false
        }
    }
}

// 搜索左下的矩形
// matrix[i:r][0:j]
func searchLeft(matrix [][]int, i, j, target int) bool {
	pi, pj := i, j-1
    for pi < len(matrix) && pj >= 0 {
        if matrix[pi][pj] == target {
            return true
        } else if matrix[pi][pj] > target {
			pj -= 1 // go left
		} else {
			pi += 1 // go down
		}
    }
    return false
}

// 搜索右上矩形
// matrix[0:i][j:c]
func searchUp(matrix [][]int, i, j, target int) bool {
	pi, pj := i-1, j
    for pi >= 0 && pj < len(matrix[0]) {
        if matrix[pi][pj] == target {
            return true
        } else if matrix[pi][pj] > target {
			pi -= 1 // go up
		} else {
			pj += 1 // go right
		}
    }
    return false
}

func min(a, b int) int {
    if a > b {
        return b
    }
    return a
}


// 378
func TestKthSmallest(t *testing.T) {
    if false {
        util.ConvertBrackets2Braces("[[1,2],[1,3]]")
    }
    matrix := [][]int{{1,2},{1,3}}
    // matrix := [][]int{{1,5,9},{10,11,13},{12,13,15}}
    k := 1
    fmt.Println(kthSmallest(matrix, k))
    // fmt.Println(countSmaller(matrix, 1, 1))
}

func kthSmallest(matrix [][]int, k int) int {
    n := len(matrix) // n * n matrix
    left, right := matrix[0][0], matrix[n-1][n-1]
    ans := -1
    for left <= right {
        mid := left + ((right-left)>>1)
        count := countSmaller(matrix, mid)
        if count >= k {
            ans = mid
            right = mid - 1
        } else {
            left = mid + 1
        }
    }
    return ans
}

// 比较mid小的数的个数
func countSmaller(matrix [][]int, mid int) int {
    // 从左下角开始
    n := len(matrix)
    i, j := n-1, 0
    count := 0
    for i >= 0 && j < n {
        if matrix[i][j] <= mid {
            count += i+1
            j++
        } else {
            i--
        }
    }
    return count
}