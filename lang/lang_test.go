package lang

import (
	"testing"
	"fmt"
	"strconv"
)

func TestSlice(t *testing.T) {
	a := new([]int)
	*a = append(*a, 12)
	fmt.Println(a)
}

type People interface {
	Speak(string) string
}

type Stduent struct{}

func (stu Stduent) Speak(think string) (talk string) {
	if think == "love" {
		talk = "You are a good boy"
	} else {
		talk = "hi"
	}
	return
}

func TestInterface(t *testing.T) {
	var peo People = Stduent{}
	think := "love"
	fmt.Println(peo.Speak(think))
}

func TestDeferReturn(t *testing.T) {
	fmt.Println(returnButDefer())
}

func returnButDefer() (t int) {  //t初始化0， 并且作用域为该函数全域

    defer func() {
        t = t * 10
    }()

    return 1
}

func TestFmtPrint(t *testing.T) {
	fmt.Printf("%08v\n", 127)
}

func TestConvert(t *testing.T) {
	v, _ := strconv.Atoi("")
	fmt.Println(v)
}