package array

import (
	"fmt"
	"testing"
	"math"
	"sort"

	"gitee.com/sugarcoder/leetcode-go/tree"
	// util "gitee.com/sugarcoder/leetcode-go/util"
)

func TestTwoSum(t *testing.T) {
	nums := []int{3, 2, 4}
	target := 6

	fmt.Println(twoSum(nums, target))
}

// 1
func twoSum(nums []int, target int) []int {
	ht := make(map[int]int) // map[nums] -> index

	for i, v := range nums {
		if idx, ok := ht[target-v]; ok {
			return []int{idx, i}
		}
		ht[v] = i
	}
	return []int{}
}

func TestMaxPoints(t *testing.T) {
	points := [][]int{{-6, -1}, {3, 1}, {12, 3}}
	fmt.Println(maxPoints(points))
}

func maxPoints(points [][]int) int {
	maxCount := 0
	if len(points) == 1 {
		return 1
	}
	for i := 0; i < len(points); i++ {
		for k := i + 1; k < len(points); k++ {
			p1, p2 := points[i], points[k]
			count := 0
			x1MinusX2, y1MinusY2 := p1[0]-p2[0], p1[1]-p2[1]
			for _, v := range points {
				if (v[0] == p1[0] && v[1] == p1[1]) || (v[0] == p2[0] && v[1] == p2[1]) {
					count += 1
				} else {
					if (v[0]-p2[0])*y1MinusY2 == (v[1]-p2[1])*x1MinusX2 {
						count += 1
					} else if x1MinusX2 == 0 && v[0]-p2[0] == 0 {
						count += 1
					} else if y1MinusY2 == 0 && v[1]-p2[1] == 0 {
						count += 1
					}
				}
			}
			if count > maxCount {
				maxCount = count
			}
		}
		if maxCount > len(points)/2 {
			return maxCount
		}
	}
	return maxCount
}

func TestRotateArray189(t *testing.T) {
	nums := []int{-1, -100, 3, 99, 0}
	k := 2
	length := len(nums)
	k = k % length
	if k == 0 {
		return
	}
	// method1: reverse
	// revert(nums)
	// revert(nums[:k])
	// revert(nums[k:])
	// fmt.Println(nums)

	// if there is cycle dependencies, need
	changed := make([]bool, len(nums))
	for i := 0; i < length; i++ {
		val := nums[i]
		newindex := (i + k) % length
		for changed[newindex] != true {
			val, nums[newindex] = nums[newindex], val
			changed[newindex] = true
			newindex = (newindex + k) % length
		}
	}
	fmt.Println(nums)
}

func revert(arr []int) {
	for i := 0; i < len(arr)/2; i++ {
		arr[i], arr[len(arr)-1-i] = arr[len(arr)-1-i], arr[i]
	}
}

// 200
func TestNumsOfIsland(t *testing.T) {
	// 题目中用ascii的字符0、1来表示
	grid := [][]byte{
		{1, 1, 0, 0, 0},
		{1, 1, 0, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 0, 1, 1},
	}
	count := 0
	for i, row := range grid {
		for j, v := range row {
			if v == 1 {
				count += 1
				marker(grid, i, j)
			}
		}
	}
	fmt.Println(count)
}

func marker(grid [][]byte, i, j int) {
	if i < 0 || i >= len(grid) || j < 0 || j >= len(grid[0]) {
		return
	}
	if grid[i][j] != 1 {
		return
	}
	grid[i][j] = 2
	marker(grid, i+1, j)
	marker(grid, i, j+1)
	marker(grid, i-1, j)
	marker(grid, i, j-1)
}

// todo 220
func TestContainsDuplicate3(tt *testing.T) {
	var (
		nums      []int
		indexDiff int
		valueDiff int
	)
	nums = []int{0, 10, 22, 15, 0, 5, 22, 12, 1, 5}
	indexDiff = 3
	valueDiff = 3

	rbtree := tree.NewRBTree()
	i := 0
	for ; i <= indexDiff && i < len(nums); i++ {
		fmt.Println("Insert", nums[i])
		rbtree.RBInsert(nums[i])
		rbtree.LevelOrderTraversalPrint()
		if nextNode := rbtree.GetSuccessor(nums[i]); nextNode != rbtree.GetNil() {
			fmt.Println("nextNode val:", nextNode.GetVal())
			if abs(nextNode.GetVal()-nums[i]) <= valueDiff {
				fmt.Println("true")
				return
			}
		}
		if prevNode := rbtree.GetPredecessor(nums[i]); prevNode != rbtree.GetNil() {
			fmt.Println("prevNode val:", prevNode.GetVal())
			if abs(nums[i]-prevNode.GetVal()) <= valueDiff {
				fmt.Println("true")
				return
			}
		}
	}
	for ; i < len(nums); i++ {
		rmVal := nums[i-indexDiff-1]
		rbtree.RBDelete(rmVal)
		rbtree.RBInsert(nums[i])
		fmt.Printf("remove %d, insert %d\n", rmVal, nums[i])
		rbtree.LevelOrderTraversalPrint()
		if nextNode := rbtree.GetSuccessor(nums[i]); nextNode != rbtree.GetNil() {
			fmt.Println("nextNode val:", nextNode.GetVal())
			if abs(nextNode.GetVal()-nums[i]) <= valueDiff {
				fmt.Println("true")
				return
			}
		}
		if prevNode := rbtree.GetPredecessor(nums[i]); prevNode != rbtree.GetNil() {
			fmt.Println("prevNode val:", prevNode.GetVal())
			if abs(nums[i]-prevNode.GetVal()) <= valueDiff {
				fmt.Println("true")
				return
			}
		}
	}

	fmt.Println("false")
}

func TestSliceCopy(tt *testing.T) {
	nums := []int{1, 2, 3, 4, 5}
	copy(nums[:4], nums[2:])
	fmt.Println(nums)
}

func abs(v int) int {
	if v < 0 {
		return -v
	}
	return v
}

// 239
func TestProductExceptSelf(tt *testing.T) {
	nums := []int{1, 2, 3, 4}
	length := len(nums)
	ans := make([]int, length)
	for i, v := range nums {
		ans[length-i-1] = v
	}
	fmt.Println("ans", ans)
	for i := range nums {
		if i != 0 {
			ans[i] = ans[i-1] * ans[i]
		}
	}
	for i := range nums {
		if i != 0 {
			nums[i] = nums[i-1] * nums[i]
		}
	}
	// reverse
	// for i, j := 0, length-1; i < j; i, j = i+1, j-1 {
	//     ans[i], ans[j] = ans[j], ans[i]
	// }
	fmt.Println("prefix", nums)
	fmt.Println("postfix:", ans)
}

// 287, 环形链表（判断环，找环的入口）
func TestFindDupicate(tt *testing.T) {
	nums := []int{1, 3, 4, 2, 2}
	slow, fast := 0, 0
	for {
		slow = nums[slow]
		fast = nums[nums[fast]]
		if slow == fast {
			break
		}
	}
	fmt.Println("first meet:", slow)
	fast = 0
	for slow != fast {
		slow = nums[slow]
		fast = nums[fast]
	}
	fmt.Println("fast=", fast)
}

func TestShuffleArray(t *testing.T) {
	nums := []int{1, 3, 4, 2}
	tmp := make([]int, 4)
	copy(tmp, nums)
	fmt.Println(tmp)
}


// 391
func TestPerfectRectangle(t *testing.T) {
	// util.ConvertBrackets2Braces("[[1,1,3,3],[3,1,4,2],[3,2,4,4],[1,3,2,4],[2,3,3,4]]")
	rectangles := [][]int{{1,1,3,3},{3,1,4,2},{3,2,4,4},{1,3,2,4},{2,3,3,4}}
	fmt.Println(isRectangleCover(rectangles))
}

// timeout
func isRectangleCover(rectangles [][]int) bool {
    // sort the rectangle with bottom left point, left, top
    sort.Slice(rectangles, func(i, j int) bool {
        if rectangles[i][0] < rectangles[j][0] {
            return true
        } else if rectangles[i][0] == rectangles[j][0] {
            return rectangles[i][1] < rectangles[j][1]
        }
        return false
    })
    bottom := math.MaxInt32
    left := math.MaxInt32
    top := math.MinInt32
    right := math.MinInt32
    for _, rect := range rectangles {
        left = min(left, rect[0])
        bottom = min(bottom, rect[1])
        right = max(right, rect[2])
        top = max(top, rect[3])
    }
    fmt.Println(bottom, left, right, top)
    // use line to scan, from left to right, from bottom to top
    for x := left; x < right; x++ {
        for y := bottom; y < top; y++ {
            // whether point(x, y) is included exactly once
            point := []int{x, y}
            finded := false
            for _, rect := range rectangles {
                flag := isInclude(rect, point)
                if flag == 0 {
                    if finded == false {
                        finded = true
                    } else {
                        return false // overlap
                    }
                } else if flag > 0{
					continue
				} else {
					break
				}
            }
            if !finded {
                return false
            }
        }
    }
    return true
}

func isInclude(rectangle []int, point []int) int {
    // point: bottom left point
    // return whether rectangle include point
    // 0 - include
    // 1 - righter and upper than current rectangle
    // -1 - lefter and downer than current rectangle
    if point[0] < rectangle[0] {
        return -1
    } else if rectangle[0] <= point[0] && point[0] < rectangle[2] && rectangle[1] <= point[1] && point[1] < rectangle[3] {
        return 0
    } else {
        return 1
    }
}

func max(a, b int) int {
    if a > b {
        return a
    }
    return b
}

func min(a, b int) int {
    if a > b {
        return b
    }
    return a
}