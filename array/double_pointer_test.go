package array

import (
	"testing"
	"fmt"

	util "gitee.com/sugarcoder/leetcode-go/util"
)

func TestTasks(t *testing.T) {
	// sorted by time increasing
	tasks := [][]int{{1, 6}, {2, 4}, {4, 9}, {5, 5}}  // {time, value}
	timeLimit := 6

	// select two different tasks that get the max value
	n := len(tasks)
	left, right := 0, n-1
	maxProfit := 0
	for left < right {
		if tasks[left][0] + tasks[right][0] <= timeLimit {
			maxProfit = util.Max(maxProfit, tasks[left][1] + tasks[right][1])
			left++
		} else {
			right--
		}
	}

	fmt.Println(maxProfit)
}

// 11 移动较小的边界
func maxArea(height []int) int {
	left, right := 0, len(height)-1
	area := 0
	for left < right {
		if height[left] < height[right] {
			area = util.Max(area, height[left] * (right - left))
			left++
		} else {
			area = util.Max(area, height[right] * (right - left))
			right--
		}
	}
	return area
}

func TestMaxArea(t *testing.T) {
	height := []int{1,8,6,2,5,4,8,3,7}
	fmt.Println(maxArea(height))
}