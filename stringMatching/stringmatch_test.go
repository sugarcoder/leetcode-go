package stringMatching

import (
	"testing"
	"fmt"
)

func TestStringMatching(t *testing.T) {
	str := "abaabcaba"
	pattern := "abca"
	fmt.Println("arr[1:2] = ", str[1:2])
	fmt.Println(prefix_string(pattern))
	fmt.Println(get_next(str))
}

// 前缀数组是为了子串服务的
// 计算前缀数组, 朴素算法
// pi[i] 的值为 s[:i+1]的最长真前缀和真后缀的长度
// 生成 部分匹配表（PM）
func prefix_string(s string) []int {
	pi := make([]int, len(s))
	for i := 1; i < len(s); i++ {
		for j := i; j > 0; j-- {  // 长度为j的真前缀和真后缀，从最大开始计算
			if s[0:j] == s[i-j+1:i+1] {
				pi[i] = j
				break
			}
		}
	}
	return pi
}

// 求子串的next数组，返回的next数组应该只使用next[1:]
// next[i]对应的是s[i-1]的next值，该值=串s[:i-1]的最长相等前后缀的长度+1
func get_next(s string) (next []int) {
	next = make([]int, len(s)+1)
	next[1] = 0
	j := 0
	for i := 1; i < len(s);  {
		if j == 0 || s[i-1] == s[j-1] {
			i += 1
			j += 1
			next[i] = j
		} else {
			j = next[j]
		}
	}
	return
}

// 返回p在s中第一次出现的下标
func Index_KMP(s string, p string, next []int) int {
	i := 1
	j := 1
	for i <= len(s) && j <= len(p) {
		if j == 0 || s[i-1] == p[j-1] {
			i += 1
			j += 1
		} else {
			j = next[j]
		}
	}
	if j > len(p) {
		return i - len(p) - 1
	} else {
		return 0
	}
}

func TestKMP(t *testing.T) {
	str := "abaabcaba"
	pattern := "abcab"
	next := get_next(pattern)
	fmt.Println(next)
	fmt.Println(Index_KMP(str, pattern, next))
}

// leetcode 28
// 使用前缀数组来求解字符串匹配
func get_prefix(p string) []int {
	prefix := make([]int, len(p)) // p的前缀函数
	prefix[0] = 0 // "a"的最长公共真前后缀长度一定为0
	j := 0
	for i := 1; i < len(prefix); i++ {
		for j > 0 && p[j] != p[i] {
			j = prefix[j-1]
		}
		if p[j] == p[i] {
			j++
		}
		prefix[i] = j
	}
	return prefix
}

func TestGetPrefix(tt *testing.T) {
	p := "cac"
	next := get_prefix(p)
	fmt.Println(next)
	s := "abcacabcab"
	j := 0
	for i := 0; i < len(s); i++ {
		for j > 0 && s[i] != p[j] {
			j = next[j-1]
		}
		if p[j] == s[i] {
			j++
		}
		if j == len(p) {
			fmt.Println("first match index", i - len(p) + 1)
			return
		}
	}
}

// 214 朴素解法
func TestShortestPalidrome(tt *testing.T) {
	s := "cacd"
	for i := len(s); i >= 0; i-- {
		if isPalidrome(s[0:i]) {
			fmt.Println(revert(s[i:]) + s)
			return
		}
	}
	// fmt.Println(isPalidrome(s))
}

func isPalidrome(str string) bool {
	for i, j:=0, len(str)-1; i < j; i, j = i+1, j-1 {
		if str[i] != str[j] {
			return false
		}
	}
	return true
}

func revert(str string) string{
	rs := []byte(str)
	for i, j:=0, len(rs)-1; i < j; i, j = i+1, j-1 {
		rs[i], rs[j] = rs[j], rs[i]
	}
	return string(rs)
}

// 214使用KMP 的找前缀数组
func TestShortestPalidromeKMP(tt *testing.T) {
	s := "aace"
	revs := revert(s)
	str := s+"#"+revs
	// get the prefix array of str
	prefix := make([]int, len(str))
	prefix[0] = 0
	j := 0
	for i := 1; i < len(str); i++ {
		for j > 0 && str[i] != str[j] {
			j = prefix[j-1]
		}
		if str[i] == str[j] {
			j++
		}
		prefix[i] = j
	}
	longest := prefix[len(str)-1]
	fmt.Println(revs[:len(revs)-longest] + s)
}