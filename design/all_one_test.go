package design

import (
	"container/list"
	"fmt"
	"testing"
)

// 432
type NodeType struct {
	strs map[string]struct{}
	cnt  int
}

type AllOne struct {
	*list.List // cnt从小到大
	headMemo   map[string]*list.Element
}

func constructor() AllOne {
	return AllOne{
		List:     list.New(),
		headMemo: make(map[string]*list.Element),
	}
}

func (this *AllOne) inc(key string) {
	if ele, ok := this.headMemo[key]; ok {
		node := ele.Value.(*NodeType)
		new_cnt := node.cnt + 1
		if ele.Next() == nil || ele.Next().Value.(*NodeType).cnt > new_cnt {
			// create new node
			new_node := &NodeType{
				cnt:  new_cnt,
				strs: make(map[string]struct{}),
			}
			new_node.strs[key] = struct{}{}
			// insert the new node after n
			new_element := this.InsertAfter(new_node, ele)
			this.headMemo[key] = new_element
		} else {
			next_element := ele.Next()
			next_element.Value.(*NodeType).strs[key] = struct{}{}
			this.headMemo[key] = next_element
		}
		delete(node.strs, key)
		if len(node.strs) == 0 {
			this.Remove(ele)
		}
	} else {
		if this.Front() == nil || this.Front().Value.(*NodeType).cnt > 1 {
			new_node := &NodeType{
				cnt:  1,
				strs: make(map[string]struct{}),
			}
			new_node.strs[key] = struct{}{}
			new_element := this.PushFront(new_node)
			this.headMemo[key] = new_element
		} else {
			this.Front().Value.(*NodeType).strs[key] = struct{}{}
			this.headMemo[key] = this.Front()
		}
	}
}

func (this *AllOne) dec(key string) {
	ele := this.headMemo[key]
	node := ele.Value.(*NodeType)
	new_cnt := node.cnt - 1
	if node.cnt > 1 {
		if ele.Prev() == nil || ele.Prev().Value.(*NodeType).cnt < new_cnt {
			this.headMemo[key] = this.InsertBefore(&NodeType{cnt: new_cnt, strs: map[string]struct{}{key: {}}}, ele)
		} else {
			ele.Prev().Value.(*NodeType).strs[key] = struct{}{}
			this.headMemo[key] = ele.Prev()
		}
	} else {
		delete(this.headMemo, key)
	}
	delete(node.strs, key)
	if len(node.strs) == 0 {
		this.Remove(ele)
	}
}

func (this *AllOne) getMaxKey() string {
	if b := this.Back(); b != nil {
		for k := range b.Value.(*NodeType).strs {
			return k
		}
	}
	return ""
}

func (this *AllOne) getMinKey() string {
	if f := this.Front(); f != nil {
		for k := range f.Value.(*NodeType).strs {
			return k
		}
	}
	return ""
}

func TestAllOne(t *testing.T) {
	one := constructor()
	one.inc("abc")
	for i := 0; i < 4; i++ {
		one.inc("bb")
	}
	one.dec("bb")
	one.dec("bb")
	// one.inc("aa")
	fmt.Println(one.getMaxKey())
	fmt.Println(one.getMinKey())
	// one.dec("aa")
	// fmt.Println(one.getMaxKey())
	// fmt.Println(one.getMinKey())
}

/**
panic: runtime error: invalid memory address or nil pointer dereference
[signal SIGSEGV: segmentation violation code=0x1 addr=0x18 pc=0x4aaa8e]
main.(*AllOne).Dec(0xc000072d50, {0x575df0, 0x1})
solution.go, line 61
main.(*DriverSolution).Helper_Select_Method(0xc0000664c0?, {0xc000012703?, 0x10?}, {0xc000014970?, 0x4b5340?, 0x3?}, 0xc000072d50)
solution.go, line 125
main.(*DriverSolution).Helper(0xc0000781e0?, {0xc000088300, 0xa, 0x4b4440?}, {0xc000002300, 0xa, 0xc000012040?})
solution.go, line 186
main.main()
solution.go, line 231
*/
