package design

import (
	"fmt"
	"testing"
)

func TestSummaryRange(t *testing.T) {
	obj := Constructor();
	values := []int{1, 3, 7, 2, 6}
	for _, v := range values {
		obj.AddNum(v);
		fmt.Println(obj.GetIntervals())
	}
}

type SummaryRanges struct {
	intervals [][]int
}


func Constructor() SummaryRanges {
	return SummaryRanges{intervals: make([][]int, 0)}
}


func (this *SummaryRanges) AddNum(value int)  {
	left, right := 0, len(this.intervals)-1
	var mid int = -1
	for left <= right {
		mid = (left + right) / 2
		if this.intervals[mid][0] <= value && value <= this.intervals[mid][1] {
			return
		} else if value < this.intervals[mid][0] {
			right = mid - 1
		} else {
			left = mid + 1
		}
	}
	if mid == -1 { // 空区间
		this.intervals = append(this.intervals, []int{value, value})
	} else if value < this.intervals[mid][0] {
		// 在左侧插入新区间
		backs := this.intervals[mid:]  // 原来slice的引用
		fronts := this.intervals[:mid]
		new_intervals := make([][]int, len(this.intervals)+1)
		copy(new_intervals[:len(fronts)], fronts)
		new_intervals[len(fronts)] = []int{value, value}
		copy(new_intervals[len(fronts)+1:], backs)
		// 注意！不能这样直接append，否则会修改原来的slice
		// fronts = append(fronts, ) // 新区间所在index == mid
		// fronts = append(fronts, backs...)
		this.intervals = new_intervals
		// 合并新区间的左右区间
		if this.merge_right(mid-1) {
			mid -= 1
		}
		this.merge_right(mid)
	} else {
		// 在右侧插入新区间
		backs := this.intervals[mid+1:]
		fronts := this.intervals[:mid+1]
		new_intervals := make([][]int, len(this.intervals)+1)
		copy(new_intervals[:len(fronts)], fronts)
		new_intervals[len(fronts)] = []int{value, value}
		copy(new_intervals[len(fronts)+1:], backs)
		// fronts = append(fronts, []int{value, value}) // 新区间所在位置 index == mid+1
		// fronts = append(fronts, backs...)
		this.intervals = new_intervals
		if this.merge_right(mid) {
			mid -= 1
		}
		this.merge_right(mid+1)
	}
}


// 合并index与index+1两个区间
// 返回是否发生了合并
func (this *SummaryRanges) merge_right(index int) bool {
	if index >= 0 && index < len(this.intervals)-1 {
		curs := this.intervals[index]
		rights := this.intervals[index+1]
		if curs[1] >= rights[0] - 1 {
			// 修改当前区间的界限
			this.intervals[index][1] = rights[1]
			// 删除右侧区间
			this.intervals = append(this.intervals[:index+1], this.intervals[index+2:]...)
			return true
		}
	}
	return false
}


func (this *SummaryRanges) GetIntervals() [][]int {
	return this.intervals
}