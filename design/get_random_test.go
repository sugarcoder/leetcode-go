package design

import (
	"testing"
	"fmt"
	"math/rand"
)

func TestGetRandom(t *testing.T) {
	rc := RCConstructor()
	rc.Insert(4)
	rc.Insert(3)
	rc.Insert(4)
	rc.Insert(2)
	rc.Insert(4)
	rc.Insert(4)

	rc.Remove(4)
	rc.Remove(4)
	fmt.Println(rc.Remove(4))
}

// 381
type RandomizedCollection struct {
    ht map[int]map[int]bool // mapping value to indexes
    nums []int
}


func RCConstructor() RandomizedCollection {
    return RandomizedCollection{
        ht: make(map[int]map[int]bool),
        nums: make([]int, 0),
    }
}


func (this *RandomizedCollection) Insert(val int) bool {
    if idxes, ok := this.ht[val]; ok && len(idxes) > 0 {
        // value already exists
        idx := len(this.nums)
        this.ht[val][idx] = true
        this.nums = append(this.nums, val)
        return false
    } else {
        idx := len(this.nums)
        this.ht[val] = make(map[int]bool)
        this.ht[val][idx] = true
        this.nums = append(this.nums, val)
        return true
    }
}


func (this *RandomizedCollection) Remove(val int) bool {
    if idxes, ok := this.ht[val]; ok && len(idxes) > 0 {
        for idx := range idxes {
            last_idx := len(this.nums)-1
			last_num := this.nums[last_idx]
			delete(this.ht[val], idx)
            if last_idx != idx {
                delete(this.ht[last_num], last_idx)
                this.ht[last_num][idx] = true
                this.nums[idx] = last_num
            }
            this.nums = this.nums[:last_idx]
            break
        }
        return true
    } else {
        return false
    }
}


func (this *RandomizedCollection) GetRandom() int {
    idx := rand.Int() % len(this.nums)
    return this.nums[idx]
}
