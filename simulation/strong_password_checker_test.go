package simulation

import (
	"testing"
	"fmt"
)

// 420
func TestStrongPasswordChecker(t *testing.T) {
	fmt.Println(strongPasswordChecker("aA1"))
}

func strongPasswordChecker(password string) int {
	has_upper, has_lower, has_digit := 0, 0, 0
	for _, c := range password {
		if c >= 'A' && c <= 'Z' {
			has_upper = 1
		} else if c >= 'a' && c <= 'z' {
			has_lower = 1
		} else if c >= '0' && c <= '9' {
			has_digit = 1
		}
	}
	if len(password) < 6 {
		return max(6 - len(password), 3 - has_upper - has_lower - has_digit)
	} else if len(password) <= 20 {
		replace := 0
		cnt := 0
		cur := '#'
		for _, c := range password {
			if c == cur {
				cnt++
			} else {
				replace += cnt / 3
				cnt = 1
				cur = c
			}
		}
		replace += cnt / 3
		return max(replace, 3 - has_upper - has_lower - has_digit)
	} else {
		replace, remove := 0, len(password)-20
		rm2 := 0 // k mod 3 == 1的组数，即删除两个字符可以减少1次替换操作
		cnt := 0
		cur := '#'
		for _, ch := range password {
			if ch == cur {
				cnt += 1
			} else {
				if remove > 0 && cnt >= 3 {
					// 可以用删除代替替换操作
					if cnt % 3 == 0 {
						remove--
						replace--
					} else if cnt % 3 == 1 {
						rm2++
					}
				}
				replace += cnt / 3
				cnt = 1
				cur = ch
			}
		}
		if remove > 0 && cnt >= 3 {
			if cnt % 3 == 0 {
				remove--
				replace--
			} else if cnt % 3 == 1 {
				rm2++
			}
		}
		replace += cnt / 3

		// 使用 k%3 == 1的组的数量，由剩余替换次数、组数、和剩余删除次数的一半的最小值决定
		use2 := min(replace, min(rm2, remove / 2))
		replace -= use2
		remove -= use2 * 2

		// 计算 k%3 == 2的组的数量, 三个删除操作替代一个替换操作
		use3 := min(replace, remove / 3)
		replace -= use3
		remove -= use3 * 3
		return (len(password)-20) + max(replace, 3 - has_digit - has_upper - has_lower)
	}	
}