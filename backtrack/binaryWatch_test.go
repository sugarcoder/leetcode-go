package backtrack

import (
	"fmt"
	"testing"
)

func readBinaryWatch(turnedOn int) []string {
    leds := make([]int, 0)
    for t := 1; t <= 32; t *= 2 {
        leds = append(leds, t)
        if t <= 8 {
            leds = append(leds, t*10) // hour led
        }
    }
    ans := make([]string, 0)
    backtrack401(leds, 0, 0, 0, turnedOn, &ans)
    return ans
}

func backtrack401(leds []int, index int, hour, minute int, turnedOne int, ans *[]string) {
	if hour >= 12 || minute >= 60 {
		return
	}
	if turnedOne == 0 {
		*ans = append(*ans, fmt.Sprintf("%d:%02d", hour, minute))
		return
	}
	for i := index; i < len(leds); i++ {
		t := leds[i]
		if t % 10 == 0 {
			backtrack401(leds, i+1, hour+t/10, minute, turnedOne - 1, ans)
		} else {
			backtrack401(leds, i+1, hour, minute+t, turnedOne - 1, ans)
		}
	}
}

// 401
func TestBinaryWatch(t *testing.T) {
	fmt.Println(readBinaryWatch(4))
}