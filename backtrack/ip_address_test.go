package backtrack

import (
	"testing"
	"fmt"
	"strconv"
)

func TestRestoreIPAddress(t *testing.T) {
	fmt.Println(restoreIpAddresses("101023"))
	fmt.Println(restoreIpAddresses("00000"))
}

func restoreIpAddresses(s string) []string {
	res := make([]string, 0)
	for i := 1; i <= 3; i++ {
		prev := s[:i]
		v, _ := strconv.Atoi(prev)
		if v > 255 {
			break
		}
		posts := backtrackIP(s[i:], 3)
		for _, post := range posts {
			res = append(res, prev + "." + post)
		}
		if v == 0 {
			break
		}
	}
	return res
}

func backtrackIP(s string, part int) []string {
	if part == 0 || len(s) == 0 {
		return []string{}
	} else if part == 1 {
		v, _ := strconv.Atoi(s)
		if v > 255 {
			return []string{}
		}
		if len(s) > 1 && s[0] == '0' {
			return []string{}
		}
		return []string{s}
	}
	res := make([]string, 0)
	for i := 1; i <= 3 && i < len(s); i++ {
		prev := s[:i]
		v, _ := strconv.Atoi(prev)
		if v > 255 {
			break
		}
		posts := backtrackIP(s[i:], part - 1)
		for _, post := range posts {
			res = append(res, prev + "." + post)
		}
		if v == 0 {
			break
		}
	}
	return res
}