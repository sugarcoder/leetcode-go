package backtrack

import (
	"fmt"
	// "math"
	"testing"
)

// 301
func TestRemoveInvalidParentheses(t *testing.T) {
	s := "((()())"
	s = "l)((j())()()))(("
	s = ")("

	lrm, rrm := 0, 0
	for i := range s {
		if s[i] == '(' {
			lrm++
		} else if s[i] == ')' {
			if lrm == 0 {
				rrm++
			} else {
				lrm--
			}
		}
	}
	// minRm = math.MaxInt32
	// ans := make(map[string]bool)
	ans := []string{}
    backtrack301(s, 0, lrm, rrm, &ans)
    // results := []string{}
    // for k := range ans {
    //     results = append(results, k)
    // }
	// if len(results) == 0 {
    //     results = append(results, "")
    // }
    fmt.Println(ans)
}

func isValid(s string) bool {
	stack := 0
	for i := range s {
		if s[i] == '(' {
			stack += 1
		} else if s[i] == ')' {
			if stack == 0 {
				return false
			} else {
				stack -= 1
			}
		}
	}
	return stack == 0
}

// var minRm int

func backtrack301(s string, idx int, lrm int, rrm int, ans *[]string) {
    if lrm == 0 && rrm == 0 {
        if isValid(s) {
            *ans = append(*ans, s)
        }
        return
    }
    for i := idx; i < len(s); i++ {
		// prune: 去重
		if i != idx && s[i] == s[i-1] {
			continue
		}
		if lrm + rrm > len(s)-i {
			return
		}
        if s[i] == '(' && lrm > 0 {
			backtrack301(s[:i]+s[i+1:], i, lrm-1, rrm, ans)
        }
		if s[i] == ')' && rrm > 0 {
			backtrack301(s[:i]+s[i+1:], i, lrm, rrm-1, ans)
		}
    }
}

func clearMap(ht map[string]bool) {
	for k := range ht {
		delete(ht, k)
	}
}

func clearMapGenerics[K comparable, V comparable](ht map[K]V) {
	// ht = make(map[int]int) // not work
	for k := range ht {
		delete(ht, k)
	}
}


func TestString(t *testing.T) {
	// ans := []string{}
	// s := "abc"
	// ans = append(ans, s)
	// s = s[:1] + s[2:]
	// ans = append(ans, s)
	// fmt.Println(ans)
	// s := "abc"
	// fmt.Println(s[3:])
	fmt.Println(isValid("aa"))
}

func TestClearMap(t *testing.T) {
	ht := make(map[int]int)
	ht[1] = 2
	clearMapGenerics(ht)
	fmt.Println(ht)
}
