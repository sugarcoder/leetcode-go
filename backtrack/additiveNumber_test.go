package backtrack

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"testing"
)

func TestOpString(t *testing.T) {
	fmt.Println("")
	fmt.Println(strconv.Atoi("2"))
	ans := &strings.Builder{}
	ans.WriteByte('3')
	fmt.Println([]byte(ans.String()))
	fmt.Println(math.MaxInt32)
	num := "12345"
	fmt.Println(num[2:8])
}

func TestStrAdd(t *testing.T) {
	fmt.Println(strAdd("0099", "9999"), 99+9999)
}

func TestAdditiveNumber(t *testing.T) {
	num := "199100199"
	num = "199111992"
	num = "199001200" // cannot have leading zero
	if len(num) < 3 {
		fmt.Println("false")
	}
	for idx1 := 1; idx1 <= len(num)/2; idx1++ {
		if backtrack306(num, idx1, idx1+1) {
			fmt.Println("true")
			return
		}
		if num[:idx1] == "0" {
			break
		}
	}
	fmt.Println("false")
}

func backtrack306(s string, idx1 int, idx2 int) bool {
	num1 := s[:idx1]
	for ; idx2 < len(s) && idx2-idx1 <= len(s)-idx2; idx2++ {
		num2 := s[idx1:idx2]
		sum := strAdd(num1, num2)
		fmt.Printf("\ns=%s\n", s)
		fmt.Printf("%s + %s = %s\n", num1, num2, sum)
		if idx2+len(sum) > len(s) {
			return false
		}
		if s[idx2:idx2+len(sum)] == sum {
			if idx2+len(sum) == len(s) {
				return true
			}
			if backtrack306(s[idx1:], idx2-idx1, idx2-idx1+1) {
				return true
			}
		}
		if num2 == "0" {
			break
		}
	}
	return false
}

func strAdd(a string, b string) string {
	ans := &strings.Builder{}
	var carry byte = 0
	for i, j := len(a)-1, len(b)-1; i >= 0 || j >= 0 || carry > 0; i, j = i-1, j-1 {
		var v1, v2 byte = 0, 0
		if i >= 0 {
			v1 = a[i] - '0'
		}
		if j >= 0 {
			v2 = b[j] - '0'
		}
		sum := v1 + v2 + carry
		digit := sum % 10
		carry = sum / 10
		ans.WriteByte('0' + digit)
	}
	res := []byte(ans.String())
	for i, j := 0, len(res)-1; i < j; i, j = i+1, j-1 {
		res[i], res[j] = res[j], res[i]
	}
	return string(res)
}

func TestCards(t *testing.T) {
	inputs := []int{000, 111, 222, 345, 678, 891}
    cards := make([]int, 0)
    memo := make(map[int]int) // mapping val --> minCount
    for _, v := range inputs {
        mask := 0
        if v == 0 {
            mask = 1
        }
        for v > 0 {
            mask |= (1 << (v % 10))
            v /= 10
        }
        // fmt.Printf("%b\n", mask)
        memo[mask] = 1
        cards = append(cards, mask)
    }
    used := make([]bool, len(cards))
    var minCount int = math.MaxInt32
    for i, v := range cards {
        used[i] = true
        ret := backtrack(cards, used, v, 1)
        used[i] = false
        if ret != -1 {
            minCount = Min(minCount, ret)
        }
    }
    if minCount == math.MaxInt32 {
        fmt.Println(-1)
    } else {
        fmt.Println(minCount)
    }
}

func backtrack(cards []int, used []bool, cur int, count int) int {
    if cur == 0x3ff {
        return count
    } else if count == len(used) {
        return -1
    }
    minCount := math.MaxInt32
    for i, b := range used {
        if !b {
            next := cur | cards[i]
            if next != cur {
                used[i] = true
                ret := backtrack(cards, used, next, count + 1)
                used[i] = false
                if ret != -1 {
                    minCount = Min(ret, minCount)
                }
            }
        }
    }
    if minCount == math.MaxInt32 {
        return -1
    }
    return minCount
}

func TestCardsMemo(t *testing.T) {
	inputs := []int{000, 111, 222, 345, 678, 891}
    cards := make([]int, 0)
    memo := make(map[int]int) // mapping val --> minCount
    for _, v := range inputs {
        mask := 0
        if v == 0 {
            mask = 1
        }
        for v > 0 {
            mask |= (1 << (v % 10))
            v /= 10
        }
        // fmt.Printf("%b\n", mask)
        // memo[mask] = 1
        cards = append(cards, mask)
    }
    used := make([]bool, len(cards))
    var minCount int = math.MaxInt32
    for i, v := range cards {
        used[i] = true
        ret := backtrackMemo(cards, used, memo, v, 1)
        used[i] = false
        if ret != -1 {
            minCount = Min(minCount, ret)
        }
    }
    if minCount == math.MaxInt32 {
        fmt.Println(-1)
    } else {
        fmt.Println(minCount)
    }
}

func backtrackMemo(cards []int, used []bool, memo map[int]int, cur int, count int) int {
    if c, ok := memo[cur]; ok {
        return count + c - 1
    }
    if cur == 0x3ff {
        return count
    } else if count == len(used) {
        return -1
    }
    minCount := math.MaxInt32
    for i, b := range used {
        if !b {
            next := cur | cards[i]
            if next != cur {
                used[i] = true
                ret := backtrackMemo(cards, used, memo, next, count + 1)
                used[i] = false
                if ret != -1 {
                    memo[next] = ret - count
                    minCount = Min(ret, minCount)
                }
            }
        }
    }
    if minCount == math.MaxInt32 {
        return -1
    }
    memo[cur] = minCount
    return minCount
}

func Min(a ...int) int {
	min := a[0]
	for i := 1; i < len(a); i++ {
		if a[i] < min {
			min = a[i]
		}
	}
	return min
}