package backtrack

import (
	"fmt"
	"testing"
	"strconv"
)

func TestExpressionAddOperator(tt *testing.T) {
	// if v, err := strconv.Atoi("-123"); err == nil {
	// 	fmt.Println(v)
	// 	fmt.Println(fmt.Sprintf("%d", v))
	// }
	fmt.Println(addOperators("232", 8))
}

func addOperators(num string, target int) []string {
    res := make([]string, 0)
    for i := 1; i <= len(num); i++ {
        if v, err := strconv.Atoi(num[:i]); err == nil {
			backtrack282(num[i:], target, v, v, num[:i], &res)
			if v == 0 {
				break
			}
		}
    }
    return res
}

func backtrack282(num string, target int, curVal int, tailNum int, expr string, res *[]string) {
	if num == "" {
		if target == curVal {
			*res = append(*res, expr)
		}
		return
	}
	for i := 1; i <= len(num); i++ {
		// 依次选取数字
		if v, err := strconv.Atoi(num[:i]); err == nil {
			backtrack282(num[i:], target, curVal + v, v, expr + "+" + fmt.Sprintf("%d", v), res)
			backtrack282(num[i:], target, curVal - v, -v, expr + "-" + fmt.Sprintf("%d", v), res)
			backtrack282(num[i:], target, curVal - tailNum + tailNum * v, tailNum * v, expr + "*" + fmt.Sprintf("%d", v), res)
			if v == 0 {
				break
			}
		}
	}
}