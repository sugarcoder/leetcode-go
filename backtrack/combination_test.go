package backtrack

import (
	"testing"
	"fmt"
)

// 216
func TestCombination(t *testing.T) {
	var k, n int
	k = 4
	n = 24
	fmt.Println(k, n)
	res := [][]int{}
	used := make([]bool, 9)
	for i := 1; i <= 9; i++ {
		tmp := []int{i}
		used[i-1] = true
		backtrack216(&res, tmp, used, k-1, n-i)
		// used[i-1] = false
	}

	for _, v := range res {
		fmt.Println(v)
	}
}

func backtrack216(res *[][]int, nums []int, used []bool, k int, n int) {
	if k < 0 || n < 0 {
		return
	}
	if k == 0 {
		if n == 0 {
			tmp := make([]int, len(nums))
			copy(tmp, nums)
			// fmt.Println(nums)
			*res = append(*res, tmp)
			return
		} else {
			return
		}
	}
	for i, v := range used {
		if v != true && i+1 > nums[len(nums)-1]{
			used[i] = true
			nums = append(nums, i+1)
			backtrack216(res, nums, used, k-1, n-i-1)
			nums = nums[:len(nums)-1]
			used[i] = false
		}
	}
}

func TestSlice(t *testing.T) {
	results := [][]int{}
	nums := []int{1, 2, 3}
	results = append(results, nums)
	nums[1] = 8
	fmt.Println(results)
}