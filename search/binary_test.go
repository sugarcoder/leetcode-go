package search

import (
	"fmt"
	"math"
	"sort"
	"testing"
	// "gitee.com/sugarcoder/leetcode-go/util"
)

func TestDivideTwoIntegers(t *testing.T) {
	fmt.Println(divide(7, 3)) // 这个测试用例说明for循环必须使用 left <= right
}

// 不使用除法来实现整数除法
// divisor 不会是0
func divide(dividend int, divisor int) int {
	// 判断被除数、除数为INT_MIN时候
	if dividend == math.MinInt32 {
		if divisor == 1 {
			return math.MinInt32
		} else if divisor == -1 { // 出现溢出
			return math.MaxInt32
		}
	}
	if divisor == math.MinInt32 {
		if dividend == math.MinInt32 {
			return 1
		}
		return 0 // 任何数除以最小的负数都在(-1, 1)之间
	}
	// 将两个数都调整为负数，防止溢出
	var revert bool = false // 记录最终结果的符号
	if divisor > 0 {
		divisor = -divisor
		revert = !revert
	}
	if dividend > 0 {
		dividend = -dividend
		revert = !revert
	}

	// 使用二分法找 x， 使得 (x * divisor) >= dividend > ((x+1) * divisor)
	// 注意上面的公式的大于号方向 是因为 除数和被除数已经是负的了
	// 要获得左边界
	var left, right int = 0, math.MaxInt32
	var ans int = 0
	watch := 0
	for left <= right {
		mid := left + ((right - left) >> 1)
		fmt.Printf("[%d, %d], mid=%d\n", left, right, mid)
		if quickAdd(mid, divisor, dividend) {
			ans = mid
			if ans == math.MaxInt32 {
				return ans
			}
			left = mid + 1
		} else {
			right = mid - 1
		}
		watch++
		if watch > 100 {
			return -1
		}
	}
	if revert {
		ans = -ans
	}
	return ans
}

// 0------------>
//       |
// true  x(true)  false

// if x * y >= z return true
// 其中 x > 0, y < 0, z < 0
// 用加法实现乘法
func quickAdd(x, y, z int) bool {
	tmp := 0
	add := y
	for x > 0 {
		if x&1 > 0 {
			if tmp < z-add {
				return false
			}
			tmp += add
		}
		if x != 1 {
			if add+add < z {
				return false
			}
			add += add
		}
		x >>= 1
	}
	return true
}

func TestMinimumInRotatedSortedArray(t *testing.T) {
	nums := []int{4, 5, 6, 7, 0, 1, 2}
	left, right := 0, len(nums)-1
	minimum := math.MaxInt32
	for left <= right {
		mid := left + ((right - left) >> 1)
		fmt.Printf("left:%d, right:%d, mid:%d\n", left, right, mid)
		if minimum > nums[mid] {
			minimum = nums[mid]
		}
		if nums[mid] < nums[right] {
			right = mid
		} else {
			left = mid + 1
		}
		// if nums[left] <= nums[mid] && nums[mid] < nums[right] {
		// 	right = mid
		// } else if nums[mid] >= nums[left] && nums[mid] > nums[right] {
		// 	left = mid + 1
		// } else if nums[mid] < nums[right] && nums[mid] <= nums[left] {
		// 	right = mid
		// } else {
		// 	panic("Wrong input array")
		// }
	}
	fmt.Println("minimum", minimum)
}

func TestMinimumInRotatedSortedArrayII(t *testing.T) {
	nums := []int{1, 3, 3}
	left, right := 0, len(nums)-1
	minimum := math.MaxInt32
	watch := 0
	for left <= right {
		mid := left + ((right - left) >> 1)
		fmt.Printf("left:%d, right:%d, mid:%d\n", left, right, mid)
		if minimum > nums[mid] {
			minimum = nums[mid]
		}
		// compare with right
		// if nums[mid] < nums[right] {
		// 	right = mid
		// } else if nums[left] <= nums[mid] {
		// 	left = mid + 1
		// } else {
		// 	right = mid - 1
		// }
		// compare with left
		if nums[left] < nums[right] {
			minimum = nums[left]
			break
		}
		if nums[left] > nums[mid] {
			right = mid
			left++
		} else if nums[mid] > nums[left] {
			left = mid + 1
		} else {
			left += 1
		}
		watch += 1
		if watch > 10 {
			break
		}
	}
	fmt.Println("minimum", minimum)
}

// 162, return any peak element index
func TestFindPeak(t *testing.T) {
	nums := []int{1, 2, 3, 2, 1}
	left, right := 0, len(nums)-1
	var ans int
	watch := 0
	get := func(index int) int {
		if index == -1 || index == len(nums)-1 {
			return math.MinInt64
		}
		return nums[index]
	}
	for left <= right {
		mid := left + ((right - left) >> 1)
		fmt.Printf("left:%d, right:%d, mid:%d\n", left, right, mid)
		if get(mid) > get(mid-1) && get(mid) > get(mid+1) {
			ans = mid
			break
		}
		// mid的左右是单调的
		// 往高处走
		if nums[mid] > nums[mid+1] {
			right = mid - 1
		} else {
			left = mid + 1
		}
		if watch++; watch > 10 {
			break
		}
	}
	fmt.Printf("nums[%d]=%d\n", ans, nums[ans])
}

// 162, left bound method
func TestLeftBound(t *testing.T) {
	nums := []int{1, 2, 3, 4}
	left, right := 0, len(nums)-1
	var mid int
	watch := 0
	for left < right {
		mid = left + ((right - left) >> 1)
		fmt.Printf("left:%d, right:%d, mid:%d\n", left, right, mid)
		if nums[mid] > nums[mid+1] {
			right = mid
		} else { // nums[mid] < nums[mid+1]
			left = mid + 1
		}
		if watch++; watch > 10 {
			break
		}
	}
	fmt.Println(left)
}

// 33 Search in the rotated array
func TestSearchInTheRotatedArray(t *testing.T) {
	nums := []int{4, 5, 6, 7, 0, 1, 2}
	target := -1
	target2 := 9
	for _, v := range nums {
		fmt.Println(searchInRotate(nums, v))
	}
	fmt.Println(searchInRotate(nums, target))
	fmt.Println(searchInRotate(nums, target2))
}

func searchInRotate(nums []int, target int) int {
	left, right := 0, len(nums)-1
	for left <= right {
		mid := left + ((right - left) >> 1)
		fmt.Printf("left:%d, right:%d, mid:%d\n", left, right, mid)
		if nums[mid] == target {
			return mid
		} else if nums[left] <= nums[right] {
			if nums[mid] < target {
				left = mid + 1
			} else {
				right = mid - 1
			}
		} else {
			// 包含旋转位的[left, right]区间
			if nums[mid] >= nums[0] {
				// mid 在左半边
				if target >= nums[0] {
					// target 也在左半边
					if target < nums[mid] {
						right = mid - 1
					} else {
						left = mid + 1
					}
				} else {
					// target在右半边
					left = mid + 1
				}
			} else {
				// mid 在右半边
				if target >= nums[0] {
					// target在左半边
					right = mid - 1
				} else {
					// target在右半边
					if target < nums[mid] {
						right = mid - 1
					} else {
						left = mid + 1
					}
				}
			}
		}
	}
	return -1
}

// 34. Find First and Last Position of Element in Sorted Array
func TestFindFirstLast(t *testing.T) {
	fmt.Println(searchRange([]int{5, 7, 7, 8, 8, 10}, 8))
}

func searchRange(nums []int, target int) []int {
	ans := []int{}
	// lowerbound
	idx := lowerbound(nums, target)
	if idx != -1 {
		ans = append(ans, idx)
		idx = upperbound(nums, target)
		ans = append(ans, idx)
		return ans
	} else {
		return []int{-1, -1}
	}
	// upperbound
}

// 找左边界
func lowerbound(nums []int, target int) int {
	// 找第一个大于等于target的位置
	left, right := 0, len(nums)-1
	ans := len(nums)
	for left <= right {
		mid := left + (right-left)/2
		fmt.Printf("left:nums[%d]=%d, right:nums[%d]:%d, mid:nums[%d]=%d\n", left, nums[left], right, nums[right], mid, nums[mid])
		if nums[mid] >= target {
			right = mid - 1
			ans = mid
		} else {
			left = mid + 1
		}
	}
	if ans == len(nums) || nums[ans] != target {
		return -1
	}
	return ans
}

func upperbound(nums []int, target int) int {
	ans := len(nums)
	left, right := 0, len(nums)-1
	for left <= right {
		mid := left + ((right - left) >> 1)
		if nums[mid] <= target {
			ans = mid
			left = mid + 1
		} else {
			right = mid - 1
		}
	}
	if ans == len(nums) || nums[ans] != target {
		return -1
	}
	return ans
}

// 875
func TestMinEatingSpeed(t *testing.T) {
	piles := []int{3, 6, 7, 11}
	h := 8
	fmt.Println(minEatingSpeed(piles, h))
}

func minEatingSpeed(piles []int, h int) int {
	lower, upper := 1, 0
	for _, v := range piles {
		upper = max(upper, v)
	}
	for lower < upper {
		k := lower + (upper-lower)/2
		t := tryEat(piles, k)
		if t > h { // 耗时过多，需要吃更多香蕉
			lower = k + 1
		} else {
			upper = k
		}
	}
	return lower
}

// 每次最多吃k个香蕉，返回需要的小时数
func tryEat(piles []int, k int) int {
	h := 0
	for _, v := range piles {
		h += (v + k - 1) / k
	}
	return h
}

// LCP 12
func TestMinTimeZhang(t *testing.T) {
	time := []int{1, 2, 3}
	m := 2
	fmt.Println(minTime(time, m))
}

func minTime(time []int, m int) int {
	maxVal, sum := 0, 0
	lower := 0
	for _, v := range time {
		maxVal = max(maxVal, v)
		sum += v
	}
	upper := sum - maxVal // 每天做一题的时间，一天做完所有题且小杨帮忙做最难的一题
	for lower < upper {
		t := lower + (upper-lower)/2
		fmt.Printf("lower=%d, upper=%d, LimitT(mid)=%d\n", lower, upper, t)
		day := tryDoLeetCode(time, t)
		fmt.Printf("Got day=%d\n", day)
		if day > m { // 耗费天数过长，每天做题时间不够
			lower = t + 1
		} else {
			upper = t
		}
	}
	return lower
}

// 每天最多花t小时做题，返回需要的天数
func tryDoLeetCode(time []int, t int) int {
	day := 1 // 注意
	curTotal := 0
	curMax := 0
	for _, v := range time {
		nextTime := min(curMax, v) // 选择较小的数加入到总时间
		if curTotal+nextTime <= t {
			curTotal += nextTime
			curMax = max(v, curMax)
		} else {
			day += 1
			curTotal = 0
			curMax = v
		}
	}
	return day
}

// 1482
func TestMinDays(t *testing.T) {
	bloomDay := []int{1, 10, 3, 10, 2}
	m := 3
	k := 1
	fmt.Println(minDays(bloomDay, m, k))
}

// 需要采m束，每束需要k朵相邻的花
// 返回需要等待的最小天数
func minDays(bloomDay []int, m int, k int) int {
	if m*k > len(bloomDay) {
		return -1
	}
	lower, upper := 1, 0 // 等待天数的上下限
	for _, v := range bloomDay {
		upper = max(upper, v) // 等待花全开
	}
	for lower < upper {
		day := lower + (upper-lower)/2
		if tryPick(bloomDay, day, m, k) {
			upper = day
		} else {
			lower = day + 1
		}
	}
	return lower
}

// 等待day天后，尝试采摘
func tryPick(bloomDay []int, day int, m int, k int) bool {
	bunch := 0
	count := 0
	for _, v := range bloomDay {
		if v <= day {
			count += 1
			if count >= k {
				bunch += 1
				count = 0
			}
		} else {
			count = 0
		}
	}
	return bunch >= m
}

// 1552
func TestMaxDistance(t *testing.T) {
	position := []int{1, 2, 3, 4, 7}
	m := 3
	fmt.Println(maxDistance(position, m))
	// position := []int{5,4,3,2,1,1000000000}
	// m := 2
	// fmt.Println(maxDistance(position, m))
}

func maxDistance(position []int, m int) int {
	sort.Ints(position)                                                            // sort in increasing order
	lower, upper := position[1]-position[0], position[len(position)-1]-position[0] // minimum force between any ball pairs
	for i := 1; i < len(position); i++ {
		lower = min(lower, position[i]-position[i-1])
	}
	ans := -1
	// loopLimit := 10
	for lower <= upper {
		force := lower + (upper-lower)/2
		ballNum := tryPlaceWithForce(position, force)
		fmt.Printf("lower=%d, upper=%d, mid=%d, canPlaceBall=%d\n", lower, upper, force, ballNum)
		if ballNum < m {
			// 可放置的球数太少，需要减小force上限
			upper = force - 1
			fmt.Printf("too little balls, update upper=%d\n", upper)
		} else {
			// 可放置的球数大于等于m，需要增大force
			// 同时找force的upperbound
			ans = force // 至少是force
			lower = force + 1
			fmt.Printf("save ans=%d, update lower=%d\n", ans, lower)
		}
		// loopLimit--
		// if loopLimit == 0 {
		// 	break
		// }
	}
	return ans
}

//	find this
//	    V         force
//
// ------------------------->
// |         |    |      |
// false   true--true  false
// 用左闭右开的方式来写二分
func maxDistance2(position []int, m int) int {
	sort.Ints(position) // 注意先排序
	// 注意upper的取值
	lower, upper := position[1]-position[0], position[len(position)-1]-position[0]+1 // minimum force between any ball pairs
	for i := 1; i < len(position); i++ {
		lower = min(lower, position[i]-position[i-1])
	}
	ans := -1 // 一定要用这个ans来保存
	// 考虑区间[lower, upper)
	for lower < upper {
		force := lower + (upper-lower)/2
		ballNum := tryPlaceWithForce(position, force)
		if ballNum < m {
			// 可放置的球数太少，需要减小force上限
			upper = force
		} else {
			// 可放置的球数大于等于m，需要增大force
			// 同时找force的upperbound
			ans = force
			lower = force + 1
		}
	}
	return ans // 不能返回lower
}

// 返回可以以最小距离minForce放置的球数
func tryPlaceWithForce(position []int, minForce int) int {
	lastIdx := 0
	ballNum := 1
	for i := 1; i < len(position); i++ {
		if position[i]-position[lastIdx] >= minForce {
			lastIdx = i
			ballNum += 1
		}
	}
	return ballNum
}

// 436
func TestFindRightInterval(t *testing.T) {
	// util.ConvertBrackets2Braces("[[3,4],[2,3],[1,2]]")
	intervals := [][]int{{3, 4}, {2, 3}, {1, 2}}
	fmt.Println(findRightInterval(intervals))
}

func findRightInterval(intervals [][]int) []int {
	ht := make(map[int]int)
	for i, itv := range intervals {
		ht[itv[0]] = i // remember the origin index
	}
	sort.Slice(intervals, func(i, j int) bool {
		return intervals[i][0] < intervals[j][0]
	})
	ans := make([]int, len(intervals))
	for i, itv := range intervals {
		end := itv[1]
		left, right := i, len(intervals)
		for left < right {
			mid := left + ((right - left) >> 1)
			if intervals[mid][0] < end {
				left = mid + 1
			} else {
				right = mid
			}
		}
		origin_idx := ht[itv[0]]
		if left == len(intervals) {
			ans[origin_idx] = -1
		} else {
			ans[origin_idx] = ht[intervals[left][0]]
		}
	}
	return ans
}

// 441 Arranging coins
func TestArrangeCoins(t *testing.T) {
	fmt.Println(arrangeCoins(3))
}

func arrangeCoins(n int) int {
    left, right := int(math.Sqrt(float64(n))), (n+1) / 2
    guess := right
    for left <= right {
        mid := left + (right - left) / 2
        if countLevel(mid) <= n {
            guess = mid
            left = mid + 1
        } else {
            right = mid - 1
        }
    }
    return guess
}

func countLevel(level int) int {
    return level * (level + 1) / 2
}