package search

import (
	"fmt"
	"testing"
)

const FirstBadVersion int = 10

// 0---------------------->
//            |
//   false   true  true 
// 278.
func isBadVersion(num int) bool {
	if num < FirstBadVersion {
		return false
	} else {
		return true
	}
}

func TestBinary(t *testing.T) {
	n := 2
	left := 1
    right := n
	quit := 0
	fmt.Printf("[%d, %d]\n", left, right)
    for left < right {
        guess := ((right - left) >> 1) + left
        if isBadVersion(guess) {
            right = guess
        } else {
            left = guess + 1 // 不是中位数
        }
		fmt.Printf("guess:%d, [%d, %d]\n", guess, left, right)
		quit += 1
		if quit > 10 {
			return
		}
    }
	fmt.Println(right)
}

// 标准写法
func TestBinaryMid(t *testing.T) {
	n := 10
	left := 1
    right := n
	quit := 0
	ans := 0
	fmt.Printf("[%d, %d]\n", left, right)
    for left <= right {
        guess := ((right - left) >> 1) + left
		fmt.Printf("guess:%d, [%d, %d]\n", guess, left, right)
        if isBadVersion(guess) {
			ans = guess
            right = guess - 1
        } else {
            left = guess + 1 // 不是中位数
        }
		quit += 1
		if quit > 10 {
			return
		}
    }
	fmt.Println(ans)
}


func TestTwoSortedArrayMedium(t *testing.T) {
	nums1 := []int{1, 3}
	nums2 := []int{2}
	m := len(nums1)
	n := len(nums2)
	isEven := (m + n) % 2 == 0
	midIndex := (m + n) / 2  // 中位数的下标（started at 0)

	if isEven {
		fmt.Println(float32(getKthElement(nums1, nums2, midIndex) + getKthElement(nums1, nums2, midIndex+1)) / 2.0)
	} else {
		fmt.Println(getKthElement(nums1, nums2, midIndex + 1))  // 下标为2， 即为第3个元素
	}
}

// 第k个元素
// k >= 1
func getKthElement(nums1 []int, nums2 []int, k int) int {
	for {
		if len(nums1) == 0 {
			return nums2[k-1]
		}
		if len(nums2) == 0 {
			return nums1[k-1]
		}
		if k == 1 {
			return Min(nums1[0], nums2[0])
		}
		half := k / 2
		checkIndex1 := Min(half, len(nums1)) - 1
		checkIndex2 := Min(half, len(nums2)) - 1
		if nums1[checkIndex1] <= nums2[checkIndex2] {
			k -= checkIndex1 + 1  // 减去被排除的数量
			nums1 = nums1[checkIndex1+1:]
		} else  {
			k -= checkIndex2 + 1
			nums2 = nums2[checkIndex2+1:]
		}
	}
}

func Min(a ...int) int {
	min := a[0]
	for i:=1; i < len(a); i++ {
		if a[i] < min {
			min = a[i]
		}
	}
	return min
}

func TestIntSlice(t *testing.T) {
	s := fmt.Sprintf("->%d", 12)
	fmt.Println(s)
	nums := []int{1, 2}
	changing(nums)
	fmt.Println(nums)
	fmt.Printf("in main, len=%d, cap=%d\n", len(nums), cap(nums))
}

func changing(nums []int) {
	nums[0] = 4  // can change the origin slice
	nums = append(nums, 4) // but here give a new slice, no effect on origin slice
	fmt.Printf("in changing, len=%d, cap=%d\n", len(nums), cap(nums))
}

// 167 Two Sum, input array sorted
func TestTwoSumSortedArray(t *testing.T) {
	numbers := []int{3,24,50,79,88,150,345}
	target := 200
	
	left, right := 0 , len(numbers)-1
	for left < right {
		curSum := numbers[left] + numbers[right]
		if curSum == target {
			fmt.Printf("left=%d, right=%d\n", left, right)
			return
		} else if curSum < target {
			left++
		} else if curSum > target {
			right--
		}
	}
}