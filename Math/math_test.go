package Math

import (
	// "strconv"
	"fmt"
	"math"
	"testing"
)

func isPalindrome(x int) bool {
	if x < 0 || (x%10 == 0 && x != 0) {
		return false
	}

	revertedNumber := 0
	for x > revertedNumber {
		revertedNumber = revertedNumber*10 + x%10
		x /= 10
	}

	// 当数字长度为奇数时，我们可以通过 revertedNumber/10 去除处于中位的数字。
	// 例如，当输入为 12321 时，在 while 循环的末尾我们可以得到 x = 12，revertedNumber = 123，
	// 由于处于中位的数字不影响回文（它总是与自己相等），所以我们可以简单地将其去除。
	return x == revertedNumber || x == revertedNumber/10
}

func TestFractionToDecimal166(t *testing.T) {
	var numerator, denominator int = 1, 6 // -50, 8 // 7, -12
	fmt.Println(float32(numerator) / float32(denominator))
	if numerator == 0 {
		fmt.Println("0")
	} else if numerator == math.MinInt32 && denominator == -1 {
		fmt.Printf("%d", math.MaxInt32+1)
	}
	var answer string
	remainder := numerator % denominator
	answer += fmt.Sprintf("%d", numerator/denominator)
	if numerator > 0 && denominator < 0 {
		answer = "-" + answer
	}
	if remainder != 0 {
		answer += "."
	}

	record := make(map[int]int) // map remainder -> index in the answer
	record[remainder] = len(answer)
	///
	watch := 0
	///
	for remainder != 0 {
		tempRemainder := remainder * 10
		for tempRemainder/denominator == 0 {
			tempRemainder *= 10
			answer += "0"
		}
		digit := tempRemainder / denominator
		remainder = tempRemainder % denominator
		answer += fmt.Sprintf("%d", int(math.Abs(float64(digit))))
		if ind, ok := record[remainder]; ok {
			fmt.Println("ind", ind)
			answer = answer[:ind] + "(" + answer[ind:] + ")"
			break
		}
		record[remainder] = len(answer)
		fmt.Printf("Add record[%d]=%d\n", remainder, len(answer))
		if watch++; watch > 10 {
			break
		}
	}
	fmt.Println(answer)
}

func TestDivision(t *testing.T) {
	fmt.Println(7 % -12)
}

func TestFactorialTrailingZeros172(t *testing.T) {
	n := 11
	fac := 1
	str := "1"
	for i := 2; i <= n; i++ {
		fac *= i
		fmt.Printf("%s*%d = %d\n", str, i, fac)
		str += fmt.Sprintf("*%d", i)
	}
}

// 233
func TestCountDigitOne(tt *testing.T) {
	n := 13

	count := 0
	place := 1
	num := n
	for num > 0 {
		digit := num % 10
		if digit == 0 {
			count += (num / 10) * place
		} else if digit == 1 {
			count += (num/10)*place + (n % place) + 1
		} else {
			// 取上界，因为出现了完整的0~9循环
			count += ((num + 9) / 10) * place
		}
		num /= 10
		place *= 10
	}
	fmt.Println(count)
}


func TestIsSelfCrossing(t *testing.T) {
	// distance := []int{2,1,1,2}
	// distance := []int{1,2,3,4}
	// distance := []int{1,1,2,1,1} // 追尾情况
	distance := []int{1, 1, 2, 2, 1, 1} // 侧面追击
	fmt.Println(isSelfCrossing(distance))
	// fmt.Println(distance)
}

// 335
func isSelfCrossing(distance []int) bool {
	// distance[1] = 9 // work
	for i := 3; i < len(distance); i++ {
		// 第一类碰撞
		if distance[i] >= distance[i-2] && distance[i-1] <= distance[i-3] {
			return true
		}
		// 追尾碰撞
		if i == 4 && distance[i]+distance[i-4] >= distance[i-2] &&
			distance[3] == distance[1] {
			return true
		}
		// 侧面追击
		if i >= 5 && distance[i-1] >= distance[i-3]-distance[i-5] && distance[i-1] <= distance[i-3] &&
			distance[i] >= distance[i-2]-distance[i-4] &&
			distance[i-2] >= distance[i-4] {
			return true
		}
	}
	return false
}

// leetcode 342
func TestIsPowerOfFour(t *testing.T) {
	// n := 16 // 4 ^ x == n, x == 2
	fmt.Println(0b10 >> 2)
	for x := 0; x < 10; x++ {
		mi := int(math.Pow(4, float64(x)))
		fmt.Printf("%d:%b\n", mi, mi)
	}
}

// 365
func TestWaterAndJug(t *testing.T) {
	// fmt.Println(canMeasureWater(3, 5, 4))
	// fmt.Println(canMeasureWater(2, 6, 5))
	// fmt.Println(canMeasureWater(1, 2, 3))
	// fmt.Println(canMeasureWater(4, 6, 8))
	// fmt.Println(canMeasureWater(34, 5, 6))
	fmt.Println(5 % 10)
}

type RemainState struct {
	x, y int
}

func canMeasureWater(jug1Capacity int, jug2Capacity int, targetCapacity int) bool {
	visited := make(map[RemainState]bool) // remainX-remainY
	stack := make([]RemainState, 0)
	// visited[RemainState{0, 0}] = true
	stack = append(stack, RemainState{0, 0})
	for len(stack) > 0 {
		remain := stack[len(stack)-1]
		stack = stack[:len(stack)-1]
		if remain.x == targetCapacity || remain.y == targetCapacity || remain.x + remain.y == targetCapacity {
			return true
		}
		if _, ok := visited[remain]; ok {
			continue
		}
		visited[remain] = true
		// 把Y壶灌满
		stack = append(stack, RemainState{remain.x, jug2Capacity})
		// 把X壶灌满
		stack = append(stack, RemainState{jug1Capacity, remain.y})
		// 把X壶倒空
		stack = append(stack, RemainState{0, remain.y})
		// 把Y壶倒空
		stack = append(stack, RemainState{remain.x, 0})
		// 把X壶中的水倒入Y壶，直到X为空或者Y满
		stack = append(stack, RemainState{remain.x - min(remain.x, jug2Capacity - remain.y), remain.y + min(jug2Capacity-remain.y, remain.x)})
		// 把Y壶中的水倒入X壶，直到Y位空或者X满
		stack = append(stack, RemainState{remain.x + min(jug1Capacity - remain.x, remain.y), remain.y - min(remain.y, jug1Capacity - remain.x)})
	}
	return false
}


// 372
func TestSuperPow(t *testing.T) {
	fmt.Println(superPower(2, []int{4, 2}))
	fmt.Println(int(myPow(2, 42)) % 1337)
}

func superPower(a int, b []int) int {
	ans := 1
	mod := 1337
	for i := len(b) - 1; i >= 0; i-- {
		ans = ans * pow(a, b[i], mod) % mod
		a = pow(a, 10, mod)
	}
	return ans
}

func superPowerQin(a int, b []int) int {
	ans := 1
	mod := 1337
	for _, v := range b {
		ans = pow(ans, 10, mod) * pow(ans, v, mod) % mod
	}
	return ans
}

// n >= 0, 取mod
// x的贡献次数刚好是n的二进制表示的中的每个1对应的2次方值
func pow(x, n, mod int) int {
	res := 1
	for ;n > 0; n /= 2 {
		if n & 1 == 1 {
			res = res * x % mod
		}
		x = x * x % mod
	}
	return res
}

// 50: 计算x^n
func myPow(x float64, n int) float64 {
	if n > 0 {
		return quickMul(x, n)
	} else {
		return 1.0 / quickMul(x, n)
	}
}

func quickMul(x float64, n int) float64 {
	if (n == 0) {
		return 1
	}
	y := quickMul(x, n / 2)
	if n % 2 == 0 {
		return y * y
	} else {
		return y * y * x
	}
}