package cache

import (
	"testing"
	"fmt"
)

func TestLRUCache(t *testing.T) {
	cache := LRUCacheConstructor[string, int](3)
	cache.Put("abc", 2)
	cache.Put("opu", 3)
	cache.Put("ccc", 1)
	cache.Put("aaa", 3)
	fmt.Println(cache.Get("abc"))
}

func TestLFUCache(t *testing.T) {
	cache := LFUCacheConstructor[string, int](3)
	cache.Put("abc", 2)
	cache.Put("opu", 3)
	cache.Put("ccc", 1)
	cache.Put("abc", 3)
	cache.Put("bbb", 1)
	fmt.Println(cache.Get("ccc"))
}