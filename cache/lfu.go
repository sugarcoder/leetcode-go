package cache

import "container/list"

type LFUCache[K comparable, V ValueType] struct {
	Nodes map[K]*list.Element
	Lists map[int]*list.List  // mapping [access count] --> elementsList
	Capacity int
	Min int
}

type node[K comparable, V ValueType] struct {
	key K
	value V
	frequency int
}

func LFUCacheConstructor[K comparable, V ValueType](cap int) LFUCache[K, V] {
	return LFUCache[K, V] {
		Nodes: make(map[K]*list.Element),
		Lists: make(map[int]*list.List),
		Capacity: cap,
		Min: 0,
	}
}

func (c *LFUCache[K, V]) Get(key K) V {
	value, ok := c.Nodes[key]
	if !ok {
		var null V
		return null
	}
	currentNode := value.Value.(*node[K, V])
	c.Lists[currentNode.frequency].Remove(value)
	currentNode.frequency++

	if _, ok := c.Lists[currentNode.frequency]; !ok {
		c.Lists[currentNode.frequency] = list.New()
	}
	newList := c.Lists[currentNode.frequency]
	newNode := newList.PushFront(currentNode)
	c.Nodes[key] = newNode
	if currentNode.frequency - 1 == c.Min && c.Lists[currentNode.frequency-1].Len() == 0 {
		c.Min++
	}
	return currentNode.value
}

func (c *LFUCache[K, V]) Put(key K, value V) {
	if c.Capacity == 0 {
		return
	}
	// 已经存在，增加访问次数
	if currentValue, ok := c.Nodes[key]; ok {
		currentNode := currentValue.Value.(*node[K, V])
		currentNode.value = value // 更新值
		c.Get(key) // 更新cache
		return 
	}
	// 新的值，但是缓存满了
	if c.Capacity == len(c.Nodes) {
		currentList := c.Lists[c.Min]
		backNode := currentList.Back()
		delete(c.Nodes, backNode.Value.(*node[K, V]).key)
		currentList.Remove(backNode)
	}
	// create new node
	c.Min = 1
	currentNode := &node[K, V] {
		key: key,
		value: value,
		frequency: 1,
	}
	if _, ok := c.Lists[1]; !ok {
		c.Lists[1] = list.New()
	}
	newNode := c.Lists[1].PushFront(currentNode)
	// newList := c.Lists[1]
	// newNode := newList.PushFront(currentNode)
	c.Nodes[key] = newNode
}