package cache

import "container/list"

type ValueType interface {
	int | string
}

type LRUCache[K comparable, V ValueType] struct {
	Cap int
	Keys map[K]*list.Element
	List *list.List
}

// Value of list.Element
type pair[K comparable, V ValueType] struct {
	Key K
	Value V
}

func LRUCacheConstructor[K comparable, V ValueType](cap int) LRUCache[K, V] {
	return LRUCache[K, V] {
		Cap: cap,
		Keys: make(map[K]*list.Element),
		List: list.New(),
	}
}

func (c *LRUCache[K, V]) Get(key K) V {
	if el, ok := c.Keys[key]; ok {
		c.List.MoveToFront(el)
		return el.Value.(pair[K, V]).Value
	}
	var null V
	return null
}

func (c *LRUCache[K, V]) Put(key K, value V) {
	if el, ok := c.Keys[key]; ok {
		// el.Value.(pair[K,V]).Value = value // cannot assign
		el.Value = pair[K, V]{Key: key, Value: value}
		c.List.MoveToFront(el)
	} else {
		el := c.List.PushFront(pair[K,V]{Key: key, Value: value})
		c.Keys[key] = el
	}
	if c.List.Len() > c.Cap {
		el := c.List.Back()
		c.List.Remove(el)
		delete(c.Keys, el.Value.(pair[K, V]).Key)
	}
}