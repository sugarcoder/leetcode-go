package greedy

import (
	"strconv"
	"testing"
	"fmt"
	"sort"
)

func TestLongestPalindrome(t *testing.T) {
	fmt.Println(isPalindrome("abcba"))
	fmt.Println(isPalindrome("aacca"))
	// 409题选取的是能构成最长回文串的字符数量
	fmt.Printf("s[i] type %T", "abc"[1])
	count := make(map[uint8]int)
	s := "abccccdd"
	// for _, c := range s {
	// 	count[uint8(c)] += 1
	// }
	for i := 0; i < len(s); i++ {
		count[s[i]] += 1
	}

	ans := 0
	first_odd := true
	for _, v := range count {
		if v % 2 == 0 {
			ans += v
		} else {
			if first_odd {
				ans += v
				first_odd = false
			} else {
				ans += (v -1)
			}
		}
	}
	fmt.Println("The longest palidrome can built by", ans)
}

func isPalindrome(s string) bool {
	for i := 0; i < len(s) / 2; i++ {
		if s[i] != s[len(s)-1-i] {
			return false
		}
	}
	return true
}

// 179
func TestMaxNumber(t *testing.T) {
	// fmt.Println(avgValOfDigits(34323))
	// fmt.Println(avgValOfDigits(3432))
	nums := []int{3,30,34,5,9}
	sort.Slice(nums, func (i, j int) bool { 
		return (fmt.Sprintf("%d", nums[i]) + fmt.Sprintf("%d", nums[j])) > (fmt.Sprintf("%d", nums[j]) + fmt.Sprintf("%d", nums[i])) 
	})
    ans := ""
    for _, val := range nums {
        ans += fmt.Sprintf("%d", val)
    }
	fmt.Println(ans)
	strconv.Itoa(5)
	fmt.Println(strconv.Atoi(ans))
}

func avgValOfDigits(val int) float32 {
    sum := 0
    count := 0
    for val != 0 {
        sum += val % 10
        val /= 10
        count += 1
    }
    return float32(sum) / float32(count)
}

func TestStringComp(t *testing.T) {
	s1 := "123"
	s2 := "321"
	fmt.Println(s1 > s2)
}