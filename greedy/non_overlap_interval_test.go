package greedy

import (
	"fmt"
	"sort"
	"testing"
	// "gitee.com/sugarcoder/leetcode-go/util"
)

// 435
func TestNonOverlap(t *testing.T) {
	// util.ConvertBrackets2Braces("[[1,2],[2,3],[3,4],[1,3]]")
	ints := [][]int{{1, 2}, {2, 3}, {3, 4}, {1, 3}}
	// fmt.Println(eraseOverlapIntervals(ints))
	fmt.Println(eraseOverlapIntervalsGreedy(ints))
}

func eraseOverlapIntervals(intervals [][]int) int {
	// 返回 （len(intervals) - 不重叠区间的最大个数）
	// sort the intervals with start point
	sort.Slice(intervals, func(i, j int) bool {
		return intervals[i][0] < intervals[j][0]
	})
	dp := make([]int, len(intervals)) // dp[i] 表示以区间i结尾时，不重叠区间的最大个数
	for i := range dp {
		dp[i] = 1 // 以自己结尾时，区间数量初始为1
	}
	// dp[i] = max(dp[k] + 1) for k in (0..i)，if intervals[k][1] <= intervals[i][1]
	for i := 0; i < len(intervals); i++ {
		for k := 0; k < i; k++ {
			if intervals[k][1] <= intervals[i][0] {
				dp[i] = max(dp[i], dp[k]+1)
			}
		}
	}
	maxCount := 0
	for i := range dp {
		maxCount = max(maxCount, dp[i])
	}
	return len(intervals) - maxCount
}

func eraseOverlapIntervalsGreedy(intervals [][]int) int {
	if len(intervals) == 0 {
		return 0
	}
	sort.Slice(intervals, func(i, j int) bool {
		return intervals[i][1] < intervals[j][1]
	})
	right := intervals[0][1] // 区间值可能是负数
	cnt := 1
	for _, intv := range intervals[1:] {
		if intv[0] >= right {
			right = intv[1]
			cnt++
		}
	}
	return len(intervals) - cnt
}
