package courseschedule

import (
	"container/heap"
	"fmt"
	"sort"
	"testing"

	"gitee.com/sugarcoder/leetcode-go/util"
)

type PriorityQueue struct {
	sort.IntSlice
}

// 大根堆
// Overwrite the Less
func (pq PriorityQueue) Less(i, j int) bool {
	return pq.IntSlice[i] > pq.IntSlice[j]
}

func (pq *PriorityQueue) Push(x any) {
	course := x.(int)
	pq.IntSlice = append(pq.IntSlice, course)
}

func (pq *PriorityQueue) Pop() any {
	n := len(pq.IntSlice)
	item := pq.IntSlice[n-1]
	pq.IntSlice = pq.IntSlice[:n-1]
	return item
}

func TestPRQ(t *testing.T) {
	pq := PriorityQueue{IntSlice: []int{3, 5, 6,1, 2}}
	heap.Init(&pq)
	// heap.Push(&pq, 1)
	fmt.Println(heap.Pop(&pq).(int))
}

// 630
func scheduleCourse(courses [][]int) int {
	sort.Slice(courses, func(i, j int) bool { return courses[i][1] < courses[j][1] })
	pq := PriorityQueue{}
	curDay := 0
	for _, course := range courses {
		dur, lastday := course[0], course[1]
		if curDay + dur <= lastday {
			heap.Push(&pq, dur)
			curDay += dur
		} else if pq.Len() > 0 && dur < pq.IntSlice[0] {
			curDay -= pq.IntSlice[0]
			curDay += dur
			pq.IntSlice[0] = dur
			heap.Fix(&pq, 0)
		}
	}
	return pq.Len()
}

func TestScheduleCourse(t *testing.T) {
	courses := [][]int{
		{100,200},{200,1300},{1000,1250},{2000,3200},
	}
	fmt.Println(scheduleCourse(courses))
}

// 45
func jump(nums []int) int {
	// 选择下一步能跳的更远的位置进行跳跃
	step := 0 // 跳跃次数
	max_position := 0
	end := 0
	for i := 0; i < len(nums) - 1; i++ {
		max_position = util.Max(max_position, i + nums[i])
		if i == end { // why i == end
			step++
			end = max_position
			max_position = nums[i]
		}
	}
	return step
}

func TestJumpII(t *testing.T) {
	fmt.Println(jump([]int{2,3,1,1,4}))
}