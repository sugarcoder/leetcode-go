package bitman

import (
	"fmt"
	"math/bits"
	"testing"
    // "math"
)

func TestReverseBits(t *testing.T) {
	var num uint32 = 0b00000010100101000001111010011100
	var result uint32 = 0
	var mask uint32 = 1
	for i := 0; i < 32; i++ {
		result <<= 1
		result |= ((num & mask) >> i)
		// fmt.Printf("result=%b", result)
		mask <<= 1
	}
	fmt.Println(result)
	// fmt.Println(num)
	fmt.Println(bits.Reverse32(num))
	// fmt.Printf("%x", 1 << 32 - 1)
}

// 201
func TestBitwiseAndRange(t *testing.T) {
	ret := 5
	for i := 0; i <= 7; i++ {
		fmt.Printf("i=%b, i&(i-1)=%b\n", i, i&(i-1))
	}
	fmt.Println(ret)
}

func TestIsHappy(t *testing.T) {
	n := 2
	mem := make(map[int]bool)
	for n != 1 {
		mem[n] = true
		sum := sumOfSquares(n)
		fmt.Printf("sum(%d)=%d\n", n, sum)
		if _, ok := mem[sum]; ok {
			fmt.Println("false")
			return // loop
		}
		n = sum
	}
	fmt.Println("true")
}

func sumOfSquares(n int) int {
	sum := 0
	for n > 0 {
		digit := n % 10
		sum += digit * digit
		n /= 10
	}
	return sum
}

func TestXOR(tt *testing.T) {
	x := 0b10010
	fmt.Printf("x=0b%b, -x=0b%b, x & -x=0b%b\n", x, uint8(-x), x&-x)
}

// 371
func TestGetSum(t *testing.T) {
	fmt.Println(getSum(-12, -8))
	fmt.Println(getSum(88, 22))
}

func getSum(a int, b int) int {
	res := 0
	carry := 0
	for i := 0; i < 32; i++ {
		digit1 := (a >> i) & 1
		digit2 := (b >> i) & 1
		sum := digit1 ^ digit2 ^ carry
		res = res | (sum << i)
		carry = digit1&digit2 | (carry & (digit1 ^ digit2))
	}
	fmt.Printf("res(%T) %v=%b\n", res, int32(res), res)
	return int(int32(res))
}

// 421
func TestFindMaximumXOR(t *testing.T) {
    nums := []int{3,10,5,25,2,8}
    fmt.Println(findMaximumXOR(nums))
}

func findMaximumXOR(nums []int) int {
	// ans := math.MinInt32
	// for i := 0; i < len(nums); i++ {
	// 	for j := i+1; j < len(nums); j++ {
    //         fmt.Printf("%d(%b) ^ %d(%b) = %d(%b)\n", nums[i],nums[i], nums[j],nums[j], nums[i] ^ nums[j], nums[i] ^ nums[j])
	// 		ans = max(nums[i]^nums[j], ans)
	// 	}
	// }
	// return ans
	// 从高到低位尝试置1，并保存当前的数
	highest := 30
	x := 0
	for b := highest; b >= 0; b-- {
		seen := make(map[int]bool)
		for _, num := range nums {
			seen[num >> b] = true
		}
		x_next := (x << 1) + 1 // 尝试置1
		found := false
		for _, num := range nums {
			if _, ok := seen[x_next ^ (num >> b)]; ok {
				found = true
				break
			}
		}
		if !found {
			x_next -= 1
		}
		x = x_next
	}
	return x
}
