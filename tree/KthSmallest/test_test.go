package kthsmallest

import (
	"fmt"
	"math"
	"testing"
)

// 440
func TestKthSmallest(t *testing.T) {
	n, k := 110, 3
	fmt.Println(findKthNumber(n, k))
	fmt.Println(math.Sqrt(float64(n)))
	// fmt.Println(getTreeCount(1))
}

func findKthNumber(n int, k int) int {
	step := 1
	cur := 1
	for step < k {
		cnt := getNodeCount(n, cur)
		if cnt + step <= k {
			cur += 1 // 同一层的下一个节点
			step += cnt
		} else {
			cur *= 10 // 移向下一层
			step += 1
		}
	}
	return cur
}

func getNodeCount(n int, cur int) int {
	// 返回当前节点值为cur的树节点个数，节点值区间为[1, n]
	cnt := 1
	left, right := cur * 10, cur * 10 + 9 // 一层节点的最左端和最右端的值
	for left <= n {
		cnt += min(right, n) - left + 1
		left = left * 10
		right = right * 10 + 9
	}
	return cnt
}