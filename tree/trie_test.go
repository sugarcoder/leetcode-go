package tree

import (
	"testing"
	"fmt"
)

type TrieNode struct {
	next []*TrieNode
	has_word bool
}

func NewTrieNode() *TrieNode {
	return &TrieNode {
		next: make([]*TrieNode, 26),  // only contain lower case letter
		has_word: false,
	}
}

func (node *TrieNode) InsertWord(word string, i int) {
	if i == len(word) {
		return
	}
	leafIndex := word[i]-'a'
	if node.next[leafIndex] == nil {
		node.next[leafIndex] = NewTrieNode()
	}
	if i == len(word) - 1 {
		node.next[leafIndex].has_word = true
	} else {
		node.next[leafIndex].InsertWord(word, i+1)
	}
}

func TestTrieNode(t *testing.T) {
	node := NewTrieNode()
	fmt.Println(node.next[2] == nil)
	node.InsertWord("hello", 0)
	node.InsertWord("hi", 0)
	fmt.Println(node.next['h'-'a'].next['i'-'a'].has_word)
}

func TestWordBreak(t *testing.T) {
	s := "penapplepen"
	wordDict := []string{"pen", "apple", "pinapple"}

	root := NewTrieNode()
	for _, v := range wordDict {
		root.InsertWord(v, 0)
	}

	queue := make([]int, 0)  // 保存当前正在查找的单词开头index
	visited := make([]int, len(s))

	queue = append(queue, 0)
	for len(queue) > 0 {
		x := queue[0]
		queue = queue[1:]
		var p *TrieNode = root
		// search for a longest word started at index [x]
		for x < len(s) {
			if p.next[s[x]-'a'] == nil {
				break
			}
			p = p.next[s[x]-'a']
			x++
			
			if p.has_word {
				if x == len(s) {
					fmt.Println("true")
					return 
				}
				if visited[x] != 1 {
					// record the shorter match word ended index
					visited[x] = 1
					queue = append(queue, x)  // 下次从x开始搜索
				}
			}
		}
	}
	fmt.Println("false")
}

func TestWordBreak2(t *testing.T) {
	s := "penapplepen"
	wordDict := []string{"pen", "apple", "penapple"}

	root := NewTrieNode()
	for _, v := range wordDict {
		root.InsertWord(v, 0)
	}

	memo := make(map[string][]string)  // mapping   "catsandog" -> {"cats and dog", "cat sand dog"}
	
	results := dfs(s, root, memo)

	for _, v := range results {
		fmt.Println(v)
	}
}

// return s can be divided into what
func dfs(s string, trieRoot *TrieNode, memo map[string][]string) []string {
	if len(s) == 0 {
		return []string{""}
	}
	if v, ok := memo[s]; ok {
		return v
	}
	x := 0  // index of s
	result := []string{}
	var p *TrieNode = trieRoot
	for p != nil && x < len(s) {
		p = p.next[s[x]-'a']
		x++
		if p != nil && p.has_word {
			word := s[:x]
			right_strings := dfs(s[x:], trieRoot, memo)  // 这里没有判断s[x:]为空的情况
			
			for _, divided_string := range right_strings {
				if divided_string == "" {  // 在这里补充判断
					result = append(result, word)
				} else {
					result = append(result, word + " " + divided_string)
				}
			}
		}
	}
	memo[s] = result
	return result
}

func TestWordSearch2(tt *testing.T) {
	// 注意go循环的局部变量
	words := []string{"abc", "def", "gdg"}
	for _, word := range words {
		fmt.Printf("%p=%s\n", &word, word)
	}
	for i:= 0; i < len(words); i++{
		fmt.Printf("%p=%s\n", &words[i], words[i])
	}
}