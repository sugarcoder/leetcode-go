package tree

import (
	"testing"
	"fmt"
	"math/rand"
)

// 220 contain values
func TestRBTree(tt *testing.T) {
	r := rand.New(rand.NewSource(9))
	vals := []int{}
	tree := NewRBTree()
	for i := 0; i < 10; i++ {
		vals = append(vals, r.Int() % 50)
		tree.RBInsert(vals[i])
	}
	fmt.Println(vals)
	tree.LevelOrderTraversalPrint()
}

// not work
// func TestInteractive(t *testing.T) {
// 	interactive_RBtree()
// }