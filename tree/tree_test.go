package tree

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"testing"
)

type TreeNode struct {
	Val int
	Left *TreeNode
	Right *TreeNode
}

func TestInorderTraversal(t *testing.T) {
	preorderVals := []int{1, 2, 4, 5, 3, 6 ,7}
	inorderVals  := []int{4, 2, 5, 1 ,6, 3, 7}
	root := constructTree(preorderVals, inorderVals)
	InorderTraversal(root)
}

func TestLevelOrderConstruct(t *testing.T) {
	levelOrderVals := []int{1, 3, NULLVAL, NULLVAL, 2}
	root := constructTreeFromLevel(levelOrderVals)
	InorderTraversal(root)
}

func constructTree(preorderVals []int, inorderVals []int) *TreeNode{
	if len(preorderVals) == 0 {
		return nil
	}
	node := new(TreeNode)
	node.Val = preorderVals[0]
	var leftNum int
	for leftNum = 0; leftNum < len(preorderVals); leftNum++ {
		// finding the node val in inorder vals
		if inorderVals[leftNum] == preorderVals[0] {
			break
		}
	}
	node.Left = constructTree(preorderVals[1:leftNum+1], inorderVals[:leftNum])
	node.Right = constructTree(preorderVals[leftNum+1:], inorderVals[leftNum+1:])
	return node
}

const NULLVAL int = math.MaxInt32

func constructTreeFromLevel(levelorderVals []int) *TreeNode {
	if len(levelorderVals) == 0 {
		return nil
	}
	root := new(TreeNode)
	root.Val = levelorderVals[0]
	levelorderVals = levelorderVals[1:]
	queue := make([]*TreeNode, 0)
	queue = append(queue, root)
	for len(levelorderVals) > 0 && len(queue) > 0 {
		node := queue[0]
		queue = queue[1:]  // pop
		if levelorderVals[0] != NULLVAL {
			left := new(TreeNode)
			left.Val = levelorderVals[0]
			node.Left = left
			queue = append(queue, left)
		} else {
			node.Left = nil
		}
		levelorderVals = levelorderVals[1:]

		if len(levelorderVals) > 0 {
			if levelorderVals[0] != NULLVAL {
				right := new(TreeNode)
				right.Val = levelorderVals[0]
				node.Right = right
				queue = append(queue, right)
			} else {
				node.Right = nil
			}
			levelorderVals = levelorderVals[1:]
		}
	}
	return root
}

func InorderTraversal(node *TreeNode) {
	if node == nil {
		return
	}
	InorderTraversal(node.Left)
	fmt.Printf(" [%d]", node.Val)
	InorderTraversal(node.Right)
}

func PreorderTraversal(node *TreeNode) {
	if node == nil {
		return
	}
	fmt.Printf(" [%d]", node.Val)
	PreorderTraversal(node.Left)
	PreorderTraversal(node.Right)
}

// 199. Binary Tree Right Side View
func TestRightSideView(t *testing.T) {
	// 层序遍历
	// 选择每一层的最右边的节点
}

// 297 序列化和反序列化
type Codec struct {}

func Constructor() Codec {
    return Codec{}
}

// Serializes a tree to a single string.
func (codec *Codec) serialize(root *TreeNode) string {
    sb := &strings.Builder{}
	var dfs func(*TreeNode)
	dfs = func(node *TreeNode) {
		if node == nil {
			sb.WriteString("n ")
			return
		}
		sb.WriteString(strconv.Itoa(node.Val))
		sb.WriteByte(' ')
		dfs(node.Left)
		dfs(node.Right)
	}
	dfs(root)
	return sb.String()
}

// Deserializes your encoded data to tree.
func (codec *Codec) deserialize(data string) *TreeNode {
	elements := strings.Split(data, " ")
	var build func() *TreeNode  // declaration before recursive call
	build = func() *TreeNode {
		if elements[0] == "n" {
			elements = elements[1:]
			return nil
		}
		val, _ := strconv.Atoi(elements[0])
		elements = elements[1:]
		return &TreeNode{Val: val, Left: build(), Right: build()}
	}
	return build()
}

func (codec *Codec) serializeByQ(root *TreeNode) string {
    queue := []*TreeNode{}
    s := ""
    queue = append(queue, root)
    for len(queue) > 0 {
		node := queue[0]
		queue = queue[1:]
		if node == nil {
			s += "n"
		} else {
			queue = append(queue, node.Left)
			queue = append(queue, node.Right)
			s += fmt.Sprintf("%d", node.Val)
		}
		s += " "
    }
	return s
}


// Deserializes your encoded data to tree.
func (codec *Codec) deserializeByQ(data string) *TreeNode {    
	if len(data) == 0 || data == "n" {
		return nil
	}
	root := &TreeNode{}
	elements := strings.Split(data, " ")
	if v, err := strconv.Atoi(elements[0]); err == nil {
		root.Val = v
	} else {
		return nil
	}
	queue := []*TreeNode{}
	queue = append(queue, root)

	offset := 1
	for len(queue) > 0 {
		cur := queue[0]
		queue = queue[1:]
		if cur == nil {
			continue
		}
		if leftv, err := strconv.Atoi(elements[offset]); err == nil {
			lnode := &TreeNode{Val: leftv}
			cur.Left = lnode
			queue = append(queue, lnode)
		}
		if rightv, err := strconv.Atoi(elements[offset+1]); err == nil {
			rnode := &TreeNode{Val: rightv}
			cur.Right = rnode
			queue = append(queue, rnode)
		}
		offset += 2
	}
	return root
}

func TestDeserQueue(t *testing.T) {
	// v, err := strconv.Atoi("n")
	// fmt.Println(v, err)
	codec := Constructor()
	root := codec.deserializeByQ("1 2 3 n n n 5 n n")
	PreorderTraversal(root)
	fmt.Println("\n", codec.serializeByQ(root))
}

func TestDeserDFS(t *testing.T) {
	codec := Constructor()
	root := codec.deserialize("1 2 n n 3 n 5 n n")
	PreorderTraversal(root)
	fmt.Println("\n", codec.serialize(root))
}