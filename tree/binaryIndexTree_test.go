package tree

import (
	"fmt"
	"sort"
	"testing"
)

type BinaryIndexTree struct {
	tree     []int
	capacity int
}

func TestBITree(t *testing.T) {
	bit := new(BinaryIndexTree)
	bit.Init([]int{1, 2, 3, 4, 5, 6, 7, 8})
	fmt.Println(bit.Query(3))
	bit.Add(8, 2)
	fmt.Println(bit.Query(8))
}

// Init with length of data
func (bit *BinaryIndexTree) InitCap(capacity int) {
	// note the capacity = len(data)+1
	bit.tree, bit.capacity = make([]int, capacity+1), capacity+1
}

func (bit *BinaryIndexTree) Init(nums []int) {
	// 索引从1开始的树
	bit.tree, bit.capacity = make([]int, len(nums)+1), len(nums)+1
	for i := 1; i < bit.capacity; i++ {
		bit.tree[i] += nums[i-1]
		for j := i - 2; j >= i-lowbit(i); j-- {
			bit.tree[i] += nums[j]
		}
	}
}

// 求以二进制表示的x的最后一位1（所代表的数），如果1所在的位置是第k位，那么返回值是 2^k
func lowbit(x int) int {
	return x & (-x)
}

// update
func (bit *BinaryIndexTree) Add(index int, val int) {
	for index < bit.capacity {
		bit.tree[index] += val
		index += lowbit(index) // 向上更新
	}
}

// 求区间[1, index]的区间和，前n个数的和
func (bit *BinaryIndexTree) Query(index int) int {
	sum := 0
	for index >= 1 {
		sum += bit.tree[index]
		index -= lowbit(index)
	}
	return sum
}

// 315
func TestCountOfSmallerNumber(t *testing.T) {
	nums := []int{5, 2, 6, 1}
	// 离散化构造新的数组allNums
	allNums := make([]int, len(nums))
	copy(allNums, nums)
	sort.Ints(allNums)  // 从小到大排序
	kth := make(map[int]int)

	k := 1
	kth[allNums[0]] = k  // 记录值的映射
	for i := 1; i < len(allNums); i++ {
		if allNums[i] != allNums[i-1] {
			k++
			kth[allNums[i]] = k
		}
	}

	bitree := &BinaryIndexTree{}
	bitree.InitCap(k) // k种不同的值

	res := make([]int, len(nums))
	// 构造树状数组A，通过依次插入数组B的元素, 插入的为 （val，1）即树状数组记录值的个数，值是被离散化了的
	// 在反向插入的同时Query，比第k个元素要小的后续元素个数为 
	for i := len(nums)-1; i >= 0; i-- {
		res[i] = bitree.Query(kth[nums[i]]-1) // 比当前数小的数的个数之和
		bitree.Add(kth[nums[i]], 1)
		// fmt.Println(bitree.tree)
	}

	// return res
	fmt.Println(res)
}