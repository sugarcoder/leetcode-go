package tree

import (
	"fmt"
	"testing"
)

func TestRangeSumQuery(t *testing.T) {
	tree := ConstructSegTree([]int{1, 3, 5})
	fmt.Println(tree.SumRange(0, 2))
	tree.Update(1, 2)
	fmt.Println(tree.SumRange(0, 2))
}

type SegTree []int

func ConstructSegTree(nums []int) SegTree {
	n := len(nums)
	var tree SegTree = make(SegTree, 4*n)
	tree._build(0, n-1, 0, nums)
	return tree
}

// 构建树中索引为node的节点代表的区间[s, e]的值
func (seg SegTree) _build(s, e, node int, nums []int) {
	if s == e {
		seg[node] = nums[s]
		return
	}
	m := s + ((e - s) >> 1)
	seg._build(s, m, node*2+1, nums)
	seg._build(m+1, e, node*2+2, nums)
	seg[node] = seg[node*2+1] + seg[node*2+2] // 父节点（区间）的值是两个子节点的和
}

func (seg SegTree) Update(index int, val int) {
	seg._change(0, len(seg)/4-1, 0, index, val)
}

func (seg SegTree) _change(s, e, node int, index, val int) {
	if s == e { // 叶子节点
		seg[node] = val
		return
	}
	m := s + ((e - s) >> 1)
	if index <= m {
		seg._change(s, m, node*2+1, index, val)
	} else {
		seg._change(m+1, e, node*2+2, index, val)
	}
	seg[node] = seg[node*2+1] + seg[node*2+2]
}

func (seg SegTree) SumRange(left int, right int) int {
	return seg._range(left, right, 0, len(seg)/4-1, 0)
}

// node 为当前搜索的节点，代表的区间为[s, e]
func (seg SegTree) _range(left, right, s, e, node int) int {
	if left == s && right == e {
		return seg[node]
	}
	m := s + ((e - s) >> 1)
	if right <= m {
		return seg._range(left, right, s, m, node*2+1) // 左子树继续搜索
	}
	if left > m {
		return seg._range(left, right, m+1, e, node*2+2)
	}
	return seg._range(left, m, s, m, node*2+1) + seg._range(m+1, right, m+1, e, node*2+2)
}
