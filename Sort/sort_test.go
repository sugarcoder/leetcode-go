package Sort

import (
	"fmt"
	"sort"
	"strings"
	"testing"
	// "gitee.com/sugarcoder/leetcode-go/util"
)

func SortString(w string) string {
    s := strings.Split(w, "")
    sort.Strings(s)
    return strings.Join(s, "")
}

func runeSort() {
    word := "1BCagM9"
    s := []rune(word)
    sort.Slice(s, func(i int, j int) bool { return s[i] < s[j] })
    fmt.Println(string(s))
	nums := []int{1, 9, 3, 2, 7}
	sort.Ints(nums)
	fmt.Println(nums)
	nums = []int{1}
	fmt.Println(nums[1:])
}

func TestStringsort(t *testing.T) {
	name := SortString("dlsjfakldjfa")
	fmt.Println(name)
	fmt.Println(name == "aaddffjjklls")
	for i, c := range name {
		fmt.Printf("%d: %T, %T\n", i, c, name[i])
	}
}

func TestBubbleSort(t *testing.T) {
	nums := []int{3, 5, 2, 1, 0}
	bubblesort(nums)
	fmt.Println(nums)
}

func bubblesort(nums []int) {
	// 大数下沉
	for i := 0; i < len(nums)-1; i++ {
		for j := 0; j < len(nums)-i-1; j++ {
			if nums[j] > nums[j+1] {
				nums[j], nums[j+1] = nums[j+1], nums[j]
			}
		}
		fmt.Println(i, nums)
	}
}

func bubblesort_up(nums []int) {
	// 小数上浮
	for i := 0; i < len(nums)-1; i++ {
		for j := i+1; j < len(nums); j++ {
			if nums[i] > nums[j] {
				nums[i], nums[j] = nums[j], nums[i]
			}
		}
		fmt.Println(i, nums)
	}
}

func TestQuickSort(t *testing.T) {
	nums := []int{5, 3, 1, 2, 0}
	quicksort(nums, 0, len(nums)-1)
	fmt.Println(nums)
}


func quicksort(nums []int, lo, hi int) {
	if lo >= hi || lo < 0 {
		return
	}
	p := partition(nums, lo, hi)
	quicksort(nums, lo, p-1)
	quicksort(nums, p+1, hi)
}

// return pivot index
func partition(nums []int, lo, hi int) int {
	pivot := nums[hi]

	i := lo - 1 // i pointing to the bound where nums[:i] <= pivot
	for j := lo; j < hi; j++ {
		if nums[j] <= pivot { // >= 则最终输出降序排列
			i += 1
			nums[i], nums[j] = nums[j], nums[i]
		}
	}
	// Move the pivot element to the correct pivot position (between the smaller and larger elements)
	i += 1
	nums[i], nums[hi] = nums[hi], nums[i]
	return i
}

// leetcode 215
func TestFindKthElement(tt *testing.T) {
	nums := []int{3,2,1,5,6,4}
	k := 2
	lo, hi := 0, len(nums)-1
    for {
        i := partition(nums, lo, hi)
		fmt.Printf("lo=%d, hi=%d, pivot=%d\n", lo, hi, i)
        if i == k-1 {
            fmt.Println(nums[i])
			return
        } else if i < k-1 {
            lo = i + 1
        } else {
            hi = i - 1
        }
    }
}

func TestSearch(tt *testing.T) {
	citations := []int{0, 1, 3, 5 ,6}
	n := len(citations)
	res := n - sort.Search(n, func (x int) bool { return citations[x] >= n-x })
	fmt.Println(res)
}

// 406
func TestReconstructQueue(t *testing.T) {
	// util.ConvertBrackets2Braces("[[7,0],[4,4],[7,1],[5,0],[6,1],[5,2]]")
	people := [][]int{{7,0},{4,4},{7,1},{5,0},{6,1},{5,2}}
	fmt.Println(reconstructQueue(people))
}

func reconstructQueue(people [][]int) [][]int {
    sort.Slice(people, func(i, j int) bool {
        if people[i][0] == people[j][0] {
            return people[i][1] < people[j][1]
        }
        return people[i][0] > people[j][0]
    })
	// fmt.Println(people)
	// moveTo(people, 2, 1)
	for i := 1; i < len(people); i++ {
		if people[i][1] != i {
			moveTo(people, i, people[i][1])
		}
	}
	return people
}

func moveTo(people [][]int, old, new int) {
	person := people[old]
	for i := old; i > new; i-- {
		people[i] = people[i-1]
	}
	people[new] = person
}

// 414
func TestThirdMax(t *testing.T) {
	fmt.Println(thirdMax([]int{1, 2, 2, 3, 2}))
}

func thirdMax(nums []int) int {
    firstIdx, secondIdx, thirdIdx := -1, -1, -1
    for i, v := range nums {
        if firstIdx == -1 || v > nums[firstIdx] {
            thirdIdx = secondIdx
            secondIdx = firstIdx
            firstIdx = i
        } else if (secondIdx == -1 || v > nums[secondIdx]) && v != nums[firstIdx] {
            thirdIdx = secondIdx
            secondIdx = i
        } else if (thirdIdx == -1 || v > nums[thirdIdx]) && (secondIdx == -1 || v != nums[secondIdx]) && v != nums[firstIdx] {
            thirdIdx = i
        }
    }
    if secondIdx == -1 || thirdIdx == -1 {
        return nums[firstIdx]
    } else {
        return nums[thirdIdx]
    }
}