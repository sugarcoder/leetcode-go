package Sort

import (
	"sort"
	"testing"
	"fmt"
	"container/heap"
)

// sort.IntSlice是实现了sort接口的 []int
type hp struct{ sort.IntSlice }
// 实现额外的heap接口
func (h *hp) Push(v interface{}) { h.IntSlice = append(h.IntSlice, v.(int)) }
func (h *hp) Pop() interface{}   { a := h.IntSlice; v := a[len(a)-1]; h.IntSlice = a[:len(a)-1]; return v }


func TestIntSlice(tt *testing.T) {
	h := &hp{sort.IntSlice{1}}
	heap.Push(h, 4)
	heap.Push(h, 3)
	for h.IntSlice.Len() > 0 {
		fmt.Println(heap.Pop(h).(int))
	}
}

// 264
func hthUglyNumber(n int) {
	h := &hp{sort.IntSlice{1}} // 小根堆
	seen := make(map[int]bool)
	factor := []int{2, 3, 5}
	var val int
	for i := 1; ; i++ {
		val = heap.Pop(h).(int)
		if i == n {
			fmt.Println(val)
			return
		}
		for _, fac := range factor {
			nextVal := val * fac
			if _, ok := seen[nextVal]; !ok {
				seen[nextVal] = true
				heap.Push(h, nextVal)
			}
		}
	}
}