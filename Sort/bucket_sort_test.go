package Sort

import (
	"testing"
	"fmt"
	"crypto/rand"
	"math/big"
)

func TestBucketSort(t *testing.T) {
	arr := []int{1, 3, 6, 7, 4}
	fmt.Println(BucketSort(arr))
}

func TestRadixSort(t *testing.T) {
	// 以线性时间复杂度排序整数
	arr := generateRandomIntArray(100)
	fmt.Println(arr)
	fmt.Println(RadixSort(arr))
	// fmt.Println(len(arr))
}

func generateRandomIntArray(length int) []int {
	arr := make([]int, length)
	b := new(big.Int)
	b.SetInt64(int64(1000))
	for i := 0; i < length; i++ {
		num, err := rand.Int(rand.Reader, b)
		if err != nil {
			fmt.Println(err)
			return []int{}
		}
		arr[i] = int(num.Int64())
	}
	return arr
}

type Element struct {
	max, min int
}

// 给一个包含n个正整数的数组，找出排序后相邻元素的最大差值
// 最大差值不会小于 valMax-valMin / (n - 1)
// return MaxMinGap
func BucketSort(arr []int) int {
	valMax := Max(arr...)
	valMin := Min(arr...)
	scope := Max(1, (valMax - valMin) / (len(arr) - 1))  // 桶覆盖的值的范围大小
	fmt.Println("The scope", scope)
	bucketNum := (valMax - valMin) / scope + 1 // 桶的个数
	fmt.Println("Bucket number", bucketNum)
	buckets := make([]Element, bucketNum)

	// init
	for i := range buckets {
		buckets[i].min = -1
	}

	// 这是拷贝！不是引用！
	// for _, bucket := range buckets {
	// 	bucket.min = -1
	// 	fmt.Println(bucket)
	// }
	
	for _, v := range arr {
		bucketIndex := (v - valMin) / scope
		if buckets[bucketIndex].min == -1 {
			buckets[bucketIndex].max = v
			buckets[bucketIndex].min = v
		}
		buckets[bucketIndex].max = Max(v, buckets[bucketIndex].max)
		buckets[bucketIndex].min = Min(v, buckets[bucketIndex].min)
	}
	for _, bucket := range buckets {
		fmt.Println(bucket)
	}
	// 遍历每个桶，求相邻桶之间的最大值最小值之差
	diff := 0
	prevMax := -1
	for _, bucket := range buckets {
		// 跳过空的桶
		if bucket.min == -1{
			continue
		}
		if prevMax != -1 {
			diff = Max(diff, bucket.min - prevMax)
		}
		prevMax = bucket.max
	}
	return diff
}

// 基数排序，从最低位开始
func RadixSort(nums []int) []int {
	maxVal := Max(nums...)
	buckets := make([][]int, 10)
	for exp := 1; exp < maxVal; exp *= 10 {
		for _, v := range nums {
			digit := v / exp % 10
			buckets[digit] = append(buckets[digit], v)
		}
		seq := []int{}
		for i := range buckets {
			seq = append(seq, buckets[i]...)
			buckets[i] = []int{}
		}
		nums = seq
	}
	return nums
}

func Max(a ...int) int {
	maxNum := a[0]
	for _, v := range a[1:] {
		if maxNum < v {
			maxNum = v
		}
	}
	return maxNum
}

func Min(a ...int) int {
	minNum := a[0]
	for _, v := range a[1:] {
		if minNum > v {
			minNum = v
		}
	}
	return minNum
}

func TestLeecode220(tt *testing.T) {
	nums := []int{2, 3 ,4}
	indexDiff := 4
	valueDiff := 5
	fmt.Println(containsNearbyAlmostDuplicate(nums, indexDiff, valueDiff))
	for i := 0; i > -6; i-- {
		fmt.Println(i, i / 3)
	}
}

// 220
// i != j,
// abs(i - j) <= indexDiff.
// abs(nums[i] - nums[j]) <= valueDiff, and
func containsNearbyAlmostDuplicate(nums []int, indexDiff int, valueDiff int) bool {
	if indexDiff <= 0 || valueDiff < 0 || len(nums) < 2 {
		return false
	}
	// 每个桶覆盖值满足valueDiff的数，桶的总个数要少于indexDiff
	return false
}