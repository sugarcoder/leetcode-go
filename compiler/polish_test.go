package compiler

import (
	"strings"
	"testing"
	"fmt"
	"strconv"
)

type OpFunc func(a, b int) int

func eval(tokens []string) int {
	// register function
	ops := make(map[string]OpFunc)
	plus := func(a, b int) int {
		return a + b
	}
	minus := func(a, b int) int {
		return a - b
	}
	ops["+"] = plus
	ops["-"] = minus

	// evaluation
	stack := make([]int, 0)
	for i, tok := range tokens {
		if f, ok := ops[tok]; ok {
			fmt.Println("Got op:", tok)
			arg2 := stack[len(stack)-1]
			arg1 := stack[len(stack)-2]
			stack = stack[:len(stack)-2]
			result := f(arg1, arg2)
			fmt.Printf("%d %s %d = %d\n", arg1, tok, arg2, result)
			stack = append(stack, result)
		} else {
			if num, err := strconv.Atoi(tok); err == nil {
				stack = append(stack, num)
			} else {
				fmt.Printf("cannot parse number at %d", i)
				panic(err)
			}
		}
	}
	return stack[0]
}

// 逆波兰表达式是表达式树的后序遍历
func TestEvalReversePolish(tt *testing.T) {
	result := eval(strings.Split("7 2 3 + -", " "))
	fmt.Println(result)
}

func pop(stack *[]int) int {
	length := len(*stack)
	if length > 0 {
		val := (*stack)[length-1]
		*stack = (*stack)[:length-1]
		return val
	} else {
		panic("empty stack")
	}
}

func top(stack []int) int {
	return stack[len(stack)-1]
}

// https://blog.reverberate.org/2013/07/ll-and-lr-parsing-demystified.html
// input: "- 7 + 2 3"
// func convertToReversePolish(polishTokens []string) []string {

// }

// // input: "7 2 3 + -"
// func convertToPolish(reversePolishTokens []string) []string {

// }