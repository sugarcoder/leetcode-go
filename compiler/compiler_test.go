package compiler

import (
	"testing"
	"fmt"
)

// 220, 227
func TestBasicCalculator(tt *testing.T) {
	// 
	input := "3+5 / 2 "
	l := NewLexer(input)
	p := NewParser(l)
	fmt.Println(p.minus_expr())
}

func TestLexer(tt *testing.T) {
	l := NewLexer("1 + 2 + 3 - 4")
	l.next()
	for l.token != EOF {
		fmt.Printf("%s ", l.token)
		l.next()
	}
}

type TokenType string

const (
	EOF = "EOF"
	ILLEGAL = "ILLEGAL"

	NUM = "NUM"
	PLUS = "+"
	MINUS = "-"
	LPAREN = "("
	RPAREN = ")"
	MULT = "*"
	DIV = "/"
)

type Lexer struct {
	input string
	position int // point to next position of ch
	token TokenType
	tokenVal int
	ch byte
}

func NewLexer(input string) *Lexer {
	l := &Lexer{input: input}
	l.readChar()
	return l
}

func (l *Lexer) next() {
	// skip space
	l.skipWhitespace()

	switch l.ch {
	case '+':
		l.token = PLUS
	case '-':
		l.token = MINUS
	case '*':
		l.token = MULT
	case '/':
		l.token = DIV
	case '(':
		l.token = LPAREN
	case ')':
		l.token = RPAREN
	case 0:
		l.token = EOF
	default:
		if l.ch < '0' || l.ch > '9' {
			l.token = ILLEGAL
			panic("Not a number")
		}
		l.token = NUM
		l.tokenVal = 0
		for l.ch >= '0' && l.ch <= '9' {
			l.tokenVal = l.tokenVal * 10 + int(l.ch - '0')
			l.readChar()
		}
		return
	}
	l.readChar()
}

func (l *Lexer) readChar() {
	if l.position >= len(l.input) {
		l.ch = 0
	} else {
		l.ch = l.input[l.position]
	}
	l.position += 1
}

func (l *Lexer) skipWhitespace() {
	for l.ch == ' ' {
		l.readChar()
	}
}
/**
<minus_expr> ::= - <term> <expr_tail>
				| <expr>
<expr> ::= <term> <expr_tail>
<expr_tail> ::= + <term> <expr_tail>
                | - <term> <expr_tail>
                | <empty>

<term> ::= <factor><term_tail>
<term_tail> ::= * <factor> <term_tail>
			|   / <factor> <term_tail>
			|   <empty>

<factor> ::= ( <minus_expr> )  // 注意这里是minus_expr不是expr
            | Num
*/
type Parser struct {
	l *Lexer
}

func NewParser(l *Lexer) *Parser {
	p := &Parser{l: l}
	p.l.next()
	return p
}

func (p *Parser) match(tk TokenType) {
	if tk != p.l.token {
		panic(fmt.Sprintf("Unexpected token:%s at %d", p.l.token, p.l.position - 1))
	}
	p.l.next()
}


func (p *Parser) factor() int {
	var value int
	if p.l.token == LPAREN {
		p.match(LPAREN)
		value = p.minus_expr()
		p.match(RPAREN)
	} else {
		value = p.l.tokenVal
		p.match(NUM)
	}
	// fmt.Println("term:value", value)
	return value
}

func (p *Parser) term_tail(lvalue int) int {
	if p.l.token == MULT {
		p.match(MULT)
		value := lvalue * p.factor()
		return p.term_tail(value)
	} else if p.l.token == DIV {
		p.match(DIV)
		value := lvalue / p.factor()
		return p.term_tail(value)
	} else {
		return lvalue
	}
}

func (p *Parser) term() int {
	lvalue := p.factor()
	return p.term_tail(lvalue)
}

func (p *Parser) expr_tail(lvalue int) int {
	if p.l.token == PLUS {
		p.match(PLUS)
		value := lvalue + p.term()
		return p.expr_tail(value)
	} else if p.l.token == MINUS {
		p.match(MINUS)
		value := lvalue - p.term()
		return p.expr_tail(value)
	} else {
		return lvalue
	}
}

func (p *Parser) expr() int {
	lvalue := p.term()
	return p.expr_tail(lvalue)
}

func (p *Parser) minus_expr() int {
	if p.l.token == MINUS {
		p.match(MINUS)
		value := p.term()
		return p.expr_tail(-value)
	} else {
		return p.expr()
	}
}