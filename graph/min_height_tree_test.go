package graph

import (
	"fmt"
	"testing"
)

func TestMinHeightTree(t *testing.T) {
	edges := [][]int{{1, 0}, {1, 2}, {1, 3}}
	edges2 := [][]int{{3,0},{3,1},{3,2},{3,4},{5,4}}
	fmt.Println(minHeightTreeDFS(4, edges))
	// fmt.Println(minHeightTreeDFS(6, edges2))
	// fmt.Println(minHeightTreeBFS(4, edges))
	fmt.Println(minHeightTreeBFS(6, edges2))
	edges3 := [][]int{{0,1}, {0,2}}
	// fmt.Println(minHeightTreeTopologicalSort(4, edges))
	// fmt.Println(minHeightTreeTopologicalSort(6, edges2))
	fmt.Println(minHeightTreeTopologicalSort(3, edges3))
}


func minHeightTreeDFS(n int, edges [][]int) []int {
	if n == 1 {
		return []int{0}
	}
	// change edge to adjacent graph
	var adjs [][]int = make([][]int, n)
	for i := range adjs {
		adjs[i] = make([]int, 0)
	}
	for _, edge := range edges {
		adjs[edge[0]] = append(adjs[edge[0]], edge[1])
		adjs[edge[1]] = append(adjs[edge[1]], edge[0])
	}
	var parents []int = make([]int, n)
	maxDepth, deepestNode := 0, -1
	var dfs func(node, parent, cur_depth int)
	dfs = func(node, parent, cur_depth int) {
		if cur_depth > maxDepth {
			maxDepth, deepestNode = cur_depth, node
		}
		parents[node] = parent // record the parent
		for _, next := range adjs[node] {
			if next != parent { // 不回溯
				dfs(next, node, cur_depth+1)
			}
		}
	}
	// from node 0 to find deepest x
	dfs(0, -1, 1)
	maxDepth = 0  // reset the global var
	// from x to find the deepest path
	dfs(deepestNode, -1, 1)
	// get the longest path
	path := make([]int, 0)
	for deepestNode != -1 {
		path = append(path, deepestNode)
		deepestNode = parents[deepestNode]
	}
	// find the path midium
	pathlen := len(path)
	if pathlen % 2 == 0 {
		return []int{path[pathlen/2-1], path[pathlen/2]}
	} else {
		return []int{path[pathlen/2]}
	}
}

// bfs
func minHeightTreeBFS(n int, edges [][]int) []int {
	if n == 1 {
		return []int{0}
	}
	// change edge to adjacent graph
	var adjs [][]int = make([][]int, n)
	for i := range adjs {
		adjs[i] = make([]int, 0)
	}
	for _, edge := range edges {
		adjs[edge[0]] = append(adjs[edge[0]], edge[1])
		adjs[edge[1]] = append(adjs[edge[1]], edge[0])
	}
	parents := make([]int, n)
	bfs := func(start int) (x int) {
		vis := make([]bool, n)
		queue := make([]int, 0)
		vis[start] = true
		queue = append(queue, start)
		for len(queue) > 0 {
			x, queue = queue[0], queue[1:]
			for _, next := range adjs[x] {
				if !vis[next] {
					vis[next] = true
					queue = append(queue, next)
					parents[next] = x
				}
			}
		}
		return
	}
	// find the deepest x
	x := bfs(0)
	// make the x the root
	parents[x] = -1
	// find the deepest y from x
	y := bfs(x)
	// construct path
	path := make([]int, 0)
	for y != -1 {
		path = append(path, y)
		y = parents[y]
	}
	// find the path midium
	pathlen := len(path)
	if pathlen % 2 == 0 {
		return []int{path[pathlen/2-1], path[pathlen/2]}
	} else {
		return []int{path[pathlen/2]}
	}
}

// topological sort
func minHeightTreeTopologicalSort(n int, edges [][]int) []int {
	if n == 1 {
		return []int{0}
	}
	// find all node that degree is 1
	degree := make([]int, n)
	graph := make([][]int, n)  // adjacent graph
	for i := range graph {
		graph[i] = make([]int, 0)
	}
	for _, edge := range edges {
		degree[edge[0]]++
		degree[edge[1]]++
		graph[edge[0]] = append(graph[edge[0]], edge[1])
		graph[edge[1]] = append(graph[edge[1]], edge[0])
	}

	remainCount := n
	queue := make([]int, 0)
	for i, dg := range degree {
		if dg == 1 {
			queue = append(queue, i)
		}
	}
	for remainCount > 2 {
		remainCount -= len(queue)
		tmp := queue
		queue = nil
		
		for _, node := range tmp {
			for _, next := range graph[node] {
				degree[next]--
				if degree[next] == 1 {
					queue = append(queue, next)
				}
			}
		}
	}
	return queue
}