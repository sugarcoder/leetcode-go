package palindrome

import (
	"fmt"
	"sort"
	"testing"
)

func TestPalidromePairs(t *testing.T) {
	// fmt.Println(palindromePairs([]string{"abcd","dcba","lls","s","sssll"}))
	// fmt.Println(palindromePairs([]string{"a", ""}))
	// fmt.Println(palindromePairs([]string{"a", "abc", "aba", ""}))
    fmt.Println(palindromePairs([]string{"a","b","c","ab","ac","aa"})) // todo
}

func palindromePairs(words []string) [][]int {
	ans := [][]int{}
	// 记录按单词长度排序后的索引映射
	indexes := make([]int, len(words))
	for i := range words {
		indexes[i] = i
	}
    // 注意按照单词的长度来排序索引
	sort.Slice(indexes, func(i, j int) bool { return len(words[indexes[i]]) < len(words[indexes[j]]) })
	// 存储反向单词
	mapIdx := make(map[string]int)
	for _, idx := range indexes {
		word := words[idx]
		rword := revert(word)
		prefix := get_prefix(word)
		r_prefix := get_prefix(rword)
		// 求回文前缀的长度,word作为模式串，rword是查询串
		kth := kmp_search(prefix, rword, word)
		for kth != 0 {
			if v, ok := mapIdx[word[kth:]]; ok {
				ans = append(ans, []int{v, idx})
			}
			kth = prefix[kth-1]
		}
		// 查询后缀回文的长度，将rword作为pattern模式串，word作为查询串
		kth = kmp_search(r_prefix, word, rword)
		for kth != 0 {
			if v, ok := mapIdx[word[:len(word)-kth]]; ok {
				ans = append(ans, []int{idx, v})
			}
			kth = r_prefix[kth-1]
		}

		if v, ok := mapIdx[word]; ok {
			ans = append(ans, []int{v, idx})
			ans = append(ans, []int{idx, v})
		}

		mapIdx[rword] = idx // 存储反向单词，因为需要拼接到word的前后位置时，可以直接比较反向的串
	}
	return ans
}

// 返回模式串p匹配到的最大位置的字符
// 也是两个串匹配的最大公共前缀的长度
// prefix是模式串p的前缀数组
func kmp_search(prefix []int, s string, p string) int {
	j := 0
	for i := 0; i < len(s); i++ {
		print_match(s, p, i, j)
		for j > 0 && s[i] != p[j] {
			j = prefix[j-1]
			print_match(s, p, i, j)
		}
		if s[i] == p[j] {
			j++
		}
		// 在本题中，s和p长度相等，完全匹配时，i == j == len(s) == len(p)
		if j == len(p) {
			fmt.Printf("first match index at s[%d]\n", i-len(p)+1)
			return j
		}
	}
	return j
}

func TestKmpSearch(t *testing.T) {
	word := "ababc"
	pattern := "abc"
	prefix := get_prefix(pattern)
	kmp_search(prefix, word, pattern)
	// prefix := get_prefix(word)
	// best := kmp_search(prefix, word, revert(word))
	// fmt.Println(best)
}

// word的后缀回文的最大长度
func get_back_palidrome_length(word string) int {
	rword := revert(word)
	r_prefix := get_prefix(rword)
	fmt.Println(r_prefix)
	l := kmp_search(r_prefix, word, rword)
	return l
}

func TestGetBackPalidromeLength(t *testing.T) {
	word := "cdaba"
	fmt.Println(get_back_palidrome_length(word))
}

// word的前缀最长回文串的长度
func get_front_palidrome_length(word string) int {
	rword := revert(word)
	prefix := get_prefix(word)
	fmt.Println("prefix of word:", prefix)
	l := kmp_search(prefix, rword, word) // word作为pattern在rword上滑动
	return l
}

func TestGetFrontPalidromeLength(t *testing.T) {
	word := "abacd"
	fmt.Println(get_front_palidrome_length(word))
}

// 获取前缀数组，prefix[i]表示，s[:i+1]的最长公共真前后缀的长度
func get_prefix(s string) []int {
	if len(s) == 0 {
		return []int{}
	}
	prefix := make([]int, len(s))
	j := 0
	prefix[j] = 0
	for i := 1; i < len(s); i++ {
		for j > 0 && s[j] != s[i] {
			j = prefix[j-1]
		}
		if s[j] == s[i] {
			j += 1
		}
		prefix[i] = j
	}
	return prefix
}

func TestGetPrefix(t *testing.T) {
	fmt.Println(get_prefix("abcaba"))
}

func revert(word string) string {
	rev_word := []byte(word)
	for i, j := 0, len(word)-1; i < j; i, j = i+1, j-1 {
		rev_word[i], rev_word[j] = rev_word[j], rev_word[i]
	}
	return string(rev_word)
}

func isPalindrome(word string) bool {
	for i, j := 0, len(word)-1; i < j; i, j = i+1, j-1 {
		if word[i] != word[j] {
			return false
		}
	}
	return true
}

func print_match(s string, p string, i int, j int) {
	print_match_at(s, i, 0)
	print_match_at(p, j, i-j)
	fmt.Println("")
}

func print_match_at(s string, idx int, lag int) {
	for i := 0; i < lag; i++ {
		fmt.Print("  ")
	}
	// 在s的第i个字符的位置画一个箭头
	for i := 0; i < idx; i++ {
		fmt.Print("  ")
	}
	fmt.Print("V\n")
	for i := 0; i < lag; i++ {
		fmt.Print("  ")
	}
	for i := range s {
		fmt.Printf("%c ", s[i])
	}
	fmt.Printf("\n")
}

func TestPrint(t *testing.T) {
	print_match_at("hello", 3, 0)
}
