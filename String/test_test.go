package String

import (
	"fmt"
	"strings"
	"testing"
)

// 蔚来笔试
func TestFindWord(t *testing.T) {
	k := 5
	str := "abcabccca"
	strings.Contains(str, "abc")
	fmt.Println(k)
	fmt.Println("abc" > "ba")
}

func TestPrefix(t *testing.T) {
	s := "abcab"
	p := "ddd"
	prefix := get_prefix("abcab")
	fmt.Println(kmp(prefix, s, p))
}

func get_prefix(str string) []int {
	prefix := make([]int, len(str))
	prefix[0] = 0
	j := 0
	for i := 1; i < len(str); i++ {
		for j > 0 && str[i] != str[j] {
			j = prefix[j-1]
		}
		if str[i] == str[j] {
			j = j + 1
		}
		prefix[i] = j
	}
	return prefix
}

func kmp(prefix []int, s string, p string) int {
	match_count := 0
	j := 0
	for i := 0; i < len(s); i++ {
		for j > 0 && s[i] != p[j] {
			j = prefix[j-1]
		}
		if s[i] == p[j] {
			j += 1
		}
		if j == len(p) {
			match_count += 1
			j = 0 // 重新匹配
		}
	}
	return match_count
}

// 405
func TestHex(t *testing.T) {
	// b := 'a' + 10
	// fmt.Printf("%s%d\n", string(b), 3)
	// ans := make([]rune, 0)
	// ans = append(ans, 'b')
	// ans = append(ans, 'c')
	// fmt.Println(string(ans))
	fmt.Println(toHex(1000))
}

func toHex(num int) string {
	ht := make(map[int]byte)
	for i := 10; i < 16; i++ {
		ht[i] = byte('a' + i - 10)
	}
	// 4 bits
	ans := make([]byte, 0)
	mask := 15
	for i := 0; i < 8; i++ {
		tmp := num >> (i * 4)
		bits := tmp & mask
		if bits < 10 {
			ans = append(ans, byte('0'+bits))
		} else {
			ans = append(ans, ht[bits])
		}
	}
	for i := len(ans) - 1; i >= 1; i-- {
		if ans[i] == '0' {
			ans = ans[:i]
		} else {
			break
		}
	}
	for i, j := 0, len(ans)-1; i < j; i, j = i+1, j-1 {
		ans[i], ans[j] = ans[j], ans[i]
	}
	return string(ans)
}

// 438 anagram
func TestFindAnagrams(t *testing.T) {
	fmt.Println(findAnagrams("cbaebabacd", "abc"))
	fmt.Println(findAnagrams("abab", "ab"))
}

func findAnagrams(s string, p string) []int {
	if len(p) > len(s) {
        return []int{}
    }
	htS := make(map[byte]int)
	htP := make(map[byte]int)
	for i := range p {
		htP[p[i]] += 1
	}
	left, right := 0, 0
	ans := make([]int, 0)
	for right <= len(s) {
		if right - left < len(p) {
			htS[s[right]]++
			right++
		} else {
			if len(htS) == len(htP) {
				match := true
				for k, v := range htS {
					if c, ok := htP[k]; !ok || c != v {
						match = false
						break
					}
				}
				if match {
					ans = append(ans, left)
				}
			}
			htS[s[left]]--
			if htS[s[left]] == 0 {
				delete(htS, s[left])
			}
			left++
			if right > len(s) - 1 {
				break
			}
			htS[s[right]]++
			right++
		}
	}
	return ans
}