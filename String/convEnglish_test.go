package String

import (
	"fmt"
	"testing"
)


var level = []string{"Hundred", "Thousand", "Million", "Billion"}
var single = []string{"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"}
var ten = []string{"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"}
var numTen = []string{"Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"}

func TestConvEnglish(tt *testing.T) {
	// for i := 900; i < 999; i++ {
	// 	fmt.Println(i, convertLess1000(i))
	// }
    num := 12300000
    lev := 0
    var ans string
    for num > 0 {
        head := convertLess1000(num % 1000)
        if head != "Zero" {
            if lev > 0 {
                if ans != "" {
                    ans = head + " " + level[lev] + " " + ans
                } else {
                    ans = head + " " + level[lev]
                }
            } else {
                ans = head
            }
        }
        num /= 1000
        lev++
    }
    if ans == "" {
        ans = "Zero"
    }
    fmt.Println(ans)
}

func convertLess1000(num int) string {
    if num < 100 {
        return convertLess100(num)
    }
    tail := convertLess100(num % 100)
    head := single[num / 100]
    if tail == "Zero" {
        return head + " " + level[0]
    }
    return head + " " + level[0] + " " + tail
}

func convertLess100(num int) string {
    if num <= 9 {
        return single[num]
    }
    if num < 20 {
        return ten[num-10]
    }
    res := numTen[num / 10 - 2]
    digit := num % 10
    if digit == 0 {
        return res
    } else {
        return res + " " + single[digit]
    }
}