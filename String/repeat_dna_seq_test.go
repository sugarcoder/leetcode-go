package String

import (
	"testing"
	"fmt"
)

func TestHashFunc(t *testing.T) {
	h := hash("CAAAAACCCC")
	s := decode(h)
	fmt.Println(s)
}

func hash(s string) uint32 {
    var ret uint32 = 0
    for _, c := range s {
        ret <<= 2
        ret |= encode(c)
    }
    return ret
}

func encode(c rune) uint32 {
    if c == 'A' {
        return 0
    } else if c == 'T' {
        return 1
    } else if c == 'C' {
        return 2
    } else {
        return 3
    }
}

func decode(c uint32) string {
    var mask uint32 = 3
    ret := make([]rune, 10)
    for i := 9; i >= 0; i-- {
        tmp := c & mask
        if tmp == 0 {
            ret[i] = 'A'
        } else if tmp == 1 {
            ret[i] = 'T'
        } else if tmp == 2 {
            ret[i] = 'C'
        } else {
            ret[i] = 'G'
        }
        c >>= 2
    }
    return string(ret)
}