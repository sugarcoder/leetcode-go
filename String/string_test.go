package String

import (
	"fmt"
	"strconv"
	"strings"
	"testing"

	util "gitee.com/sugarcoder/leetcode-go/util"
)

func TestLengthOfLongestSubstring(t *testing.T) {
	fmt.Println(lengthOfLongestSubstring("abcabcbb"))
	fmt.Println(lengthOfLongestSubstring("bbbbb"))
	fmt.Println(lengthOfLongestSubstring("pwwkew"))
	fmt.Println(lengthOfLongestSubstring("aab"))
	// fmt.Println("234"[0:0])
}

// 3
func lengthOfLongestSubstring(s string) int {
	ht := make(map[byte]bool)
	left, right := 0, 0
	longestLen := -1
	for left < len(s) && right < len(s) {
		if _, ok := ht[s[right]]; ok {
			// 重复字符，删除区间头left   [left, right)
			delete(ht, s[left])
			left += 1
		} else {
			ht[s[right]] = true
			right += 1
			longestLen = util.Max(longestLen, right-left)
		}
	}
	return longestLen
}

// 6 zigzag
func convert(s string, numRows int) string {
	matrix := make([][]byte, numRows)
	for i := range matrix {
		matrix[i] = make([]byte, 0)
	}
	down := true
	row := 0
	for i := range s {
		if down {
			matrix[row] = append(matrix[row], s[i])
			row += 1
			if row == numRows {
				down = false
				row = numRows - 2
				if row <= 0 {
					down = true
					row = 0
				}
			}
		} else {
			matrix[row] = append(matrix[row], s[i])
			row -= 1
			if row == 0 {
				down = true
			}
		}
	}
	res := ""
	for i := range matrix {
		res += string(matrix[i])
	}
	return res
}

func TestZigZagConversion(t *testing.T) {
	fmt.Println(convert("PAYPALISHIRING", 3))
}

func TestExcelSheetColumn(t *testing.T) {
	fmt.Println("Test of Leetcode168")
	var ans string = "ZZC"
	ans = "A" + ans
	// fmt.Println(ans)
	// fmt.Println(701 / 26) // 26
	// fmt.Println(701 % 26) // 25
	fmt.Println(string('A' + 2))
	// convertToTitle(701)
	s := "abc"
	s += string('a')
	fmt.Println(s)
	// s := "ans"
	// fmt.Printf("%T\n", s[0]) // uint8
	for _, c := range s {
		fmt.Printf("%T:%c\n", c, c) // uint32
	}
}

func convertToTitle(columnNumber int) {
	var ans string
	for columnNumber > 0 {
		ans = fmt.Sprintf("%c", 'A'+(columnNumber-1)%26) + ans
		columnNumber -= (columnNumber - 1) % 26
		columnNumber /= 26
	}
	fmt.Println(ans)
	// return ans
}

// 205
func TestIsomorphic(tt *testing.T) {
	s := "badc"
	t := "baba"
	// s = "paper"
	// t = "title"
	if len(s) != len(t) {
		fmt.Println("false")
	}
	ht := make(map[uint8]uint8)
	used := make(map[uint8]bool)
	for i := 0; i < len(s); i++ {
		fmt.Println(s[i])
		if v, ok := ht[s[i]]; ok {
			if v == t[i] {
				continue
			} else {
				fmt.Println("false")
				return
			}
		} else {
			if _, ok := used[t[i]]; ok {
				fmt.Println("false")
				return
			} else {
				ht[s[i]] = t[i]
				used[t[i]] = true
			}
		}
	}
	fmt.Println("true")
}

func TestSubstring(t *testing.T) {
	var input string = "abcabc"
	fmt.Println(input[:3] == input[3:])
	j := 1
	j++
	fmt.Println(j)
}

func TestSpliting(t *testing.T) {
	// domain := "9001 www.abc.com"
	// ele := strings.Split(domain, " ")
	// fmt.Println(ele[1])
	// fmt.Println(strings.Cut(ele[1], "."))
	// num, _ := strconv.Atoi(ele[0])
	// fmt.Printf("%d , %T\n", num ,num)
	// strings.Contains(domain, ".")
	// sn := fmt.Sprintf("%d", num)
	// fmt.Println(sn )
	verions1 := "0.23.1.3"
	fmt.Println(strings.Split(verions1, "."))
	fmt.Println(strconv.Atoi("001"))
}

func TestStrSlice(t *testing.T) {
	records := []string{"9900 aa.ee", "100 bb.ee"}
	res := subdomainVisits(records)
	for _, v := range res {
		fmt.Println(v)
	}
}

func subdomainVisits(cpdomains []string) []string {
	counter := make(map[string]int)
	for _, record := range cpdomains {
		elestr := strings.Split(record, " ")
		num, _ := strconv.Atoi(elestr[0])
		domain := elestr[1]
		counter[domain] += num
		for index := strings.Index(domain, "."); index != -1; index = strings.Index(domain, ".") {
			fmt.Println(domain)
			counter[domain[index+1:]] += num
			domain = domain[index+1:]
		}
	}
	results := []string{}
	for k, v := range counter {
		ans := fmt.Sprintf("%d %s", v, k)
		results = append(results, ans)
	}
	return results
}

func TestReverse(tt *testing.T) {
	s := "abcc"
	rs := []byte(s) // use () to initialize a new byte slice
	for i, j := 0, len(rs)-1; i < j; i, j = i+1, j-1 {
		rs[i], rs[j] = rs[j], rs[i]
	}
	fmt.Println(s)
	fmt.Printf("%v\n", string(rs[:3]) == "ccb")
}

// todo
func TestFrogCroak(t *testing.T) {
	// fmt.Println(minNumberOfFrogs("croakcroak"))
	fmt.Println(minNumberOfFrogs("crokk")) // 顺序
}

func minNumberOfFrogs(croakOfFrogs string) int {
	var c, r, o, a, k int = 0, 0, 0, 0, 0
	count := 0
	for i := range croakOfFrogs {
		switch croakOfFrogs[i] {
		case 'c':
			c++
			r++
			o++
			a++
			k++
			if c > count {
				count += 1
			}
		case 'r':
			r--
		case 'o':
			o--
		case 'a':
			a--
		case 'k':
			k--
			c--
		}
		if r > o || o > a || a > k || k > c {
			return -1
		}
		if c < 0 || r < 0 || o < 0 || a < 0 || k < 0 {
			return -1
		}
	}
	if c == 0 && r == 0 && o == 0 && a == 0 && k == 0 {
		return count
	} else {
		return -1
	}
}

func TestBuddyString(t *testing.T) {
	fmt.Println(buddyStrings("ab", "ab")) // false
	fmt.Println(buddyStrings("aa", "aa")) // true
}

// 859
func buddyStrings(s string, goal string) bool {
	if len(s) != len(goal) {
		return false
	}
	if s == goal {
		ht := make(map[byte]bool)
		for i := range s {
			if _, ok := ht[s[i]]; ok {
				// 存在相同元素
				return true
			} else {
				ht[s[i]] = true
			}
		}
		return false
	}
	idx1, idx2 := -1, -1
	for i := range s {
		if s[i] != goal[i] {
			if idx1 == -1 {
				idx1 = i
			} else if idx2 == -1 {
				idx2 = i
			} else {
				return false
			}
		}
	}
	if idx1 == -1 || idx2 == -1 {
		return false
	}
	return s[idx1] == goal[idx2] && s[idx2] == goal[idx1]
}

// 345
func TestVowels(t *testing.T) {
	// c := 'a'
	// fmt.Println(c - 32 == 'A')
	fmt.Println(reverseVowels("Hello"))
}

func reverseVowels(s string) string {
	bs := []byte(s)
	vowels := []byte{'a', 'e', 'i', 'o', 'u'}
	ht := make(map[byte]bool)
	for _, ch := range vowels {
		ht[ch] = true
		ht[ch-32] = true
	}
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		for {
			if _, ok := ht[bs[i]]; !ok && i < j {
				i++
			} else {
				break
			}
		}
		for {
			if _, ok := ht[bs[j]]; !ok && i < j {
				j--
			} else {
				break
			}
		}
		bs[i], bs[j] = bs[j], bs[i]
	}
	return string(bs)
}

// 395. Longest Substring with At Least K Repeating Characters
func TestLongestSubstring(t *testing.T) {
	s := "bbaaacbd"
	k := 3
	fmt.Println(longestSubstring(s, k))
}

func longestSubstring(s string, k int) int {
	return divide(s, 0, len(s), k)
}

func divide(s string, left, right, k int) int {
	fmt.Printf("checking %s\n", s[left:right])
	ht := make(map[byte][]int)
	for i := left; i < right; i++ {
		c := s[i]
		if _, ok := ht[s[i]]; ok {
			ht[c] = append(ht[c], i) // save the index
		} else {
			ht[c] = make([]int, 0)
			ht[c] = append(ht[c], i)
		}
	}
	splits := make([]bool, len(s))
	flag := false
	for _, idxes := range ht {
		if len(idxes) < k {
			for _, idx := range idxes {
				splits[idx] = true
			}
			flag = true
		}
	}
	if flag {
		ans := 0
		for i := left; i < right; i++ {
			for i < right && splits[i] {
				i++
			}
			l := i
			for i < right && !splits[i] {
				i++
			}
			r := i

			ans = max(ans, divide(s, l, r, k))
		}
		return ans
	} else {
		return right - left
	}
}

// 423. Reconstruct Original Digits from English
func TestReconstructDigits(t *testing.T) {
	fmt.Println(originalDigits("owoztneoer"))
}

func originalDigits(s string) string {
	// alphabet := make(map[int]string)
	// nums := []string{"zero","one","two","three","four","five","six","seven","eight","nine"}
	// for i, n := range nums {
	//     alphabet[i] = n
	// }
	ht := make(map[rune]int)
	for _, c := range s {
		ht[c] += 1
	}
	cnt := make([]int, 10)
	cnt[0] = ht['z']
	cnt[2] = ht['w']
	cnt[4] = ht['u']
	cnt[6] = ht['x']
	cnt[8] = ht['g']
	cnt[3] = ht['h'] - cnt[8]
	cnt[7] = ht['s'] - cnt[6]
	cnt[5] = ht['v'] - cnt[7]
	cnt[9] = ht['i'] - cnt[5] - cnt[6] - cnt[8]
	cnt[3] = ht['t'] - cnt[2] - cnt[8]
	cnt[1] = ht['o'] - cnt[0] - cnt[2] - cnt[4]
	ans := ""
	for i, c := range cnt {
		ans += strings.Repeat(fmt.Sprintf("%d", i), c)
	}
	return ans
	// for ch, x := range ht {
	// 	fmt.Println(ch, x)
	// }
}

// 424. Longest Repeating Character Replacement
func TestLongestRepeatingAfterReplace(t *testing.T) {
	fmt.Println(characterReplacement("ABCDACDD", 2))
}

func characterReplacement(s string, k int) int {
	ht := make(map[byte]int)
	left, right := 0, 0 // 闭区间
	maxn := 0
	for right < len(s) {
		ht[s[right]]++
		maxn = max(maxn, ht[s[right]])
		if right-left+1-maxn > k { // 非重复字符超过可替换数量了
			ht[s[left]]--
			left++
		}
		right++
	}
	return right - left
}
