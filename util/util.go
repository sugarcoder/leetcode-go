package util

import (
	"strings"
	"fmt"
)

func Max(a ...int) int {
	max := a[0]
	for i := 1; i < len(a); i++ {
		if a[i] > max {
			max = a[i]
		}
	}
	return max
}

func Min(a ...int) int {
	min := a[0]
	for i := 1; i < len(a); i++ {
		if a[i] < min {
			min = a[i]
		}
	}
	return min
}

func ConvertBrackets2Braces(input string) {
	// input := "[[1,15],[7,18],[7,6],[7,100],[2,200],[17,30],[17,45],[3,5],[7,8],[3,6],[3,10],[7,20],[17,3],[17,45]]"
	input = strings.ReplaceAll(input, "[", "{")
	input = strings.ReplaceAll(input, "]", "}")
	fmt.Println(input)
}