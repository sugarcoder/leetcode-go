package util

import (
	"testing"
	"fmt"
)

func TestTreap(t *testing.T) {
	treap := Treap{}
	vals := []int{3, 4, 6, 1, 9}
	for _, v := range vals {
		treap.Put(v)
	}
	n := treap.Lowerbound(8)
	fmt.Println(n.val)
}