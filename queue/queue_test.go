package queue

import (
	"testing"
	"fmt"
)

type Queue struct{
	elements []int
}

func (q Queue) full() bool {
	return len(q.elements) == cap(q.elements)
}

func (q Queue) empty() bool {
	return len(q.elements) == 0
}

func (q *Queue) pop() (int, bool) {
	if q.empty() {
		return 0, false
	}
	val := q.elements[0]
	q.elements = q.elements[1:]
	return val, true
}

func (q Queue) front() (int, bool) {
	if q.empty() {
		return 0, false
	}
	return q.elements[0], true
}

func (q *Queue) push(val int) bool {
	if q.full() {
		return false
	}
	q.elements = append(q.elements, val)
	return true
}

func (q Queue) size() int {
	return len(q.elements)
}

func TestQueue(t *testing.T) {
	queue := Queue{elements: make([]int, 0, 5)}
	queue.push(3)
	queue.push(5)
	fmt.Println(queue.size())
	fmt.Println(queue.front())
	queue.pop()
	fmt.Println(queue.front())
}
