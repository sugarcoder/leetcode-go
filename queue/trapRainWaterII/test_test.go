package traprainwaterii

import (
	"container/heap"
	"fmt"
	"testing"

	// "gitee.com/sugarcoder/leetcode-go/util"
)
// 407
type cell struct {
	x, y int
	height int
}

type hp []cell
func (h hp) Len() int { return len(h) }
func (h hp) Swap(i, j int) { h[i], h[j] = h[j], h[i]}
func (h hp) Less(i, j int) bool { // 小根堆
	return h[i].height < h[j].height
}
func (h *hp) Push(v interface{}) {
	*h = append(*h, v.(cell))
}
func (h *hp) Pop() interface{} {
	a := *h
	v := a[len(a)-1]
	*h = a[:len(a)-1]
	return v
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func trapRainWater(heightMap [][]int) (ans int) {
	m, n := len(heightMap), len(heightMap[0])
	if m <= 2 || n <= 2 {
		return 0
	}

	vis := make([][]bool, m)
	for i := range vis {
		vis[i] = make([]bool, n)
	}
	h := &hp{}
	for i, row := range heightMap {
		for j, v := range row {
			if i == 0 || i == m-1 || j == 0 || j == n-1 { // 最外围边界
				heap.Push(h, cell{i, j, v})
				vis[i][j] = true
			}
		}
	}
	dirs := []int{-1, 0, 1, 0, -1}
	for h.Len() > 0 {
		cur := heap.Pop(h).(cell)
		for k := 0; k < 4; k++ {  // 堆中最小元素的相邻位置能否接到雨水
			nx, ny := cur.x + dirs[k], cur.y + dirs[k+1]
			if 0 <= nx && nx < m && 0 <= ny && ny < n && !vis[nx][ny] {
				if heightMap[nx][ny] < cur.height {
					ans += cur.height - heightMap[nx][ny]
				}
				vis[nx][ny] = true
				// 修改相邻位置的高度为水或墙的高度
				heap.Push(h, cell{nx, ny, max(heightMap[nx][ny], cur.height)})
			}
		}
	}
	return
}

func TestTrapRainWaterII(t *testing.T) {
	// util.ConvertBrackets2Braces("[[1,4,3,1,3,2],[3,2,1,3,2,4],[2,3,3,2,3,1]]")
	heightMap := [][]int{{1,4,3,1,3,2},{3,2,1,3,2,4},{2,3,3,2,3,1}}
	fmt.Println(trapRainWater(heightMap))
}

// 我们定义从点 (x,y) 到边界的路径中出现的最大高度为「路径高度」
// 问题的本质是求「从点 (x,y)到边界的所有路径高度的最小值为多少」
// 这个路径高度的最小值与 (x,y) 本身的高度 heightMap[x][y] 之间的差值，即是该点能接到的雨水数量。
