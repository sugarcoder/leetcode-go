package queue

import (
	"sort"
	"testing"
	"container/heap"
	"fmt"
)

type minHeap struct {
	sort.IntSlice
}

func (h *minHeap) Push(x any) {
	h.IntSlice = append(h.IntSlice, x.(int))
}

func (h *minHeap) Pop() any {
	// a := h.IntSlice
	// v := a[len(a)-1]
	// h.IntSlice = a[:len(a)-1]
	// return v
	minVal := h.IntSlice[h.Len()-1]
	h.IntSlice = h.IntSlice[:h.Len()-1]
	return minVal
}

type maxHeap struct {
	vals []int
}

func (h *maxHeap) Len() int { return len(h.vals) }
func (h *maxHeap) Swap(i, j int) { h.vals[i], h.vals[j] = h.vals[j], h.vals[i] }
func (h *maxHeap) Less(i, j int) bool { return h.vals[i] > h.vals[j] }
func (h *maxHeap) Push(x any) { h.vals = append(h.vals, x.(int)) }
func (h *maxHeap) Pop() any {a := h.vals; v := a[len(a)-1]; h.vals = a[:len(a)-1]; return v}

func TestMinHeap(t *testing.T) {
	h := &minHeap{sort.IntSlice{4, 9, 5, 2}}
	heap.Init(h)
	fmt.Println(heap.Pop(h))
}

type MedianFinder struct {
    left *maxHeap
    right *minHeap
}

func Constructor() MedianFinder {
    return MedianFinder{
        left: &maxHeap{[]int{}},
        right: &minHeap{sort.IntSlice{}},
    }
}


func TestMaxHeap(t *testing.T) {
	h := &maxHeap{[]int{3, 4, 5, 1}}
	heap.Init(h)
	fmt.Println(heap.Pop(h))
	fmt.Println((1 + 3)/ 3.0)
	fmt.Println("sdf" + fmt.Sprintf("%d", 3))
}