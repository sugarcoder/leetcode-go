package minsumpair

import (
	"container/heap"
)

type heap_adv_t struct {
	indexes [][]int
	nums1, nums2 []int
}

func (h heap_adv_t) Len() int { return len(h.indexes) }
func (h heap_adv_t) Less(i, j int) bool {
	sum1 := h.nums1[h.indexes[i][0]] + h.nums2[h.indexes[i][1]]
	sum2 := h.nums1[h.indexes[j][0]] + h.nums2[h.indexes[j][1]]
	return sum1 < sum2
}
func (h heap_adv_t) Swap(i, j int) {
	h.indexes[i], h.indexes[j] = h.indexes[j], h.indexes[i]
}
func (h *heap_adv_t) Pop() any {
	old_indexes := h.indexes
	n := h.Len()
	item := old_indexes[n-1]
	old_indexes = old_indexes[0:n-1]
	h.indexes = old_indexes
	return item
}
func (h *heap_adv_t) Push(item any) {
	h.indexes = append(h.indexes, item.([]int))
}

func kSmallestPairs2(nums1 []int, nums2 []int, k int) [][]int {
	h := heap_adv_t{indexes: make([][]int, 0), nums1: nums1, nums2: nums2}
	ans := make([][]int, 0)
	for i := 0; i < k && i < len(nums1); i++ {
		h.indexes = append(h.indexes, []int{i, 0})
	}
	heap.Init(&h)
	for h.Len() > 0 && len(ans) < k {
		pair := heap.Pop(&h).([]int)
		i, j := pair[0], pair[1]
		ans = append(ans, []int{nums1[i], nums2[j]})
        if j+1 < len(nums2) {
            heap.Push(&h, []int{i, j + 1})
        }
	}
	return ans
}
