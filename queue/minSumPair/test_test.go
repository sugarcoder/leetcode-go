package minsumpair

import (
	"testing"
	"fmt"
	"container/heap"
)

type item_t struct {
	index []int
	val int
}

type heap_t []*item_t

func (h heap_t) Len() int { return len(h) }
func (h heap_t) Less(i, j int) bool { return h[i].val > h[j].val } // 大根堆
func (h heap_t) Swap(i, j int) { h[i], h[j] = h[j], h[i] }
func (h *heap_t) Pop() any {
	old := *h
	item := old[len(old)-1]
	old[len(old)-1] = nil
	*h = old[:len(old)-1]
	return item
}
func (h *heap_t) Push(it any) {
	item := it.(*item_t)
	*h = append(*h, item)
}
func (h heap_t) Top() any {
	return h[0]
}

func TestMinSumPair(t *testing.T) {
	// nums1 := []int{1, 1, 2}
	// nums2 := []int{1,2,3}
	nums1 := []int{1, 2}
	nums2 := []int{3}
	k := 3
	fmt.Println(kSmallestPairs(nums1, nums2, k))
}

func kSmallestPairs(nums1 []int, nums2 []int, k int) [][]int {
	h := make(heap_t, 0)
	for i := 0; i < len(nums1); i++ {
		for j := 0; j < len(nums2); j++ {
			item := &item_t{index: []int{nums1[i],nums2[j]}, val: nums1[i]+nums2[j]}
			if h.Len() < k {
				heap.Push(&h, item)
			} else if item.val < h.Top().(*item_t).val {
				heap.Pop(&h)
				heap.Push(&h, item)
			} else {
				break
			}
		}
	}
	ans := make([][]int, 0)
	for i := 0; i < k && h.Len() > 0; i++ {
		index := heap.Pop(&h).(*item_t).index
		ans = append(ans, index)
	}
	return ans
}
