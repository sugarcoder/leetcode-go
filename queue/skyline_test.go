package queue

import (
	"container/heap"
	"fmt"
	"sort"
	"testing"
)

type item_t struct {
	right, height int
}

type prq_t []item_t

func (prq prq_t) Len() int           { return len(prq) }
func (prq prq_t) Less(i, j int) bool { return prq[i].height > prq[j].height }
func (prq prq_t) Swap(i, j int)      { prq[i], prq[j] = prq[j], prq[i] }
func (prq *prq_t) Push(x any) {
	item := x.(item_t)
	*prq = append(*prq, item)
}
func (prq *prq_t) Pop() any {
	old := *prq
	n := len(old)
	item := old[n-1]
	*prq = old[:n-1]
	return item
}
func (prq prq_t) Top() any {
	return prq[0]
}

func TestSkyline(t *testing.T) {
	// left, right, height, buildings 按 left 非递减排序
	buildings := [][]int{{2, 9, 10}, {3, 7, 15}, {5, 12, 12}, {15, 20, 10}, {19, 24, 8}}

	results := [][]int{}
	pq := prq_t{} // pq := make(prq_t, n)

	// 保存所有检查点x坐标，即所有buildings的boundary
	boundary := make([]int, 0, 2*len(buildings))
	for _, building := range buildings {
		boundary = append(boundary, building[0], building[1])
	}
	sort.Ints(boundary)
	fmt.Println(boundary)

	// 从左到右依次检查
	idx := 0 // building index
	for _, x := range boundary {
		// 对于某个检查点x，查看界限内的buildings (left <= x)，入队所有满足条件的（right, height)
		for idx < len(buildings) && buildings[idx][0] <= x {
			item := item_t{
				right:  buildings[idx][1],
				height: buildings[idx][2],
			}
			heap.Push(&pq, item)
			idx++
		}
		// 出队已经不在范围内的building，即 right <= x的
		for pq.Len() > 0 && pq.Top().(item_t).right <= x {
			heap.Pop(&pq)
		}

		maxh := 0
		if pq.Len() > 0 {
			maxh = pq.Top().(item_t).height
		}
		if len(results) == 0 || maxh != results[len(results)-1][1] {
			// 第一个点 或者 非两个紧连着的建筑
			results = append(results, []int{x, maxh})
		}
	}

	fmt.Println(results)
}
