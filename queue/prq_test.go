package queue

import (
	"testing"
	"container/heap"
	"fmt"
)

// An Item is something we manage in a priority queue.
type Item struct {
	value    int // The value of the item; arbitrary.
	index int // The index of the item in the origin array (not the heap)
}

// A PriorityQueue implements heap.Interface and holds Items.
type PriorityQueue []*Item

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	// We want Pop to give us the highest, not lowest, priority so we use greater than here.
	return pq[i].value > pq[j].value
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	// pq[i].index = i
	// pq[j].index = j
}

func (pq *PriorityQueue) Push(x any) {
	// n := len(*pq)
	item := x.(*Item)
	// item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() any {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil  // avoid memory leak
	// item.index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

func (pq PriorityQueue) Top() any {
	return pq[0]
}

// 这个方法需要追踪heap中item在heap二叉堆数组表示的下标
// update modifies the priority and value of an Item in the queue.
// func (pq *PriorityQueue) update(item *Item, value int) {
// 	item.value = value
// 	heap.Fix(pq, item.index)
// }

func TestPriorityQueue(t *testing.T) {
	// Some items and their priorities.
	items := []int{1, 3, -1, -3}

	// Create a priority queue, put the items in it, and
	// establish the priority queue (heap) invariants.
	pq := make(PriorityQueue, len(items))
	for i, v := range items {
		pq[i] = &Item{
			value:    v,
			index:    i,
		}
	}
	heap.Init(&pq)

	// Insert a new item and then modify its priority.
	item := &Item{
		value:    5,
		index: 4,
	}
	heap.Push(&pq, item)  // this will call the pq.Push(x)
	// update an items priority
	// pq.update(item, item.value, 5)

	// Take the items out; they arrive in decreasing priority order.
	for pq.Len() > 0 {
		// item := heap.Pop(&pq).(*Item)
		item := pq.Top().(*Item) // 堆顶元素 index = 0
		fmt.Printf("[%d]=%d ", item.index, item.value)
		item = heap.Pop(&pq).(*Item)
		fmt.Printf("pop: [%d]=%d \n", item.index, item.value)
	}
}

// leetcode 239: 优先队列方法
func TestSlidingWindow(t *testing.T) {
	nums := []int{1,3,-1,-3,5,3,6,7}
	k := 3

	pq := make(PriorityQueue, k)
	for i := 0; i < k; i++ {
		pq[i] = &Item{value: nums[i], index: i}
	}
	heap.Init(&pq)

	ans := make([]int, 1, len(nums)-k+1)
	ans[0] = pq.Top().(*Item).value  // 第一个区间的最大值
	for i := k; i < len(nums); i++ {
		heap.Push(&pq, &Item{value: nums[i], index: i})
		for pq.Top().(*Item).index < i - k {  // 剔除不属于当前区间的元素
			heap.Pop(&pq)
		}
		ans = append(ans, pq.Top().(*Item).value)
	}

	fmt.Println(ans)
}


func TestMonoQueue(t *testing.T) {
	nums := []int{1, -1}
	k := 1
	fmt.Println(maxSlidingWindow(nums, k))
}

// 单调队列解法
func maxSlidingWindow(nums []int, k int) []int {
    queue := make([]int, 0) // 存储index的单调队列，对应nums中的值从队头到队尾递减，
    var queue_back = func() int {
        if len(queue) > 0 {
            return queue[len(queue) - 1]
        } else {
            return -1
        }
    }
    var queue_front = func() int {
        if len(queue) > 0 {
            return queue[0]
        } else {
            return -1
        }
    }
    // initialize queue
    for i := 0; i < k; i++ {
        for len(queue) > 0 && nums[queue_back()] < nums[i] {
            queue = queue[:len(queue)-1] // pop back
        }
        queue = append(queue, i)
    }
    ans := make([]int, 0, len(nums) - k + 1)
    ans = append(ans, nums[queue_front()])

    for i := k; i < len(nums); i++ {
        for len(queue) > 0 && nums[queue_back()] < nums[i] {
            queue = queue[:len(queue) - 1]
        }
        queue = append(queue, i)
		// 注意记得保证单调队列最左边的元素的值在当前滑动窗口中
		for queue_front() <= i - k {
			queue = queue[1:]  // pop front
		}
        ans = append(ans, nums[queue_front()])
    }
    return ans
}