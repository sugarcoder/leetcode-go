package enumeration

import (
	"fmt"
	"testing"
)

func TestEnum(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5, 6}
	for j, val := range arr[2:] {
		fmt.Printf("arr[%d] = %d\n", j, val)
	}
}