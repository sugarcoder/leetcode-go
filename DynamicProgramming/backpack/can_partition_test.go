package backpack

import (
	"testing"
	"fmt"
)

// 416
func TestCanPartition(t *testing.T) {
	fmt.Println(canPartition([]int{1,2,3,5}))
}

func canPartition(nums []int) bool {
    if len(nums) < 2 {
        return false
    }
    sum := 0
    maxNum := 0
    for _, v := range nums {
        sum += v
        maxNum = max(maxNum, v)
    }
    if sum % 2 == 1 {
        return false
    }
    target := sum / 2
    if target < maxNum {
        return false
    }
    dp := make([][]bool, len(nums))
    for i := range dp {
        dp[i] = make([]bool, target+1) // dp[i][j]==true 表示可以从num[:i+1]中选取几个数，这些数的和为target
        dp[i][0] = true // 不选任何数，和为0
    }
    dp[0][nums[0]] = true
    for i := 1; i < len(nums); i++ {
        for j := 1; j <= target; j++ {
            if j >= nums[i] {
                dp[i][j] = dp[i-1][j-nums[i]] || dp[i-1][j] // 选或者不选nums[j]
            } else {
                dp[i][j] = dp[i-1][j]
            }
        }
    }
    return dp[len(nums)-1][target]
}