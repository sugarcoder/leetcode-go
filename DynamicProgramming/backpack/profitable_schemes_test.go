package backpack

import (
	"fmt"
	"testing"
)

func TestProfitable(t *testing.T) {
	n := 5
	minProfit := 3
	group := []int{2, 2}
	profit := []int{2, 3}
	fmt.Println(profitableSchemes(n, minProfit, group, profit))
}

func profitableSchemes(n int, minProfit int, group []int, profit []int) int {
	dp := make([][][]int, len(group)+1) // 考虑前i个组
	for i := range dp {
		dp[i] = make([][]int, n+1) // 允许最多使用人数
		for j := range dp[i] {
			dp[i][j] = make([]int, minProfit+1)
		}
	}
	// dp[i][j][k]: 考虑前i组，最多使用j人时，最小利润为k时的可选组合数
	// for j := 0; j <= n; j++ { // 不能这么写！
	// 	dp[0][j][0] = 1
	// }
	dp[0][0][0] = 1
	for i := 1; i <= len(group); i++ {
		g := group[i-1]
		p := profit[i-1]
		for j := 0; j <= n; j++ { // 注意从0开始
			for k := 0; k <= minProfit; k++ { // 注意从0开始
				if g > j {
					// 当前工作需要的员工数大于最多允许的员工数时
					dp[i][j][k] = dp[i-1][j][k]
				} else {
					// 加上不选当前工作的情况数
					dp[i][j][k] = dp[i-1][j][k]
					// 可以选择当前的工作
					// 可选员工数变为 j - g
					if p > k { // 可赚取的利润大于已经赚取的利润，由于定义的第三维是最小利润
						dp[i][j][k] += dp[i-1][j-g][0]
					} else {
						dp[i][j][k] += dp[i-1][j-g][k-p]
					}
					dp[i][j][k] = dp[i][j][k] % (1e9 + 7)
				}
				fmt.Println(dp[i])
			}
		}
	}
	ans := 0
	for j := 0; j <= n; j++ {
		ans += dp[len(group)][j][minProfit]
		ans = ans % (1e9 + 7)
	}
	return ans
}

func TestExpression(t *testing.T) {
	fmt.Println(1e2)
}