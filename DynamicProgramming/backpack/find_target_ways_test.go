package backpack

import (
	"fmt"
	"testing"
)

// 494
func TestFindTargetWays(t *testing.T) {
	nums := []int{1,1,1,1,1}
	target := 3
	fmt.Println(findTargetSumWaysDFS(nums, target))
	fmt.Println(findTargetSumWays(nums, target))
}

func findTargetSumWays(nums []int, target int) int {
	// neg: the sum of nums selected with negative sign
	// sum: the sum of nums
	// (sum - neg): the sum of nums selected with positive sign
	// Eq: (sum - neg) - neg == target  =>  neg == (sum - target) / 2
	// dp[i][j]: consider the first i-th nums, 使得选取的数的和为j的个数
	sum := 0
	for _, v := range nums {
		sum += v
	}
	diff := sum - target
	if diff < 0 || diff % 2 == 1 {
		return 0
	}
	neg := diff / 2
	dp := make([][]int, len(nums)+1)
	for i := range dp {
		dp[i] = make([]int, neg+1)
	}
	dp[0][0] = 1 // important!
	for i := 1; i <= len(nums); i++ {
		num := nums[i-1]
		for j := 0; j <= neg; j++ { // 注意j从0开始
			if j >= num {
				//        选取当前这个数       不选这个数
				dp[i][j] = dp[i-1][j-num] + dp[i-1][j]
			} else {
				dp[i][j] = dp[i-1][j]
			}
		}
	}
	return dp[len(nums)][neg]
}

// backtrack
func findTargetSumWaysDFS(nums []int, target int) int {
	return dfs(nums, 0, 0, target)
}

func dfs(nums []int, idx int, curSum int, target int) int {
	if idx == len(nums) {
		if curSum == target {
			return 1
		} else {
			return 0
		}
	}
	ans := 0
	ans += dfs(nums, idx+1, curSum + nums[idx], target)
	ans += dfs(nums, idx+1, curSum - nums[idx], target)
	return ans
}