package backpack

import (
	"fmt"
	"testing"
)

// 474
func TestZerosOnes(t *testing.T) {
	strs := []string{"10","0001","111001","1","0"}
	m := 5
	n := 3
	fmt.Println(findMaxFormSpace(strs, m, n))
}

func findMaxForm(strs []string, m int, n int) int {
    zeros, ones := make([]int, len(strs)), make([]int, len(strs))
    for i, s := range strs {
        for _, c := range s {
            if c == '1' {
                ones[i] += 1
            } else {
                zeros[i] += 1
            } 
        }
    }
    // fmt.Println(zeros, ones)
    dp := make([][][]int, len(strs)+1)
	for i := range dp {
		dp[i] = make([][]int, m+1)
		for j := range dp[i] {
			dp[i][j] = make([]int, n+1)
		}
	}
	for i := 1; i <= len(strs); i++ { // 考虑前i个数
		for j := 0; j <= m; j++ {
			for k := 0; k <= n; k++ {
				if j - zeros[i-1] >= 0 && k - ones[i-1] >= 0 {
					dp[i][j][k] = max(dp[i-1][j-zeros[i-1]][k-ones[i-1]] + 1, dp[i-1][j][k])
				} else {
					dp[i][j][k] = dp[i-1][j][k]
				}
			}
		}
	}
    return dp[len(strs)][m][n]
}

func findMaxFormSpace(strs []string, m int, n int) int {
    zeros, ones := make([]int, len(strs)), make([]int, len(strs))
    for i, s := range strs {
        for _, c := range s {
            if c == '1' {
                ones[i] += 1
            } else {
                zeros[i] += 1
            } 
        }
    }
    // fmt.Println(zeros, ones)
    dp := make([][]int, m+1)
	for i := range dp {
		dp[i] = make([]int, n+1)
	}
	for i := 1; i <= len(strs); i++ { // 考虑前i个数
		for j := m; j >= zeros[i-1]; j-- {
			for k := n; k >= ones[i-1]; k-- {
				dp[j][k] = max(dp[j-zeros[i-1]][k-ones[i-1]] + 1, dp[j][k])
			}
		}
	}
    return dp[m][n]
}
