package DynamicProgramming

import (
	"fmt"
	"testing"
)

// 312
func TestBurstBalloons(t *testing.T) {
	nums := []int{3, 1, 5, 8}
	fmt.Println(maxCoins(nums))
}

func maxCoins(nums []int) int {
	n := len(nums)
	vals := make([]int, 0, n+2)
	vals = append(vals, 1)
	vals = append(vals, nums...)
	vals = append(vals, 1)

	dp := make([][]int, n+2)
	for i := range dp {
		dp[i] = make([]int, n+2)
	}

	for i := n-1; i >=0 ; i-- {
		for j := i+2; j <= n+1; j++ {
			for k := i+1; k <= j-1; k++ {
				dp[i][j] = max(dp[i][j], dp[i][k] + dp[k][j] + vals[i] * vals[k] * vals[j])
			}
		}
	}

	return dp[0][n+1]
}