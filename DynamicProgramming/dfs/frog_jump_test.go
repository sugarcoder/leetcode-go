package dfs

import (
	"testing"
	"fmt"
)

// leetcode 403
func TestFrogJump(t *testing.T) {
	stones := []int{0,1,3,5,6,8,12,17}
	// stones = []int{0,1,3,6,7}
	// stones = []int{0, 2}
	stones = []int{0,1,3,6,10,15,16,21}
	// fmt.Println(canCross(stones))
	fmt.Println(canCrossWithMemo(stones))
}

func canCross(stones []int) bool {
	return dfs(stones, 0, 0)
}

func dfs(stones []int, at int, lastJump int) bool {
	if at == len(stones) - 1 {
		return true
	}
	for next := at+1; next < len(stones) && stones[next] - stones[at] <= lastJump+1; next++ {
		if stones[next]-stones[at] == lastJump-1 {
			if dfs(stones, next, lastJump-1) {
				return true
			}
		} else if stones[next]-stones[at] == lastJump {
			if dfs(stones, next, lastJump) {
				return true
			}
		} else if stones[next]-stones[at] == lastJump+1{
			if dfs(stones, next, lastJump+1) {
				return true
			}
		}
	}
	return false
}

func canCrossWithMemo(stones []int) bool {
	memo := make([]map[int]bool, len(stones))
	for i := range stones {
		memo[i] = make(map[int]bool)
	}
	var dfs func(at int, lastJump int) bool
	dfs = func(at, lastJump int) (res bool) {
		defer func () {
			memo[at][lastJump] = res
		}()
		if at == len(stones)-1 {
			return true
		}
		if res, ok := memo[at][lastJump]; ok {
			return res
		}
		for next := at+1; next < len(stones) && stones[next] - stones[at] <= lastJump+1; next++ {
			if stones[next]-stones[at] == lastJump-1 {
				if dfs(next, lastJump-1) {
					return true
				}
			} else if stones[next]-stones[at] == lastJump {
				if dfs(next, lastJump) {
					return true
				}
			} else if stones[next]-stones[at] == lastJump+1{
				if dfs(next, lastJump+1) {
					return true
				}
			}
		}
		return false
	}
	return dfs(0, 0)
}

func canCrossDP(stones []int) bool {
	dp := make([][]bool, len(stones))
	for i := range dp {
		dp[i] = make([]bool, len(stones))  // dp[i][j] 表示当前所处位置为i时，上一跳距离为j时能否到达
	}
	dp[0][0] = true
	for i := 1; i < len(stones); i++ {
		if stones[i] - stones[i-1] > i {
			return false
		}
	}
	for i := 1; i < len(stones); i++ {
		for j := i-1; j >= 0; j-- {
			k := stones[j] - stones[i]
			if k > j+1 {
				break
			}
			dp[i][k] = dp[j][k-1] || dp[j][k] || dp[j][k+1]
			if i == len(stones)-1 && dp[i][k] {
				return true
			}
		}
	}
	return false
}