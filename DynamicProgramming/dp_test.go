package DynamicProgramming

import (
	"fmt"
	"math"
	"math/bits"
	"sort"

	// "strings"
	"testing"
	// util "gitee.com/sugarcoder/leetcode-go/util"
)

// 5. Longest Palindromic Substring
func longestPalindrome(s string) string {
	// dp[i][j]   s[i:j]是否为回文字符串 i <= j
	// dp[k][k] == true,
	// dp[i][j] = true only s[i] == s[j] && dp[i+1][j-1] == true  (j - i >= 2)
	if len(s) <= 1 {
		return s
	}
	dp := make([][]bool, len(s))
	for row := range dp {
		dp[row] = make([]bool, len(s))
		dp[row][row] = true
	}

	ans := s[0:1]
	for i := len(s) - 1; i >= 0; i -= 1 {
		for j := i + 1; j < len(s); j++ {
			if s[i] == s[j] {
				dp[i][j] = (j-i < 2) || dp[i+1][j-1]
			} else {
				dp[i][j] = false
			}
			if dp[i][j] && j-i+1 > len(ans) {
				ans = s[i : j+1]
			}
		}
	}
	return ans
}

func TestLongestPalindrome(t *testing.T) {
	fmt.Println(longestPalindrome("abcbaabaa"))
	fmt.Println(longestPalindrome("a"))
	fmt.Println(longestPalindrome("ac"))
}

// 区间平移，后一个区间的元素等于前一个区间的对应元素值加一
func TestCountBits(t *testing.T) {
	n := 10
	ans := make([]int, n+1)
	exp := 1
	for i := 0; i <= n; i++ {
		if i >= (1 << (exp + 1)) {
			exp += 1
		}
		if i == 0 {
			ans[i] = 0
		} else if i == 1 {
			ans[i] = 1
		} else {
			ans[i] = ans[i-(1<<exp)] + 1
		}
	}
	fmt.Println(ans)
	fmt.Println(bits.OnesCount(12))
	k := 10
	fmt.Printf("10 = %b", k&(k-1))
}

// leecode10 regular expression
// 规定：'*'之前一定会有字母
func TestRegularExpr(t *testing.T) {
	s := "abca"
	p := ".*"
	dp := make([][]bool, len(p)+1)
	for i := range dp {
		dp[i] = make([]bool, len(s)+1)
	}
	// dp[i][k] : whether p[:i] s[:k] match
	dp[0][0] = true
	// 当s为空，p为 "x*y*z*"才能匹配
	for i := 1; i < len(p); i += 2 { // 注意！这里是i+=2
		if p[i] != '*' {
			break
		}
		dp[i+1][0] = true
	}
	for i := 0; i < len(p); i++ {
		for k := 0; k < len(s); k++ {
			if p[i] == '*' {
				//             不使用            匹配一次, 保证s[k]之前的能匹配 且 *之前的字母与s[k]相等
				dp[i+1][k+1] = dp[i-1][k+1] || (dp[i+1][k] && (p[i-1] == '.' || p[i-1] == s[k]))
			} else if p[i] == '.' {
				dp[i+1][k+1] = dp[i][k]
			} else {
				dp[i+1][k+1] = dp[i][k] && (p[i] == s[k])
			}
		}
	}
	fmt.Println(dp[len(p)][len(s)])
}

// 路径和尽可能大
// 从出发点到当前的所需的最小初始值尽可能小
// dp思想：在格子的 右边 和 下边 选出一个耗费生命较小的，首先要分别计算 右边 的格子和 下边 的格子的最优解。
// dp[i][k]表示到达这个格子的需要的最小生命值
// 选则一个需要较小的最小生命值的走法 choice = min(dp[i+1][k]), dp[i][k+1])
// 再根据当前格子的耗费或者补血来确定当前dp[i][k]
// if dungeon[i][k] > 0:
//
//	dp[i][k] = max(choice - dungeon[i][k], 1)
//
// if dungeon[i][k] < 0:
//
//	dp[i][k] = choice - dungeon[i][k]
//
// if dungeon[i][k] = 0
//
//	dp[i][k] = choice
func TestDungeon174(t *testing.T) {
	dungeon := [][]int{{-2, -3, 3}, {-5, -10, 1}, {10, 30, -5}}
	m, n := len(dungeon), len(dungeon[0])

	dp := make([][]int, m)
	for i := 0; i < m; i++ {
		dp[i] = make([]int, n)
	}
	dp[m-1][n-1] = max(1-dungeon[m-1][n-1], 1)
	for j := n - 2; j >= 0; j-- {
		// last row, from right to left
		dp[m-1][j] = max(dp[m-1][j+1]-dungeon[m-1][j], 1)
	}
	for i := m - 2; i >= 0; i-- {
		// last col
		dp[i][n-1] = max(dp[i+1][n-1]-dungeon[i][n-1], 1)
	}
	for i := m - 2; i >= 0; i-- {
		for j := n - 2; j >= 0; j-- {
			dp[i][j] = max(min(dp[i+1][j], dp[i][j+1])-dungeon[i][j], 1)
		}
	}
	fmt.Println(dp[0][0])
	// dp[i][j] =
	for i := 0; i < m; i++ {
		fmt.Println(dp[i])
	}
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

const INVALID int = math.MinInt32 / 2 // 因为后面还要用减法，防止溢出
func TestBuySellStockIV(t *testing.T) {
	k := 4 // txn number
	prices := []int{3, 2, 6, 5, 0, 3}
	n := len(prices)
	Shares := make([][]int, n)
	NoShares := make([][]int, n)
	for i := range Shares {
		Shares[i] = make([]int, k+1) // 第i天手上有股票，进行了j次交易后的最大利润
		NoShares[i] = make([]int, k+1)
	}
	// 第0天的情况，第一行
	for j := 1; j <= k; j++ {
		Shares[0][j] = INVALID // 无效状态
		NoShares[0][j] = INVALID
	}
	Shares[0][0] = -prices[0]
	NoShares[0][0] = 0
	for i := 1; i < n; i++ {
		// NoShares更新的时候需要用到这个值，完成0次交易的情况
		// 对于j == 0，不需要更新NoShares[i][0], 因为不存在Shares[i][-1]
		Shares[i][0] = max(Shares[i-1][0], NoShares[i-1][0]-prices[i])
		for j := 1; j <= k; j++ {
			Shares[i][j] = max(Shares[i-1][j], NoShares[i-1][j]-prices[i])
			NoShares[i][j] = max(NoShares[i-1][j], Shares[i-1][j-1]+prices[i]) // 卖掉后算一次交易
		}
	}
	maxProfit := 0
	for j := 0; j <= k; j++ {
		maxProfit = max(maxProfit, NoShares[n-1][j])
	}
	fmt.Println("Shares")
	for _, v := range Shares {
		fmt.Println(v)
	}
	fmt.Println("NoShares")
	for _, v := range NoShares {
		fmt.Println(v)
	}
	fmt.Println(maxProfit)
}

// 198 house robber
func TestHouseRobber(t *testing.T) {
	nums := []int{2, 7, 9, 3, 1}
	dp := make([][]int, len(nums))
	n := len(nums)
	for i := range dp {
		dp[i] = make([]int, 2)
	}
	dp[0][0] = 0
	dp[0][1] = nums[0]
	for i := 1; i < len(nums); i++ {
		dp[i][0] = max(dp[i-1][0], dp[i-1][1])
		dp[i][1] = max(dp[i-1][0]+nums[i], dp[i-1][1])
	}
	fmt.Println(max(dp[n-1][0], dp[n-1][1]))
}

// 264
func TestNthUglyNumber(tt *testing.T) {
	n := 10
	dp := make([]int, n)
	dp[0] = 1
	var p2, p3, p5 int = 0, 0, 0
	for i := 1; i < n; i++ {
		dp[i] = min(min(dp[p2]*2, dp[p3]*3), dp[p5]*5)
		// 注意不能用else if，否则有重复的
		if dp[i] == dp[p2]*2 {
			p2++
		}
		if dp[i] == dp[p3]*3 {
			p3++
		}
		if dp[i] == dp[p5]*5 {
			p5++
		}
		fmt.Printf("dp[p2] = %d, dp[p3] = %d, dp[p5] = %d\n", dp[p2], dp[p3], dp[p5])
	}
	fmt.Println(dp)
	// fmt.Println( dp[n-1])
}

func TestNumSquares(tt *testing.T) {
	n := 10
	fmt.Println(numSquares(n))
}

// 279
func numSquares(n int) int {
	dp := make([]int, n+1)
	// dp[0] = 0
	// dp[i] 表示 数字i需要最少dp[i]个 PerfectSquares 构成
	// dp[i] = 1 + min (dp[i-j^2]), where j >= 1 and j < i ^ 1/2
	for i := 1; i < n+1; i++ {
		minFac := math.MaxInt32
		for j := 1; j*j <= i; j++ {
			minFac = min(minFac, dp[i-j*j])
		}
		dp[i] = minFac + 1
	}
	return dp[n]
}

// 300. Longest Increasing Subsequence
func TestLIS(t *testing.T) {
	nums := []int{4, 10, 4, 3, 8, 9}
	// len(nums) >= 1
	dp := make([]int, len(nums))
	dp[0] = 1
	maxLen := 1
	for i := 1; i < len(nums); i++ {
		dp[i] = 1
		for j := 0; j < i; j++ {
			if nums[i] > nums[j] {
				dp[i] = max(dp[i], dp[j]+1)
			}
		}
		maxLen = max(maxLen, dp[i])
	}
	fmt.Println(maxLen)
}

func TestNthSuperUglyNumber(tt *testing.T) {
	fmt.Println(nthSuperUglyNumber(12, []int{2, 7, 13, 19}))
}

// 313
func nthSuperUglyNumber(n int, primes []int) int {
	pointers := make([]int, len(primes))
	dp := make([]int, n)
	dp[0] = 1
	for i := 1; i < n; i++ {
		nextVal := math.MaxInt32
		for j, p := range primes {
			nextVal = min(nextVal, dp[pointers[j]]*p)
		}
		dp[i] = nextVal
		for j, pt := range pointers {
			if dp[pt]*primes[j] == nextVal {
				pointers[j]++
			}
		}
	}

	return dp[n-1]
}

func TestJumping(t *testing.T) {
	n := 2 // n + 1 个房间
	rooms := []int{1, 2}
	// 访问过房间i偶数次，则下一次移动到i+1的房间 i >=1 i <= n+1
	// 访问过房间i奇数次，则下一次移动到rooms[i]的房间
	// 1 <= rooms[i] <= i // 从这点就可以看出，奇数次到达一个房间的时候，只可能向后退或原地踏步
	// fmt.Println(35 - 2 * 12)
	fmt.Println(stepCount(rooms, n))
}

func stepCount(rooms []int, n int) int {
	mod := 10e9 + 7
	// dp[i]表示访问到第i个房间时，需要的步数
	dp := make([]int, n+2) // dp[1] ... dp[n+1]
	dp[1] = 0
	for i := 2; i < n+2; i++ {
		dp[i] = (2*dp[i-1]%int(mod) - dp[rooms[i-1]-1] + 2) % int(mod)
	}
	return dp[n+1]
	// dp[i] = (2 * dp[i-1] % mod - dp[rooms[i-1]-1] + 2) % mod;
}

// ByteDance 笔试题
func TestFancySeqCount(t *testing.T) {
	arr := []int{1, 3, 5, 7, 2}
	m := 2
	fmt.Println(fancySequence(arr, m))
}

// m >= 1
func fancySequence(nums []int, m int) int {
	n := len(nums)
	count := 0

	for i := 0; i < n; i++ {
		max := nums[i]
		min := nums[i]
		for j := i; j < n; j++ {
			if nums[j] > max {
				max = nums[j]
			}
			if nums[j] < min {
				min = nums[j]
			}
			if max-min < 5 {
				count++
			}
		}
	}
	return count
}

// tasks
func TestTaskSelect(t *testing.T) {
	// 需要完成两个task task总数为n，时间限制为m 每个task有自己耗费的时间和价值 求下列task中哪两个价值之和最大
	// {time , value}
	tasks := [][]int{{2, 5}, {3, 8}, {4, 10}, {1, 3}, {5, 15}}
	n := len(tasks)
	m := 7 // time limit
	dp := make([][]int, n+1)
	for i := range dp {
		dp[i] = make([]int, m+1)
	}
	// dp[i][j] 表示 考虑了前i个任务后，最大耗时不超过j的两个任务的最大价值
	for i := range dp {
		dp[i][0] = 0
	}
	for j := 0; j <= m; j++ {
		dp[0][j] = 0
	}
	for i := 1; i <= len(tasks); i++ {
		time, value := tasks[i-1][0], tasks[i-1][1] // 考虑第i个任务
		for j := 1; j <= m; j++ {
			if j >= time {
				dp[i][j] = max(dp[i-1][j], dp[i-1][j-time]+value)
			} else {
				dp[i][j] = dp[i-1][j]
			}
		}
	}
	fmt.Printf("The max value of two tasks %v\n", dp[n][m])
}

func TestSwitchGem(t *testing.T) {
	numbers := []int{3, 5, 1, 6}
	prices := []int{8, 1, 9, 4}
	totalValue := 0
	for i := 0; i < len(numbers); i++ {
		totalValue += numbers[i] * prices[i]
	}
	fmt.Println(totalValue)

	maxValueIncrease := 0
	for i := 0; i < len(prices); i++ {
		for j := 0; j < len(prices); j++ {
			valueIncrease := (prices[j] - prices[i]) * (numbers[i] - numbers[j])
			if valueIncrease > maxValueIncrease {
				maxValueIncrease = valueIncrease
			}
		}
	}

	fmt.Println("new total Value", totalValue+maxValueIncrease)
}

// 343
func TestIntegerBread(t *testing.T) {
	fmt.Println(integerBreak(10))
}

func integerBreak(n int) int {
	// dp[i], 表示对于数i，当拆出去j时（j < i)，可以构成的乘积最大值
	// dp[i] = max(dp[i], dp[i-k] * k), 1 <= k < i
	dp := make([]int, n+1)
	dp[0] = 0
	dp[1] = 1
	dp[2] = 1
	for i := 3; i <= n; i++ {
		for k := 1; k < i; k++ {
			prod := (i - k) * k // i-k 不继续拆分
			prod = max(prod, dp[i-k]*k)
			dp[i] = max(prod, dp[i])
		}
		fmt.Printf("dp[%d]=%d, ", i, dp[i])
	}
	return dp[n]
}

func TestMaxEnvelopes(t *testing.T) {
	// input := "[[1,15],[7,18],[7,6],[7,100],[2,200],[17,30],[17,45],[3,5],[7,8],[3,6],[3,10],[7,20],[17,3],[17,45]]"
	// input = strings.ReplaceAll(input, "[", "{")
	// input = strings.ReplaceAll(input, "]", "}")
	// fmt.Println(input)
	envelopes := [][]int{{1, 15}, {7, 18}, {7, 6}, {7, 100}, {2, 200}, {17, 30}, {17, 45}, {3, 5}, {7, 8}, {3, 6}, {3, 10}, {7, 20}, {17, 3}, {17, 45}}
	fmt.Println(maxEnvelopes(envelopes))
	fmt.Println(maxEnvelopesBinary(envelopes))
}

// 354 二分法搜索
func maxEnvelopesBinary(envelopes [][]int) int {
	// sort.SearchInts()
	sort.Slice(envelopes, func(i, j int) bool {
		a, b := envelopes[i], envelopes[j]
		return a[0] < b[0] || a[0] == b[0] && a[1] > b[1]
	})
	// 设 f[j] 表示 h 的前 x 个元素可以组成的长度为 j 的最长严格递增子序列的末尾元素的最小值
	// f 只针对 某次循环
	f := []int{} // f[i] 表示末尾信封的高度，f按升序排序
	for _, e := range envelopes {
		height := e[1]
		// 找到小于height的第一个坐标
		if i := sort.SearchInts(f, height); i < len(f) {
			f[i] = height // 更新最小高度
		} else {
			f = append(f, height)
		}
	}
	return len(f)
}

// 354 信封套娃
func maxEnvelopes(envelopes [][]int) int {
	// sort by envelope's area
	// dp[i] represent the maximum number that the i-th envelop can hold
	// dp[i] = max(dp[k] + 1, 1), 0 <= k < i 且 第i个信封能装第k个信封
	// 注意，信封不允许翻转
	// sort.Slice(envelopes, func (i, j int) bool {
	// 	return envelopes[i][0] * envelopes[i][1] < envelopes[j][0] * envelopes[j][1]
	// })
	sort.Slice(envelopes, func(i, j int) bool {
		a, b := envelopes[i], envelopes[j]
		// 先按宽度排序，争取先放下最高的
		return a[0] < b[0] || a[0] == b[0] && a[1] > b[1]
	})
	for i, enve := range envelopes {
		fmt.Printf("[%d]:%v ", i, enve)
	}
	fmt.Println("")
	dp := make([]int, len(envelopes))
	max_num := 1
	for i := 0; i < len(envelopes); i++ {
		dp[i] = 1
		// maxk := -1
		for k := i - 1; k >= 0; k-- {
			if can_fit(envelopes[i], envelopes[k]) {
				// if dp[k]+1 > dp[i] {
				// 	maxk = k
				// }
				dp[i] = max(dp[i], dp[k]+1)
			}
		}
		max_num = max(dp[i], max_num)
		// fmt.Printf("[%d]:%d ", i, maxk)
	}
	// fmt.Println("")
	// for i, v := range dp {
	// 	fmt.Printf("[%d]:%d ", i, v)
	// }
	// fmt.Println("")
	return max_num
}

// 注意：不允许翻转信封
func can_fit(envelopBig, envelopSmall []int) bool {
	if envelopBig[0] > envelopSmall[0] && envelopBig[1] > envelopSmall[1] {
		return true
	} else {
		return false
	}
}

// 357
func TestNumWithUniqueDigit(t *testing.T) {
	fmt.Println(countNumbersWithUniqueDigits(3))
}

var factorial []int

func countNumbersWithUniqueDigits(n int) int {
	if n == 0 {
		return 1
	}
	factorial = make([]int, 10)
    factorial[0] = 1
    for i := 1; i <= 9; i++ {
        factorial[i] = factorial[i-1] * i
    }
	dp := make([]int, n+1)
	// dp[i]表示i位数包含的数字中，有着独一无二digit的数字个数
	// dp[n] = dp[n-1] + 10^n - 10^(n-1) - (n位数中有重复数字的数)
	dp[0] = 0
	if n == 1 {
		return 10
	}
	dp[1] = 10
	for k := 2; k <= n; k++ {
		// 				第一位数从1-9中选一个x	剩下的k-1位数从0-9中排除了x后的8个digit中选, 全排列
		dp[k] = dp[k-1] + combination(9, 1) * permutation(9, k-1)
	}
	return dp[n]
}

func combination(n, m int) int {
    // n >= m
    return factorial[n] / factorial[m] / factorial[n-m]
}

func permutation(n, m int) int {
	// n >= m
	return factorial[n] / factorial[n-m]
}

// 368
func TestLargestDivisibleSubset(t *testing.T) {
	// fmt.Println(largestDivisibleSubset([]int{3,4,16,8}))
	fmt.Println(largestDivisibleSubset([]int{2,3,8,9,27}))
}

func largestDivisibleSubset(nums []int) []int {
    sort.Ints(nums)
    dp := make([]int, len(nums)) // dp[i]表示考虑到第i个数字后，最长的subset长度
    index := make([]int, len(nums))
    dp[0] = 1
    index[0] = -1
    begin_index := 0
    max_length := 0
    for i := range dp {
		index[i] = -1
		dp[i] = 1
        for j := i-1; j >= 0; j-- {
            if nums[i] % nums[j] == 0 {
                if dp[j] + 1 > dp[i] {
                    dp[i] = dp[j]+1
                    index[i] = j
                }
            }
        }
        if dp[i] > max_length {
            max_length = dp[i]
            begin_index = i
        }
    }
    ans := make([]int, 0)
    for idx := begin_index; idx != -1; idx = index[idx]{
        ans = append(ans, nums[idx])
    }
    return ans
}


// 377
func TestCombinationIV(t *testing.T) {
	nums := []int{1, 2, 3}
	target := 4
	fmt.Println(combinationSum4(nums, target))
}

func combinationSum4(nums []int, target int) int {
	dp := make([]int, target+1) // dp[i] 表示 组合的元素之和为i的个数
	dp[0] = 1
	// dp[i] = sum(dp[i-num]), for num in nums
	for i := 1; i <= target; i++ {
		for _, num := range nums {
			if i - num >= 0 {
				dp[i] += dp[i-num]
			}
		}
	}
	return dp[target]
}

// 410 Split Array largest sum
func TestSplitArrayLargestSum(t *testing.T) {
	nums := []int{7,2,5,10,8}
	k := 2
	// fmt.Println(splitArray(nums, k))
	fmt.Println(splitArrayDivide(nums, k))
	fmt.Println(splitArrayDivide2(nums, k))
}

func splitArray(nums []int, k int) int {
	dp := make([][]int, len(nums)+1)
	presum := make([]int, len(nums)+1)
	for i := range dp {
		dp[i] = make([]int, k+1)  // 将前i个数分割成k份的区间和最大值
		for j := range dp[i] {
			dp[i][j] = math.MaxInt32
		}
	}
	for i := 0; i < len(nums); i++ {
		presum[i+1] = presum[i] + nums[i]
	}
	dp[0][0] = 0
	for i := 1; i <= len(nums); i++ { // 考虑前i个数
		for j := 1; j <= k && j <= i; j++ { // 分成j个区间
			for m := 0; m < i; m++ {
				// 将前m个数分成j-1个区间，后i-m个数分成1个区间
				dp[i][j] = min(dp[i][j], max(dp[m][j-1], presum[i] - presum[m]))
			}
		}
	}
	return dp[len(nums)][k]
}

// 二分法，开区间搜索[lower, upper)
func splitArrayDivide(nums []int, k int) int {
	lower, upper := 0, 0
	for _, v := range nums {
		lower = max(lower, v) // 每个数单独划分成一个区间
		upper += v // 整个数组为一个区间
	}
	for lower < upper {
		mid := lower + (upper - lower) / 2
		fmt.Printf("lower:%d, upper:%d -->mid[%d]\n", lower, upper, mid)
		m := split(nums, mid)  // 尝试以mid作为「子数组各自和的最大值」进行分割，获得分割得到的区间数m
		fmt.Printf("got split count:%d\n", m)
		if m > k {
			// 分割出的区间太多了
			// 「子数组各自和的最大值」应该大于mid
			fmt.Println("Update lower = mid + 1")
			lower = mid + 1
		} else {
			fmt.Println("Update upper = mid")
			upper = mid
		}
	}
	return lower
}

// 二分法，闭区间搜索
func splitArrayDivide2(nums []int, k int) int {
	lower, upper := 0, 0
	for _, v := range nums {
		lower = max(lower, v)
		upper += v
	}
	// 找lowerbound，满足划分区间为k时，「子数组各自和的最大值」最小
	// 找最小的mid值，使得分割数为k
	// 在闭区间[lower, upper]中寻找
	for lower <= upper {
		mid := lower + (upper - lower) / 2
		m := split(nums, mid)
		if m > k {
			lower = mid + 1 // 继续在区间[mid+1, upper]中寻找
		} else {
			upper = mid - 1 // 继续在区间[lower, mid-1]中寻找
		}
	}
	return lower
}

// 「子数组各自和的最大值」设置为m时，产生的分割区间数
func split(nums []int, m int) int {
	curSum := 0
	count := 1 // 分割的区间数
	for _, v := range nums {
		if curSum + v <= m {
			curSum += v
		} else { // 超过预设的最大值，另起炉灶
			count += 1
			curSum = v
		}
	}
	return count
}