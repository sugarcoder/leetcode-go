package completebackpack

import (
	"fmt"
	"math"
	"sort"
	"testing"
)

func TestCoinChange(t *testing.T) {
	coins := []int{1, 2, 5}
	amount := 11
	fmt.Println(coinChange(coins, amount))
	fmt.Println(coinChangeBFS(coins, amount))
	fmt.Println(coinChangeDFS(coins, amount))
}

// 322
func coinChange(coins []int, amount int) int {
	dp := make([][]int, len(coins) + 1)
	for i := range dp {
		dp[i] = make([]int, amount+1) // dp[i][v]: 考虑前i个硬币，面值和为v时，需要的每种硬币总数
	}
	for v := 1; v <= amount; v++ {
		dp[0][v] = -1
	}
	for i := 1; i <= len(coins); i++ {
		c := coins[i-1]
		for j := 1; j <= amount; j++ {
			valid := false
			for k := 0; k * c <= j; k++ { // 注意k从0开始，可以不使用第j枚硬币
				if dp[i-1][j - k * c] != -1 {
					valid = true
					if dp[i][j] == 0 {
						dp[i][j] = dp[i-1][j-k*c] + k
					} else {
						dp[i][j] = min(dp[i][j], dp[i-1][j-k*c] + k)
					}
				}
			}
			if valid == false {
				dp[i][j] = -1
			}
		}
	}
	return dp[len(coins)][amount]
}

// 一维动态规划
// 自底向上求解
func coinChangeII(coins []int, amount int) int {
	dp := make([]int, amount+1) // dp[i]表示，凑成面值和为i时的最小需要硬币数量
	for i := range dp {
		dp[i] = amount + 1 // 赋值一个最大的不可能的值
	}
	dp[0] = 0
	for i := 1; i <= amount; i++ {
		for _, c := range coins {
			if i - c >= 0 && dp[i-c] != amount+1 {
				dp[i] = min(dp[i], dp[i-c] + 1)
			}
		}
	}
	if dp[amount] == amount + 1 {
		dp[amount] = -1
	}
	return dp[amount]
}

// BFS搜索 (但内存超限)
// 找从amount到0的最短路径
func coinChangeBFS(coins []int, amount int) int {
	if amount == 0 {
        return 0
    }
	visited := make([]bool, amount+1)
	queue := make([]int, 0)
	queue = append(queue, amount)
	visited[amount] = true
	depth := 1
	sort.Ints(coins)
	for len(queue) > 0 {
		level := len(queue)
		for i := 0; i < level; i++ {
			rem := queue[i]
			for _, c := range coins {
				next := rem - c
				if next == 0 {
					return depth
				}
				if next < 0 {
					break // 剪纸，由于coins从小到大排序了，不需要再考虑后面的大额硬币了
				}
				if visited[next] == false {
					queue = append(queue, next)
				}
			}
		}
		depth++
		queue = queue[level:]
	}
	return -1
}

// DFS搜索 记忆化递归
func coinChangeDFS(coins []int, amount int) int {
	memo := make([]int, amount+1) // memo[i]记录总额为i时需要的最小硬币数量
	for i := range memo {
		memo[i] = -2 // 未计算出结果
	}
	sort.Ints(coins) // 从小到大排序
	var dfs func(value int) int
	dfs = func(value int) int {
		if value == 0 {
			return 0
		}
		if memo[value] != -2 {
			return memo[value]
		}
		res := math.MaxInt32
		for _, c := range coins {
			if value - c < 0 {
				break
			}
			left := dfs(value - c)
			if left != -1 {
				res = min(res, left + 1)
			}
		}
		if res != math.MaxInt32 {
			memo[value] = res
		} else {
			memo[value] = -1
		}
		return memo[value]
	}
	return dfs(amount)
}


// 518 完全背包的组合数
func TestChangeTotal(t *testing.T) {
	amount := 5
	coins := []int{1,2,5}
	fmt.Println(change(amount, coins))
	// coins = []int{3,5,7,8,9,10,11}
	// amount = 500
	// fmt.Println(changeDFS(amount, coins))
}

// 优化空间的解法
// func changeII(amount int, coins []int) int {
// 	dp := make([]int, amount+1) // dp[i]=count, 金额为i时，硬币的组合数
// 	dp[0] = 1
// 	for _, c := range coins {

// 	}
// }

func change(amount int, coins []int) int {
	// dp[i][v] == c, 考虑前i个数，构成价值总和为v的组合数
	dp := make([][]int, len(coins)+1)
	for i := range dp {
		dp[i] = make([]int, amount+1)
		dp[i][0] = 1
	}
	for i := 1; i <= len(coins); i++ {
		c := coins[i-1]
		for j := 1; j <= amount; j++ {
			for k := 0; k * c <= j; k++ {
				dp[i][j] += dp[i-1][j-k*c]
			}
		}
	}
	return dp[len(coins)][amount]
}

func changeDFS(amount int, coins []int) int {
	var dfs func(remain int, idx int) int
	dfs = func(remain, idx int) int {
		if idx >= len(coins) {
			return 0
		}
		if remain == 0 {
			return 1
		} else if remain < 0 {
			return 0
		}
		ans := dfs(remain - coins[idx], idx)
		ans += dfs(remain, idx+1)
		return ans
	}
	return dfs(amount, 0)
}