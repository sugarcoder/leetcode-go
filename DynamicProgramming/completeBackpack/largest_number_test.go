package completebackpack

import (
	"testing"
	"fmt"
)

// 1449
func TestLargestNumber(t *testing.T) {
	cost := []int{4,3,2,5,6,7,2,5,5}
	// cost = []int{7,6,5,5,5,6,8,7,8}
	target := 9
	// target = 12
	fmt.Println(largestNumber(cost, target))
}

func largestNumber(cost []int, target int) string {
    // find the deepest cost tree
	dp := make([]int, target+1) // dp[i]表示当费用和为i时，最长的数字长度
	for i := range dp {
		dp[i] = -1
	}
	dp[0] = 0
	chs := make([]int, target+1) // 记录费用和为i时，最长数字的选取字符
	chs[0] = -1
	for i := 1; i <= target; i++ {
		chs[i] = -1
		for num, c := range cost { // the cost[i] represent the cost of number i-1
			if i >= c && dp[i-c] != -1 {
				if dp[i-c]+1 > dp[i] {
					dp[i] = dp[i-c] + 1
					chs[i] = num+1
				} else if dp[i-c] + 1 == dp[i] {
					chs[i] = num+1
				}
			}
		}
	}
	fmt.Println(dp)
	for s, num := range chs {
		fmt.Printf("sum=%d, ", s)
		if num == -1 {
			fmt.Println("no number")
		} else {
			fmt.Printf("cost<%d>=%d\n", num, cost[num-1])
		}
	}
	if dp[target] == 0 {
		return ""
	}
	ans := ""
	for t := target; t > 0; {
		num := chs[t]
		t -= cost[num-1]
		ans += fmt.Sprintf("%d",num)
	}
	return ans
}